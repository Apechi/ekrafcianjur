@php $editing = isset($event) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.text name="title" label="Title" :value="old('title', $editing ? $event->title : '')" maxlength="255" placeholder="Title" required>
        </x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <label for="">Body</label>
        <textarea id="summernote" name="body" label="Body" maxlength="255" required>
            {{ old('body', $editing ? $event->body : '') }}</textarea id="summernote">
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.date name="tanggal" label="Tanggal"
            value="{{ old('tanggal', $editing ? optional($event->tanggal)->format('Y-m-d') : '') }}" max="255"
            required></x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea name="pembiayaan" label="Pembiayaan" maxlength="255" required>
            {{ old('pembiayaan', $editing ? $event->pembiayaan : '') }}</x-inputs.textarea>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text name="lokasi" label="Lokasi" :value="old('lokasi', $editing ? $event->lokasi : '')" maxlength="255" placeholder="Lokasi" required>
        </x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea name="alamat" label="Alamat" maxlength="255" required>
            {{ old('alamat', $editing ? $event->alamat : '') }}</x-inputs.textarea>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.datetime name="waktu_mulai" label="Waktu Mulai"
            value="{{ old('waktu_mulai', $editing ? optional($event->waktu_mulai)->format('Y-m-d\TH:i:s') : '') }}"
            max="255" required></x-inputs.datetime>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.datetime name="waktu_selesai" label="Waktu Selesai"
            value="{{ old('waktu_selesai', $editing ? optional($event->waktu_selesai)->format('Y-m-d\TH:i:s') : '') }}"
            max="255" required></x-inputs.datetime>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <div x-data="imageViewer('{{ $editing && $event->image ? \Storage::url($event->image) : '' }}')">
            <x-inputs.partials.label name="image" label="Image"></x-inputs.partials.label><br />

            <!-- Show the image -->
            <template x-if="imageUrl">
                <img :src="imageUrl" class="object-cover rounded border border-gray-200"
                    style="width: 100px; height: 100px;" />
            </template>

            <!-- Show the gray box when image is not available -->
            <template x-if="!imageUrl">
                <div class="border rounded border-gray-200 bg-gray-100" style="width: 100px; height: 100px;"></div>
            </template>

            <div class="mt-2">
                <input type="file" name="image" id="image" @change="fileChosen" />
            </div>

            @error('image')
    @include('components.inputs.partials.error')
@enderror
        </div>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="event_category_id" label="Event Category" required>
            @php $selected = old('event_category_id', ($editing ? $event->event_category_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Event Category</option>
            @foreach ($eventCategories as $value => $label)
<option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }}>{{ $label }}
                </option>
@endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="kecamatan_id" label="Kecamatan" required>
            @php $selected = old('kecamatan_id', ($editing ? $event->kecamatan_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Kecamatan</option>
            @foreach ($allKecamatan as $value => $label)
<option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }}>{{ $label }}
                </option>
@endforeach
        </x-inputs.select>
    </x-inputs.group>
</div>

<script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
</script>
