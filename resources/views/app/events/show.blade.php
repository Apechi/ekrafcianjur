@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('events.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.event.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.event.inputs.title')</h5>
                    <span>{{ $event->title ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.event.inputs.body')</h5>
                    <span>{{ $event->body ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.event.inputs.tanggal')</h5>
                    <span>{{ $event->tanggal ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.event.inputs.pembiayaan')</h5>
                    <span>{{ $event->pembiayaan ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.event.inputs.lokasi')</h5>
                    <span>{{ $event->lokasi ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.event.inputs.alamat')</h5>
                    <span>{{ $event->alamat ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.event.inputs.waktu_mulai')</h5>
                    <span>{{ $event->waktu_mulai ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.event.inputs.waktu_selesai')</h5>
                    <span>{{ $event->waktu_selesai ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.event.inputs.image')</h5>
                    <x-partials.thumbnail
                        src="{{ $event->image ? \Storage::url($event->image) : '' }}"
                        size="150"
                    />
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.event.inputs.event_category_id')</h5>
                    <span
                        >{{ optional($event->eventCategory)->nama ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.event.inputs.kecamatan_id')</h5>
                    <span>{{ optional($event->kecamatan)->nama ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('events.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\Event::class)
                <a href="{{ route('events.create') }}" class="btn btn-light">
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
