@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="searchbar mt-0 mb-4">
            <div class="row">
                <div class="col-md-6">
                    <form>
                        <div class="input-group">
                            <input id="indexSearch" type="text" name="search" placeholder="{{ __('crud.common.search') }}"
                                value="{{ $search ?? '' }}" class="form-control" autocomplete="off" />
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icon ion-md-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6 text-right">
                    @can('create', App\Models\Event::class)
                        <a href="{{ route('events.create') }}" class="btn btn-primary">
                            <i class="icon ion-md-add"></i> @lang('crud.common.create')
                        </a>
                    @endcan
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div style="display: flex; justify-content: space-between;">
                    <h4 class="card-title">@lang('crud.event.index_title')</h4>
                </div>

                <div class="table-responsive">
                    <table class="table table-borderless table-hover">
                        <thead>
                            <tr>
                                <th class="text-left">
                                    @lang('crud.event.inputs.title')
                                </th>
                                <th class="text-left">
                                    @lang('crud.event.inputs.body')
                                </th>
                                <th class="text-left">
                                    @lang('crud.event.inputs.tanggal')
                                </th>
                                <th class="text-left">
                                    @lang('crud.event.inputs.pembiayaan')
                                </th>
                                <th class="text-left">
                                    @lang('crud.event.inputs.lokasi')
                                </th>
                                <th class="text-left">
                                    @lang('crud.event.inputs.alamat')
                                </th>
                                <th class="text-left">
                                    @lang('crud.event.inputs.waktu_mulai')
                                </th>
                                <th class="text-left">
                                    @lang('crud.event.inputs.waktu_selesai')
                                </th>
                                <th class="text-left">
                                    @lang('crud.event.inputs.image')
                                </th>
                                <th class="text-left">
                                    @lang('crud.event.inputs.event_category_id')
                                </th>
                                <th class="text-left">
                                    @lang('crud.event.inputs.kecamatan_id')
                                </th>
                                <th class="text-center">
                                    @lang('crud.common.actions')
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($events as $event)
                                <tr>
                                    <td>{{ $event->title ?? '-' }}</td>
                                    <td>{{ $event->body ?? '-' }}</td>
                                    <td>{{ $event->tanggal ?? '-' }}</td>
                                    <td>{{ $event->pembiayaan ?? '-' }}</td>
                                    <td>{{ $event->lokasi ?? '-' }}</td>
                                    <td>{{ $event->alamat ?? '-' }}</td>
                                    <td>{{ $event->waktu_mulai ?? '-' }}</td>
                                    <td>{{ $event->waktu_selesai ?? '-' }}</td>
                                    <td>
                                        <x-partials.thumbnail
                                            src="{{ $event->image ? \Storage::url($event->image) : '' }}" />
                                    </td>
                                    <td>
                                        {{ optional($event->eventCategory)->nama ?? '-' }}
                                    </td>
                                    <td>
                                        {{ optional($event->kecamatan)->nama ?? '-' }}
                                    </td>
                                    <td class="text-center" style="width: 134px;">
                                        <div role="group" aria-label="Row Actions" class="btn-group">
                                            @can('update', $event)
                                                <a href="{{ route('events.edit', $event) }}">
                                                    <button type="button" class="btn btn-light">
                                                        <i class="icon ion-md-create"></i>
                                                    </button>
                                                </a>
                                                @endcan @can('view', $event)
                                                <a href="{{ route('events.show', $event) }}">
                                                    <button type="button" class="btn btn-light">
                                                        <i class="icon ion-md-eye"></i>
                                                    </button>
                                                </a>
                                                @endcan @can('delete', $event)
                                                <form action="{{ route('events.destroy', $event) }}" method="POST"
                                                    onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')">
                                                    @csrf @method('DELETE')
                                                    <button type="submit" class="btn btn-light text-danger">
                                                        <i class="icon ion-md-trash"></i>
                                                    </button>
                                                </form>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="12">
                                        @lang('crud.common.no_items_found')
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="12">{!! $events->render() !!}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
