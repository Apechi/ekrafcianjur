@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('all-ekraf.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.ekraf.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.ekraf.inputs.nama')</h5>
                    <span>{{ $ekraf->nama ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ekraf.inputs.logo')</h5>
                    <x-partials.thumbnail
                        src="{{ $ekraf->logo ? \Storage::url($ekraf->logo) : '' }}"
                        size="150"
                    />
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ekraf.inputs.alamat')</h5>
                    <span>{{ $ekraf->alamat ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ekraf.inputs.coordinate')</h5>
                    <span>{{ $ekraf->coordinate ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ekraf.inputs.nama_pemilik')</h5>
                    <span>{{ $ekraf->nama_pemilik ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ekraf.inputs.email')</h5>
                    <span>{{ $ekraf->email ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ekraf.inputs.no_telp')</h5>
                    <span>{{ $ekraf->no_telp ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ekraf.inputs.website')</h5>
                    <span>{{ $ekraf->website ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ekraf.inputs.desc')</h5>
                    <span>{{ $ekraf->desc ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ekraf.inputs.category_id')</h5>
                    <span>{{ optional($ekraf->category)->nama ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ekraf.inputs.sub_sector_id')</h5>
                    <span>{{ optional($ekraf->subSector)->nama ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ekraf.inputs.kecamatan_id')</h5>
                    <span>{{ optional($ekraf->kecamatan)->nama ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.ekraf.inputs.user_id')</h5>
                    <span
                        >{{ optional($ekraf->user)->nama_lengkap ?? '-' }}</span
                    >
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('all-ekraf.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\Ekraf::class)
                <a href="{{ route('all-ekraf.create') }}" class="btn btn-light">
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
