@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="searchbar mt-0 mb-4">
            <div class="row">
                <div class="col-md-6">
                    <form>
                        <div class="input-group">
                            <input id="indexSearch" type="text" name="search" placeholder="{{ __('crud.common.search') }}"
                                value="{{ $search ?? '' }}" class="form-control" autocomplete="off" />
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icon ion-md-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6 text-right">
                    @can('create', App\Models\Ekraf::class)
                        <a href="{{ route('all-ekraf.create') }}" class="btn btn-primary">
                            <i class="icon ion-md-add"></i> @lang('crud.common.create')
                        </a>
                    @endcan
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div style="display: flex; justify-content: space-between;">
                    <h4 class="card-title">@lang('crud.ekraf.index_title')</h4>
                </div>

                <div class="table-responsive">
                    <table class="table table-borderless table-hover">
                        <thead>
                            <tr>
                                <th class="text-left">
                                    @lang('crud.ekraf.inputs.nama')
                                </th>
                                <th class="text-left">
                                    @lang('crud.ekraf.inputs.logo')
                                </th>
                                <th class="text-left">
                                    @lang('crud.ekraf.inputs.alamat')
                                </th>
                                <th class="text-left">
                                    @lang('crud.ekraf.inputs.coordinate')
                                </th>
                                <th class="text-left">
                                    @lang('crud.ekraf.inputs.nama_pemilik')
                                </th>
                                <th class="text-left">
                                    @lang('crud.ekraf.inputs.email')
                                </th>
                                <th class="text-left">
                                    @lang('crud.ekraf.inputs.no_telp')
                                </th>
                                <th class="text-left">
                                    @lang('crud.ekraf.inputs.website')
                                </th>
                                <th class="text-left">
                                    @lang('crud.ekraf.inputs.desc')
                                </th>
                                <th class="text-left">
                                    @lang('crud.ekraf.inputs.category_id')
                                </th>
                                <th class="text-left">
                                    @lang('crud.ekraf.inputs.sub_sector_id')
                                </th>
                                <th class="text-left">
                                    @lang('crud.ekraf.inputs.kecamatan_id')
                                </th>
                                <th class="text-left">
                                    @lang('crud.ekraf.inputs.user_id')
                                </th>
                                <th class="text-center">
                                    @lang('crud.common.actions')
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($allEkraf as $ekraf)
                                <tr>
                                    <td>{{ $ekraf->nama ?? '-' }}</td>
                                    <td>
                                        <x-partials.thumbnail src="{{ $ekraf->logo ? \Storage::url($ekraf->logo) : '' }}" />
                                    </td>
                                    <td>{{ $ekraf->alamat ?? '-' }}</td>
                                    <td>{{ $ekraf->coordinate ?? '-' }}</td>
                                    <td>{{ $ekraf->nama_pemilik ?? '-' }}</td>
                                    <td>{{ $ekraf->email ?? '-' }}</td>
                                    <td>{{ $ekraf->no_telp ?? '-' }}</td>
                                    <td>{{ $ekraf->website ?? '-' }}</td>
                                    <td>{{ $ekraf->desc ?? '-' }}</td>
                                    <td>
                                        {{ optional($ekraf->category)->nama ?? '-' }}
                                    </td>
                                    <td>
                                        {{ optional($ekraf->subSector)->nama ?? '-' }}
                                    </td>
                                    <td>
                                        {{ optional($ekraf->kecamatan)->nama ?? '-' }}
                                    </td>
                                    <td>
                                        {{ optional($ekraf->user)->nama_lengkap ?? '-' }}
                                    </td>
                                    <td class="text-center" style="width: 134px;">
                                        <div role="group" aria-label="Row Actions" class="btn-group">
                                            @can('update', $ekraf)
                                                <a href="{{ route('all-ekraf.edit', $ekraf) }}">
                                                    <button type="button" class="btn btn-light">
                                                        <i class="icon ion-md-create"></i>
                                                    </button>
                                                </a>
                                                @endcan @can('view', $ekraf)
                                                <a href="{{ route('all-ekraf.show', $ekraf) }}">
                                                    <button type="button" class="btn btn-light">
                                                        <i class="icon ion-md-eye"></i>
                                                    </button>
                                                </a>
                                                @endcan @can('delete', $ekraf)
                                                <form action="{{ route('all-ekraf.destroy', $ekraf) }}" method="POST"
                                                    onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')">
                                                    @csrf @method('DELETE')
                                                    <button type="submit" class="btn btn-light text-danger">
                                                        <i class="icon ion-md-trash"></i>
                                                    </button>
                                                </form>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="14">
                                        @lang('crud.common.no_items_found')
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="14">{!! $allEkraf->render() !!}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
