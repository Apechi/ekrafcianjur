@php $editing = isset($bookingDetail) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="booking_header_id" label="Booking Header" required>
            @php $selected = old('booking_header_id', ($editing ? $bookingDetail->booking_header_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Booking Header</option>
            @foreach ($bookingHeaders as $value)
                <option value="{{ $value->id }}" {{ $selected == $value ? 'selected' : '' }}>{{ $value->id }}
                </option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text name="harga_unit" label="Harga Unit" :value="old('harga_unit', $editing ? $bookingDetail->harga_unit : '')" maxlength="255" placeholder="Harga Unit"
            required></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text name="sub_total" label="Sub Total" :value="old('sub_total', $editing ? $bookingDetail->sub_total : '')" maxlength="255" placeholder="Sub Total"
            required></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.number name="durasi" label="Durasi" :value="old('durasi', $editing ? $bookingDetail->durasi : '')" max="255" placeholder="Durasi" required>
        </x-inputs.number>
    </x-inputs.group>
</div>
