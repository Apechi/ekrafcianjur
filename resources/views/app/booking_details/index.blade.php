@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="searchbar mt-0 mb-4">
            <div class="row">
                <div class="col-md-6">
                    <form>
                        <div class="input-group">
                            <input id="indexSearch" type="text" name="search" placeholder="{{ __('crud.common.search') }}"
                                value="{{ $search ?? '' }}" class="form-control" autocomplete="off" />
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icon ion-md-search"></i>
                                </button>
                            </div>


                        </div>
                        <div class="option d-flex justify-center align-items-center mt-4">
                            <label class="m-0" for="booking_header">Booking Header</label>
                            <select class="ml-4" name="booking_header" id="booking_header">
                                <option value="">Semua Booking Header</option>
                                @foreach ($bookingHeaderOption as $item)
                                    <option value="{{ $item->id }}">{{ $item->id }}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="col-md-6 text-right">
                    @can('create', App\Models\BookingDetail::class)
                        <a href="{{ route('booking-details.create') }}" class="btn btn-primary">
                            <i class="icon ion-md-add"></i> @lang('crud.common.create')
                        </a>
                    @endcan
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div style="display: flex; justify-content: space-between;">
                    <h4 class="card-title">
                        @lang('crud.booking_detail.index_title')
                    </h4>
                </div>

                <div class="table-responsive">
                    <table class="table table-borderless table-hover">
                        <thead>
                            <tr>
                                <th class="text-left">
                                    @lang('crud.booking_detail.inputs.booking_header_id')
                                </th>
                                <th class="text-left">
                                    @lang('crud.booking_detail.inputs.harga_unit')
                                </th>
                                <th class="text-left">
                                    @lang('crud.booking_detail.inputs.sub_total')
                                </th>
                                <th class="text-left">
                                    @lang('crud.booking_detail.inputs.durasi')
                                </th>
                                <th class="text-center">
                                    @lang('crud.common.actions')
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($bookingDetails as $bookingDetail)
                                <tr>
                                    <td>
                                        {{ optional($bookingDetail->bookingHeader)->id ?? '-' }}
                                    </td>
                                    <td>{{ $bookingDetail->harga_unit ?? '-' }}</td>
                                    <td>{{ $bookingDetail->sub_total ?? '-' }}</td>
                                    <td>{{ $bookingDetail->durasi ?? '-' }}</td>
                                    <td class="text-center" style="width: 134px;">
                                        <div role="group" aria-label="Row Actions" class="btn-group">
                                            @can('update', $bookingDetail)
                                                <a href="{{ route('booking-details.edit', $bookingDetail) }}">
                                                    <button type="button" class="btn btn-light">
                                                        <i class="icon ion-md-create"></i>
                                                    </button>
                                                </a>
                                                @endcan @can('view', $bookingDetail)
                                                <a href="{{ route('booking-details.show', $bookingDetail) }}">
                                                    <button type="button" class="btn btn-light">
                                                        <i class="icon ion-md-eye"></i>
                                                    </button>
                                                </a>
                                                @endcan @can('delete', $bookingDetail)
                                                <form action="{{ route('booking-details.destroy', $bookingDetail) }}"
                                                    method="POST"
                                                    onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')">
                                                    @csrf @method('DELETE')
                                                    <button type="submit" class="btn btn-light text-danger">
                                                        <i class="icon ion-md-trash"></i>
                                                    </button>
                                                </form>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">
                                        @lang('crud.common.no_items_found')
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">
                                    {!! $bookingDetails->render() !!}
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
