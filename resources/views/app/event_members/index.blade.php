@extends('layouts.app')

@section('content')
<div class="container">
    <div class="searchbar mt-0 mb-4">
        <div class="row">
            <div class="col-md-6">
                <form>
                    <div class="input-group">
                        <input
                            id="indexSearch"
                            type="text"
                            name="search"
                            placeholder="{{ __('crud.common.search') }}"
                            value="{{ $search ?? '' }}"
                            class="form-control"
                            autocomplete="off"
                        />
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary">
                                <i class="icon ion-md-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6 text-right">
                @can('create', App\Models\EventMember::class)
                <a
                    href="{{ route('event-members.create') }}"
                    class="btn btn-primary"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div style="display: flex; justify-content: space-between;">
                <h4 class="card-title">
                    @lang('crud.member_event.index_title')
                </h4>
            </div>

            <div class="table-responsive">
                <table class="table table-borderless table-hover">
                    <thead>
                        <tr>
                            <th class="text-left">
                                @lang('crud.member_event.inputs.user_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.member_event.inputs.nama_lengkap')
                            </th>
                            <th class="text-left">
                                @lang('crud.member_event.inputs.email')
                            </th>
                            <th class="text-left">
                                @lang('crud.member_event.inputs.no_telp')
                            </th>
                            <th class="text-left">
                                @lang('crud.member_event.inputs.sub_sector_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.member_event.inputs.kecamatan_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.member_event.inputs.event_id')
                            </th>
                            <th class="text-center">
                                @lang('crud.common.actions')
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($eventMembers as $eventMember)
                        <tr>
                            <td>
                                {{ optional($eventMember->user)->nama_lengkap ??
                                '-' }}
                            </td>
                            <td>{{ $eventMember->nama_lengkap ?? '-' }}</td>
                            <td>{{ $eventMember->email ?? '-' }}</td>
                            <td>{{ $eventMember->no_telp ?? '-' }}</td>
                            <td>
                                {{ optional($eventMember->subSector)->nama ??
                                '-' }}
                            </td>
                            <td>
                                {{ optional($eventMember->kecamatan)->nama ??
                                '-' }}
                            </td>
                            <td>
                                {{ optional($eventMember->event)->title ?? '-'
                                }}
                            </td>
                            <td class="text-center" style="width: 134px;">
                                <div
                                    role="group"
                                    aria-label="Row Actions"
                                    class="btn-group"
                                >
                                    @can('update', $eventMember)
                                    <a
                                        href="{{ route('event-members.edit', $eventMember) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-create"></i>
                                        </button>
                                    </a>
                                    @endcan @can('view', $eventMember)
                                    <a
                                        href="{{ route('event-members.show', $eventMember) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-eye"></i>
                                        </button>
                                    </a>
                                    @endcan @can('delete', $eventMember)
                                    <form
                                        action="{{ route('event-members.destroy', $eventMember) }}"
                                        method="POST"
                                        onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')"
                                    >
                                        @csrf @method('DELETE')
                                        <button
                                            type="submit"
                                            class="btn btn-light text-danger"
                                        >
                                            <i class="icon ion-md-trash"></i>
                                        </button>
                                    </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="8">
                                @lang('crud.common.no_items_found')
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="8">{!! $eventMembers->render() !!}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
