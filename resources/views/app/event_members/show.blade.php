@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('event-members.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.member_event.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.member_event.inputs.user_id')</h5>
                    <span
                        >{{ optional($eventMember->user)->nama_lengkap ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.member_event.inputs.nama_lengkap')</h5>
                    <span>{{ $eventMember->nama_lengkap ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.member_event.inputs.email')</h5>
                    <span>{{ $eventMember->email ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.member_event.inputs.no_telp')</h5>
                    <span>{{ $eventMember->no_telp ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.member_event.inputs.sub_sector_id')</h5>
                    <span
                        >{{ optional($eventMember->subSector)->nama ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.member_event.inputs.kecamatan_id')</h5>
                    <span
                        >{{ optional($eventMember->kecamatan)->nama ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.member_event.inputs.event_id')</h5>
                    <span
                        >{{ optional($eventMember->event)->title ?? '-' }}</span
                    >
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('event-members.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EventMember::class)
                <a
                    href="{{ route('event-members.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
