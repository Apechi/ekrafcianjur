@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('all-fasilitas-gedung.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.fasilitas_gedung.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.fasilitas_gedung.inputs.gedung_id')</h5>
                    <span>{{ $fasilitasGedung->gedung_id ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.fasilitas_gedung.inputs.fasilitas')</h5>
                    <span>{{ $fasilitasGedung->fasilitas ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('all-fasilitas-gedung.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\FasilitasGedung::class)
                <a
                    href="{{ route('all-fasilitas-gedung.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
