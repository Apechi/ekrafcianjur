@php $editing = isset($fasilitasGedung) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="gedung_id" label="Nama Gedung">
            @foreach ($gedung as $item)
                <option value="{{ $item->id }}">{{ $item->nama }}</option>
            @endforeach
            @php $selected = old('gedung_id', ($editing ? $fasilitasGedung->gedung_id : '')) @endphp
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text name="fasilitas" label="Fasilitas" :value="old('fasilitas', $editing ? $fasilitasGedung->fasilitas : '')" maxlength="255" placeholder="Fasilitas"
            required></x-inputs.text>
    </x-inputs.group>
</div>
