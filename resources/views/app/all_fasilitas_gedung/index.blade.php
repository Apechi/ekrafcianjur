@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="searchbar mt-0 mb-4">
            <div class="row">
                <div class="col-md-6">
                    <form>
                        <div class="input-group">
                            <input id="indexSearch" type="text" name="search" placeholder="{{ __('crud.common.search') }}"
                                value="{{ $search ?? '' }}" class="form-control" autocomplete="off" />

                            <select name="gedung_id" class="form-control ml-4">
                                <option value="">All Gedungs</option>
                                @foreach ($gedungOptions as $id => $name)
                                    <option value="{{ $id }}" {{ $gedungId == $id ? 'selected' : '' }}>
                                        {{ $name }}
                                    </option>
                                @endforeach
                            </select>

                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icon ion-md-search"></i>
                                </button>
                            </div>

                        </div>

                    </form>
                </div>
                <div class="col-md-6 text-right">
                    @can('create', App\Models\FasilitasGedung::class)
                        <a href="{{ route('all-fasilitas-gedung.create') }}" class="btn btn-primary">
                            <i class="icon ion-md-add"></i> @lang('crud.common.create')
                        </a>
                    @endcan
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div style="display: flex; justify-content: space-between;">
                    <h4 class="card-title">
                        @lang('crud.fasilitas_gedung.index_title')
                    </h4>
                </div>

                <div class="table-responsive">
                    <table class="table table-borderless table-hover">
                        <thead>
                            <tr>
                                <th class="text-left">
                                    @lang('crud.fasilitas_gedung.inputs.gedung_id')
                                </th>
                                <th class="text-left">
                                    @lang('crud.fasilitas_gedung.inputs.fasilitas')
                                </th>
                                <th class="text-center">
                                    @lang('crud.common.actions')
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($allFasilitasGedung as $fasilitasGedung)
                                <tr>
                                    <td>{{ $fasilitasGedung->gedung_id ?? '-' }}</td>
                                    <td>{{ $fasilitasGedung->fasilitas ?? '-' }}</td>
                                    <td class="text-center" style="width: 134px;">
                                        <div role="group" aria-label="Row Actions" class="btn-group">
                                            @can('update', $fasilitasGedung)
                                                <a href="{{ route('all-fasilitas-gedung.edit', $fasilitasGedung) }}">
                                                    <button type="button" class="btn btn-light">
                                                        <i class="icon ion-md-create"></i>
                                                    </button>
                                                </a>
                                                @endcan @can('view', $fasilitasGedung)
                                                <a href="{{ route('all-fasilitas-gedung.show', $fasilitasGedung) }}">
                                                    <button type="button" class="btn btn-light">
                                                        <i class="icon ion-md-eye"></i>
                                                    </button>
                                                </a>
                                                @endcan @can('delete', $fasilitasGedung)
                                                <form action="{{ route('all-fasilitas-gedung.destroy', $fasilitasGedung) }}"
                                                    method="POST"
                                                    onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')">
                                                    @csrf @method('DELETE')
                                                    <button type="submit" class="btn btn-light text-danger">
                                                        <i class="icon ion-md-trash"></i>
                                                    </button>
                                                </form>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">
                                        @lang('crud.common.no_items_found')
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3">
                                    {!! $allFasilitasGedung->render() !!}
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
