@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('all-gedung.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.gedung.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.gedung.inputs.image')</h5>
                    <x-partials.thumbnail
                        src="{{ $gedung->image ? \Storage::url($gedung->image) : '' }}"
                        size="150"
                    />
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.gedung.inputs.nama')</h5>
                    <span>{{ $gedung->nama ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.gedung.inputs.desc')</h5>
                    <span>{{ $gedung->desc ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.gedung.inputs.kegiatan')</h5>
                    <span>{{ $gedung->kegiatan ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.gedung.inputs.harga')</h5>
                    <span>{{ $gedung->harga ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.gedung.inputs.alamat')</h5>
                    <span>{{ $gedung->alamat ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.gedung.inputs.no_telp')</h5>
                    <span>{{ $gedung->no_telp ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('all-gedung.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\Gedung::class)
                <a
                    href="{{ route('all-gedung.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
