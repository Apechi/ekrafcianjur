@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('contact-informations.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.informasi_kontak.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.informasi_kontak.inputs.image')</h5>
                    <x-partials.thumbnail
                        src="{{ $contactInformation->image ? \Storage::url($contactInformation->image) : '' }}"
                        size="150"
                    />
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.informasi_kontak.inputs.about')</h5>
                    <span>{{ $contactInformation->about ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.informasi_kontak.inputs.email')</h5>
                    <span>{{ $contactInformation->email ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.informasi_kontak.inputs.coordinate')</h5>
                    <span>{{ $contactInformation->coordinate ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.informasi_kontak.inputs.address')</h5>
                    <span>{{ $contactInformation->address ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.informasi_kontak.inputs.no_telp')</h5>
                    <span>{{ $contactInformation->no_telp ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('contact-informations.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\ContactInformation::class)
                <a
                    href="{{ route('contact-informations.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
