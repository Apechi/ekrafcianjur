@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('contactsboxes.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.hasil_submit_kontak.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.hasil_submit_kontak.inputs.nama')</h5>
                    <span>{{ $contactsbox->nama ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.hasil_submit_kontak.inputs.email')</h5>
                    <span>{{ $contactsbox->email ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.hasil_submit_kontak.inputs.message')</h5>
                    <span>{{ $contactsbox->message ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('contactsboxes.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\Contactsbox::class)
                <a
                    href="{{ route('contactsboxes.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
