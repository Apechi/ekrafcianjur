@php $editing = isset($socialmedia) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="icon"
            label="Icon"
            :value="old('icon', ($editing ? $socialmedia->icon : ''))"
            maxlength="255"
            placeholder="Icon"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea name="link" label="Link" maxlength="255" required
            >{{ old('link', ($editing ? $socialmedia->link : ''))
            }}</x-inputs.textarea
        >
    </x-inputs.group>
</div>
