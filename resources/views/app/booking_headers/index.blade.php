@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="searchbar mt-0 mb-4">
            <div class="row">
                <div class="col-md-6">
                    <form>
                        <div class="input-group">
                            <input id="indexSearch" type="text" name="search" placeholder="{{ __('crud.common.search') }}"
                                value="{{ $search ?? '' }}" class="form-control" autocomplete="off" />
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icon ion-md-search"></i>
                                </button>
                            </div>
                        </div>
                        <div class="option mt-4">
                            <label for="status">Status</label>
                            <select name="status" id="status">
                                <option value="">Semua</option>
                                <option value="Pending">Pending</option>
                                <option value="Selesai">Selesai</option>
                            </select>
                            <label class="ml-4" for="status">Payment Type</label>
                            <select name="paymenttype" id="paymenttype">
                                <option value="">Semua</option>
                                <option value="Tunai">Tunai</option>
                                <option value="Debit">Debit</option>
                            </select>
                            <label class="ml-4" for="status">Gedung</label>
                            <select name="gedung" id="gedung">
                                <option value="">Semua</option>
                                @foreach ($gedungoption as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>

                    </form>
                </div>
                <div class="col-md-6 text-right">
                    @can('create', App\Models\BookingHeader::class)
                        <a href="{{ route('booking-headers.create') }}" class="btn btn-primary">
                            <i class="icon ion-md-add"></i> @lang('crud.common.create')
                        </a>
                    @endcan
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div style="display: flex; justify-content: space-between;">
                    <h4 class="card-title">
                        @lang('crud.booking_header.index_title')
                    </h4>
                </div>

                <div class="table-responsive">
                    <table class="table table-borderless table-hover">
                        <thead>
                            <tr>
                                <th class="text-left">
                                    ID
                                </th>
                                <th class="text-left">
                                    @lang('crud.booking_header.inputs.date')
                                </th>
                                <th class="text-left">
                                    @lang('crud.booking_header.inputs.total_harga')
                                </th>
                                <th class="text-left">
                                    @lang('crud.booking_header.inputs.status')
                                </th>
                                <th class="text-left">
                                    @lang('crud.booking_header.inputs.payment_type')
                                </th>
                                <th class="text-left">
                                    @lang('crud.booking_header.inputs.card_number')
                                </th>
                                <th class="text-left">
                                    @lang('crud.booking_header.inputs.bank')
                                </th>
                                <th class="text-left">
                                    @lang('crud.booking_header.inputs.user_id')
                                </th>
                                <th class="text-left">
                                    @lang('crud.booking_header.inputs.gedung_id')
                                </th>
                                <th class="text-center">
                                    @lang('crud.common.actions')
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($bookingHeaders as $bookingHeader)
                                <tr>
                                    <td>{{ $bookingHeader->id ?? '-' }}</td>
                                    <td>{{ $bookingHeader->date ?? '-' }}</td>
                                    <td>{{ $bookingHeader->total_harga ?? '-' }}</td>
                                    <td>{{ $bookingHeader->status ?? '-' }}</td>
                                    <td>{{ $bookingHeader->payment_type ?? '-' }}</td>
                                    <td>{{ $bookingHeader->card_number ?? '-' }}</td>
                                    <td>{{ $bookingHeader->bank ?? '-' }}</td>
                                    <td>
                                        {{ optional($bookingHeader->user)->nama_lengkap ?? '-' }}
                                    </td>
                                    <td>
                                        {{ optional($bookingHeader->gedung)->image ?? '-' }}
                                    </td>
                                    <td class="text-center" style="width: 134px;">
                                        <div role="group" aria-label="Row Actions" class="btn-group">
                                            @can('update', $bookingHeader)
                                                <a href="{{ route('booking-headers.edit', $bookingHeader) }}">
                                                    <button type="button" class="btn btn-light">
                                                        <i class="icon ion-md-create"></i>
                                                    </button>
                                                </a>
                                                @endcan @can('view', $bookingHeader)
                                                <a href="{{ route('booking-headers.show', $bookingHeader) }}">
                                                    <button type="button" class="btn btn-light">
                                                        <i class="icon ion-md-eye"></i>
                                                    </button>
                                                </a>
                                                @endcan @can('delete', $bookingHeader)
                                                <form action="{{ route('booking-headers.destroy', $bookingHeader) }}"
                                                    method="POST"
                                                    onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')">
                                                    @csrf @method('DELETE')
                                                    <button type="submit" class="btn btn-light text-danger">
                                                        <i class="icon ion-md-trash"></i>
                                                    </button>
                                                </form>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9">
                                        @lang('crud.common.no_items_found')
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="9">
                                    {!! $bookingHeaders->render() !!}
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
