@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('booking-headers.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.booking_header.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.booking_header.inputs.date')</h5>
                    <span>{{ $bookingHeader->date ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.booking_header.inputs.total_harga')</h5>
                    <span>{{ $bookingHeader->total_harga ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.booking_header.inputs.status')</h5>
                    <span>{{ $bookingHeader->status ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.booking_header.inputs.payment_type')</h5>
                    <span>{{ $bookingHeader->payment_type ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.booking_header.inputs.card_number')</h5>
                    <span>{{ $bookingHeader->card_number ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.booking_header.inputs.bank')</h5>
                    <span>{{ $bookingHeader->bank ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.booking_header.inputs.user_id')</h5>
                    <span
                        >{{ optional($bookingHeader->user)->nama_lengkap ?? '-'
                        }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.booking_header.inputs.gedung_id')</h5>
                    <span
                        >{{ optional($bookingHeader->gedung)->image ?? '-'
                        }}</span
                    >
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('booking-headers.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\BookingHeader::class)
                <a
                    href="{{ route('booking-headers.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
