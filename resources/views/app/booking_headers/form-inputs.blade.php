@php $editing = isset($bookingHeader) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.date name="date" label="Date" value="{{ $today }}" max="255" required>
        </x-inputs.date>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text name="total_harga" label="Total Harga" :value="old('total_harga', $editing ? $bookingHeader->total_harga : '')" maxlength="255" placeholder="Total Harga"
            required></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="status" label="Status">
            @php $selected = old('status', ($editing ? $bookingHeader->status : '')) @endphp
            <option value="Pending" {{ $selected == 'Pending' ? 'selected' : '' }}>Pending</option>
            <option value="Selesai" {{ $selected == 'Selesai' ? 'selected' : '' }}>Selesai</option>
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="payment_type" label="Payment Type">
            @php $selected = old('payment_type', ($editing ? $bookingHeader->payment_type : '')) @endphp
            <option value="Tunai" {{ $selected == 'Tunai' ? 'selected' : '' }}>Tunai</option>
            <option value="Debit" {{ $selected == 'Debit' ? 'selected' : '' }}>Debit</option>
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text name="card_number" label="Card Number" :value="old('card_number', $editing ? $bookingHeader->card_number : '')" maxlength="255"
            placeholder="Card Number"></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text name="bank" label="Bank" :value="old('bank', $editing ? $bookingHeader->bank : '')" maxlength="255" placeholder="Bank">
        </x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="user_id" label="User" required>
            @php $selected = old('user_id', ($editing ? $bookingHeader->user_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the User</option>
            @foreach ($users as $value => $label)
                <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }}>{{ $label }}
                </option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="gedung_id" label="Gedung" required>
            @php $selected = old('gedung_id', ($editing ? $bookingHeader->gedung_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Gedung</option>
            @foreach ($allGedung as $item)
                <option value="{{ $item->id }}" {{ $selected == $item->id ? 'selected' : '' }}>
                    {{ $item->nama }}
                </option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>
</div>
