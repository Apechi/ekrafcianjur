@php $editing = isset($article) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <div x-data="imageViewer('{{ $editing && $article->image ? \Storage::url($article->image) : '' }}')">
            <x-inputs.partials.label name="image" label="Image"></x-inputs.partials.label><br />

            <!-- Show the image -->
            <template x-if="imageUrl">
                <img :src="imageUrl" class="object-cover rounded border border-gray-200"
                    style="width: 100px; height: 100px;" />
            </template>

            <!-- Show the gray box when image is not available -->
            <template x-if="!imageUrl">
                <div class="border rounded border-gray-200 bg-gray-100" style="width: 100px; height: 100px;"></div>
            </template>

            <div class="mt-2">
                <input type="file" name="image" id="image" @change="fileChosen" />
            </div>

            @error('image')
                @include('components.inputs.partials.error')
            @enderror
        </div>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text name="title" label="Title" :value="old('title', $editing ? $article->title : '')" maxlength="255" placeholder="Title" required>
        </x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <label for="summernote">Body</label>
        <textarea name="body" id="summernote" label="Body" required>
            {{ old('body', $editing ? $article->body : '') }}
        </textarea>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text name="writer" label="Writer" :value="old('writer', $editing ? $article->writer : '')" maxlength="255" placeholder="Writer" required>
        </x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.datetime name="published_at" label="Published At"
            value="{{ old('published_at', $editing ? optional($article->published_at)->format('Y-m-d\TH:i:s') : '') }}"
            max="255" required></x-inputs.datetime>
    </x-inputs.group>
</div>

<script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
</script>
