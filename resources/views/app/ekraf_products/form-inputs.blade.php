@php $editing = isset($ekrafProduct) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <div
            x-data="imageViewer('{{ $editing && $ekrafProduct->image ? \Storage::url($ekrafProduct->image) : '' }}')"
        >
            <x-inputs.partials.label
                name="image"
                label="Image"
            ></x-inputs.partials.label
            ><br />

            <!-- Show the image -->
            <template x-if="imageUrl">
                <img
                    :src="imageUrl"
                    class="object-cover rounded border border-gray-200"
                    style="width: 100px; height: 100px;"
                />
            </template>

            <!-- Show the gray box when image is not available -->
            <template x-if="!imageUrl">
                <div
                    class="border rounded border-gray-200 bg-gray-100"
                    style="width: 100px; height: 100px;"
                ></div>
            </template>

            <div class="mt-2">
                <input
                    type="file"
                    name="image"
                    id="image"
                    @change="fileChosen"
                />
            </div>

            @error('image') @include('components.inputs.partials.error')
            @enderror
        </div>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="nama"
            label="Nama"
            :value="old('nama', ($editing ? $ekrafProduct->nama : ''))"
            maxlength="255"
            placeholder="Nama"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="tipe"
            label="Tipe"
            :value="old('tipe', ($editing ? $ekrafProduct->tipe : ''))"
            maxlength="255"
            placeholder="Tipe"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="harga_jual"
            label="Harga Jual"
            :value="old('harga_jual', ($editing ? $ekrafProduct->harga_jual : ''))"
            maxlength="255"
            placeholder="Harga Jual"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="ekspor" label="Ekspor">
            @php $selected = old('ekspor', ($editing ? $ekrafProduct->ekspor : '')) @endphp
            <option value="Ya" {{ $selected == 'Ya' ? 'selected' : '' }} >Ya</option>
            <option value="Tidak" {{ $selected == 'Tidak' ? 'selected' : '' }} >Tidak</option>
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea name="desc" label="Desc" maxlength="255" required
            >{{ old('desc', ($editing ? $ekrafProduct->desc : ''))
            }}</x-inputs.textarea
        >
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="ekraf_id" label="Ekraf" required>
            @php $selected = old('ekraf_id', ($editing ? $ekrafProduct->ekraf_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Ekraf</option>
            @foreach($allEkraf as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>
</div>
