@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('ekraf-products.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.produk_ekraf.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.produk_ekraf.inputs.image')</h5>
                    <x-partials.thumbnail
                        src="{{ $ekrafProduct->image ? \Storage::url($ekrafProduct->image) : '' }}"
                        size="150"
                    />
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.produk_ekraf.inputs.nama')</h5>
                    <span>{{ $ekrafProduct->nama ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.produk_ekraf.inputs.tipe')</h5>
                    <span>{{ $ekrafProduct->tipe ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.produk_ekraf.inputs.harga_jual')</h5>
                    <span>{{ $ekrafProduct->harga_jual ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.produk_ekraf.inputs.ekspor')</h5>
                    <span>{{ $ekrafProduct->ekspor ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.produk_ekraf.inputs.desc')</h5>
                    <span>{{ $ekrafProduct->desc ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.produk_ekraf.inputs.ekraf_id')</h5>
                    <span
                        >{{ optional($ekrafProduct->ekraf)->logo ?? '-' }}</span
                    >
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('ekraf-products.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\EkrafProduct::class)
                <a
                    href="{{ route('ekraf-products.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
