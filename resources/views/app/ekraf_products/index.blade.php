@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="searchbar mt-0 mb-4">
            <div class="row">
                <div class="col-md-6">
                    <form>
                        <div class="input-group">
                            <input id="indexSearch" type="text" name="search" placeholder="{{ __('crud.common.search') }}"
                                value="{{ $search ?? '' }}" class="form-control" autocomplete="off" />
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icon ion-md-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6 text-right">
                    @can('create', App\Models\EkrafProduct::class)
                        <a href="{{ route('ekraf-products.create') }}" class="btn btn-primary">
                            <i class="icon ion-md-add"></i> @lang('crud.common.create')
                        </a>
                    @endcan
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div style="display: flex; justify-content: space-between;">
                    <h4 class="card-title">
                        @lang('crud.produk_ekraf.index_title')
                    </h4>
                </div>

                <div class="table-responsive">
                    <table class="table table-borderless table-hover">
                        <thead>
                            <tr>
                                <th class="text-left">
                                    @lang('crud.produk_ekraf.inputs.image')
                                </th>
                                <th class="text-left">
                                    @lang('crud.produk_ekraf.inputs.nama')
                                </th>
                                <th class="text-left">
                                    @lang('crud.produk_ekraf.inputs.tipe')
                                </th>
                                <th class="text-left">
                                    @lang('crud.produk_ekraf.inputs.harga_jual')
                                </th>
                                <th class="text-left">
                                    @lang('crud.produk_ekraf.inputs.ekspor')
                                </th>
                                <th class="text-left">
                                    @lang('crud.produk_ekraf.inputs.desc')
                                </th>
                                <th class="text-left">
                                    @lang('crud.produk_ekraf.inputs.ekraf_id')
                                </th>
                                <th class="text-center">
                                    @lang('crud.common.actions')
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($ekrafProducts as $ekrafProduct)
                                <tr>
                                    <td>
                                        <x-partials.thumbnail
                                            src="{{ $ekrafProduct->image ? \Storage::url($ekrafProduct->image) : '' }}" />
                                    </td>
                                    <td>{{ $ekrafProduct->nama ?? '-' }}</td>
                                    <td>{{ $ekrafProduct->tipe ?? '-' }}</td>
                                    <td>{{ $ekrafProduct->harga_jual ?? '-' }}</td>
                                    <td>{{ $ekrafProduct->ekspor ?? '-' }}</td>
                                    <td>{{ $ekrafProduct->desc ?? '-' }}</td>
                                    <td>
                                        {{ optional($ekrafProduct->ekraf)->nama ?? '-' }}
                                    </td>
                                    <td class="text-center" style="width: 134px;">
                                        <div role="group" aria-label="Row Actions" class="btn-group">
                                            @can('update', $ekrafProduct)
                                                <a href="{{ route('ekraf-products.edit', $ekrafProduct) }}">
                                                    <button type="button" class="btn btn-light">
                                                        <i class="icon ion-md-create"></i>
                                                    </button>
                                                </a>
                                                @endcan @can('view', $ekrafProduct)
                                                <a href="{{ route('ekraf-products.show', $ekrafProduct) }}">
                                                    <button type="button" class="btn btn-light">
                                                        <i class="icon ion-md-eye"></i>
                                                    </button>
                                                </a>
                                                @endcan @can('delete', $ekrafProduct)
                                                <form action="{{ route('ekraf-products.destroy', $ekrafProduct) }}"
                                                    method="POST"
                                                    onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')">
                                                    @csrf @method('DELETE')
                                                    <button type="submit" class="btn btn-light text-danger">
                                                        <i class="icon ion-md-trash"></i>
                                                    </button>
                                                </form>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8">
                                        @lang('crud.common.no_items_found')
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="8">
                                    {!! $ekrafProducts->render() !!}
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
