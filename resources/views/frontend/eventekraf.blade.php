@extends('frontend.layouts/main')

@section('metaseo')
    @include('meta::manager', [
        'title' => 'Event - Ekraf Cianjur',
        'description' =>
            'EKRAF CIANJUR adalah lembaga yang dibentuk untuk mendorong kolaborasi pemangku kepentingan ekonomi kreatif dalam melahirkan kreativitas dan inovasi yang memberikan nilai tambah dan meningkatkan kualitas hidup masyarakat Cianjur',
    ])
@endsection

@section('container')
    {{-- Pilih event di kecamatan karang tengah --}}
    <div class="container">
        <form action="" method="get">
            <div class="event d-md-flex gap-4 mt-5">
                <div class="dropdown">
                    <select class="btn border border-danger text-danger dropdown-toggle w-100" id="kecamatanDropdown"
                        name="kecamatan">
                        <option disabled selected>Pilih Kecamatan</option>
                        <option value="" class="text-black">Semua Kecamatan</option>
                        @foreach ($kecamatan as $kecamatans)
                            <option class="text-black" value="{{ $kecamatans->id }}">{{ $kecamatans->nama }}</option>
                        @endforeach
                    </select>
                </div>


                <div class="dropdown mt-2 mt-md-0">
                    <select class="btn border border-danger text-danger w-100 dropdown-toggle" id="kategoriDropdown"
                        name="kategori">
                        <option disabled selected>Pilih Kategori</option>
                        <option class="text-black" value="">Semua Kategori</option>
                        @foreach ($category as $categories)
                            <option class="text-black" value="{{ $categories->id }}">{{ $categories->nama }}</option>
                        @endforeach
                    </select>
                </div>

                <button class="btn btn-outline-danger bi bi-search mt-2 mt-md-0" type="submit"></button>

            </div>
        </form>
    </div>
    <div class="container">
        <div class="col">
            @if ($event->count() > 0)
                <div class="row g-4 mb-4 mt-2">
                    @foreach ($event as $events)
                        <div class="col-md-3">
                            <a href="event/detail/{{ $events->id }}/{{ Str::slug($events->title) }}"
                                class="card h-100 shadow text-decoration-none text-black">
                                <img src="{{ \Storage::url($events->image) }}" class="card-img-top" height="200px"
                                    style="object-fit: cover" alt="...">
                                <div class="card-body">
                                    <p class="text-danger mb-2">{{ $events->eventCategory->nama }}</p>
                                    <h5 class="m-0 mb-2"> {{ $events->title }}</h5>
                                    <p style="font-size: 0.8rem" class="text-muted">
                                        {{ \Carbon\Carbon::parse($events->tanggal)->locale('id')->isoFormat('dddd, D MMMM Y') }}
                                    </p>
                                    <p class="text-danger m-0"><span><i
                                                class="bi bi-geo-alt-fill"></i></span>{{ $events->alamat }}
                                    </p>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="my-5">
                    @include('frontend.partials.datanull')
                </div>
            @endif
        </div>
        <div class="align-items-center d-flex justify-content-center">
            {{ $event->render() }}
        </div>
    </div>
    <div class="container">
        <div class="event">
            <h4 class="mt-5 mb-0">Nikmati Event Di <span class="text-danger"> Minggu Ini </span></h4>
        </div>
    </div>
    <div class="container mt-3">
        <div class="col">
            @if ($weeklyEvent->count() > 0)
                <div class="row row-cols-1 row-cols-md-3 g-4 mb-5">
                    @foreach ($weeklyEvent as $weeklyEvents)
                        <div class="col-md-3">
                            <a href="event/detail/{{ $weeklyEvents->id }}/{{ Str::slug($events->title) }}"
                                class="card h-100 shadow text-decoration-none text-black">
                                <img src="{{ \Storage::url($weeklyEvents->image) }}" class="card-img-top" height="200px"
                                    style="object-fit: cover" alt="...">
                                <div class="card-body">
                                    <p class="text-danger mb-2">{{ $weeklyEvents->eventCategory->nama }}</p>
                                    <h5 class="m-0 mb-2"> {{ $weeklyEvents->title }}</h5>
                                    <p style="font-size: 0.8rem" class="text-muted">
                                        {{ \Carbon\Carbon::parse($weeklyEvents->tanggal)->locale('id')->isoFormat('dddd, D MMMM Y') }}
                                    </p>
                                    <p class="text-danger m-0"><span><i
                                                class="bi bi-geo-alt-fill"></i></span>{{ $weeklyEvents->alamat }}
                                    </p>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="text-center my-5">
                    <p style="font-size: 3rem;">:(</p>
                    <p class="mt-2 ">Tidak ada event minggu ini</p>
                </div>
            @endif
        </div>
    </div>
@endsection
