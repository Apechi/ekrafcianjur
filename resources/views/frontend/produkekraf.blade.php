@extends('frontend/layouts/main')

@section('metaseo')
    @include('meta::manager', [
        'title' => 'Produk Ekraf - Ekraf Cianjur',
        'image' => asset('image/metaimages'),
        'description' =>
            'EKRAF CIANJUR adalah lembaga yang dibentuk untuk mendorong kolaborasi pemangku kepentingan ekonomi kreatif dalam melahirkan kreativitas dan inovasi yang memberikan nilai tambah dan meningkatkan kualitas hidup masyarakat Cianjur',
    ])
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('style/pelakuekraf.css') }}">
@endsection

@section('container')
    <div class="container mt-3 mb-5">
        <div class="row mt-3">
            @include('frontend.partials.persub')

            <div class="col ps-lg-5">
                {{-- Content --}}
                <div class="row">
                    @if ($allProduk->count() > 0)
                        @foreach ($allProduk as $produk)
                            <div class="col-lg-4 p-0">
                                <div class="card m-1"
                                    style="box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px; height: 21em;">
                                    <div class="card-head p-3">
                                        <img src="{{ \Storage::url($produk->image) }}" class="card-img-top"
                                            style="height: 10em; object-fit: scale-down" alt="...">
                                        <div class="info-kategori">
                                            <p>{{ $produk->tipe }}</p>
                                        </div>
                                    </div>
                                    <div class="card-body h-25">
                                        <a class="text-decoration-none text-black"
                                            href="/ekraf/detail/{{ $produk->ekraf->id }}/produk/{{ $produk->id }}">
                                            {{ Str::substr($produk->nama, 0, 50) }}
                                            @if (strlen($produk->nama) > 50)
                                                ...
                                            @endif
                                        </a>
                                        <p class="m-0 text-danger" style="font-weight: 700">Rp
                                            {{ number_format($produk->harga_jual) }}</p>
                                        <a href="/ekraf/detail/{{ $produk->ekraf->id }}/{{ Str::slug($produk->ekraf->nama) }}"
                                            class="text-decoration-none">
                                            <p class="fw-light text-muted card-text "><span><i
                                                        class="bi bi-shop text-danger" style="font-size: 1.3rem"></i></span>
                                                {{ $produk->ekraf->nama }}</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="d-flex justify-content-center mt-2">
                            {{ $allProduk->render() }}
                        </div>
                    @else
                        <div class="null col">
                            @include('frontend.partials.datanull')
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>

    {{-- Konten --}}
@endsection
