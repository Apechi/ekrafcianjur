@extends('frontend/layouts/main')

@section('metaseo')
    @include('meta::manager', [
        'title' => 'Hubungi Kami - Ekraf Cianjur',
        'image' => asset('image/metaimages'),
        'description' =>
            'EKRAF CIANJUR adalah lembaga yang dibentuk untuk mendorong kolaborasi pemangku kepentingan ekonomi kreatif dalam melahirkan kreativitas dan inovasi yang memberikan nilai tambah dan meningkatkan kualitas hidup masyarakat Cianjur',
    ])
@endsection

@section('container')
    <div class="container">
        <h1 class="mt-5">Hubungi Kami</h1>
    </div>
    <div class="container mt-3">
        <div class="d-md-flex gap-3" style="color: #113f67">
            <div class="" style="flex: 1">
                <iframe src="//maps.google.com/maps?q={{ $information->coordinate }}&z=15&output=embed" width="100%"
                    height="100%"></iframe>

            </div>
            <div class=" d-flex flex-column mt-4 mt-md-0" style="flex: 1">
                <form action="/kontak" method="post">
                    @csrf
                    @error('nama')
                        <div class="text-danger"><i class="bi bi-exclamation fs-5"></i> Nama harus di isi</div>
                    @enderror
                    <div class="input-group input-group-sm mb-3">
                        <input type="text" placeholder="Nama" name="nama" class="form-control"
                            value="{{ old('nama') }}">
                    </div>
                    @error('email')
                        <div class="text-danger"><i class="bi bi-exclamation fs-5"></i>Email harus di isi</div>
                    @enderror
                    <div class="input-group input-group-sm mb-3">
                        <input type="text" placeholder="Email" name="email" class="form-control"
                            value="{{ old('email') }}">
                    </div>
                    @error('message')
                        <div class="text-danger"><i class="bi bi-exclamation fs-5"></i>Message harus di isi</div>
                    @enderror
                    <div class="mb-3 text-center">
                        <textarea class="form-control" rows="3" placeholder="Enter Your Message" name="message" style="height: 200px;">{{ old('message') }}</textarea>
                    </div>
                    @error('g-recaptcha-response')
                        <div class="text-danger"><i class="bi bi-exclamation fs-5"></i>reCAPTCHA harus di isi</div>
                    @enderror
                    <div class="mb-3">

                        {!! NoCaptcha::renderJs() !!}
                        {!! NoCaptcha::display() !!}
                    </div>
                    <button class="btn btn-danger ml-auto" type="submit"
                        style="margin-right: 59px; height: 38px; width: 115px;">Kirim</button>
                </form>
            </div>
        </div>
        <div class="text my-3" style="color: #113f67">
            <div class="info d-flex gap-3">
                <i class="bi bi-pin"></i>
                <p>{{ $information->address }}</p>
            </div>
            <div class="info d-flex gap-3">
                <i class="bi bi-envelope"></i>
                <p>{{ $information->email }}</p>
            </div>
            <div class="info d-flex gap-3">
                <i class="bi bi-telephone"></i>
                <p>{{ $information->no_telp }}</p>
            </div>
        </div>
    </div>
@endsection
