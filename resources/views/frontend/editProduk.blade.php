@extends('frontend.layouts.main')

@section('metaseo')
    @include('meta::manager', [
        'title' => 'Edit Produk - ' . $produk->nama,
        'image' => asset('image/metaimages'),
    ])
@endsection

@section('container')
    <div class="container p-5 shadow rounded rounded-3">

        <form method="POST" action="{{ route('products.update', $produk->id) }}" class="row gap-3"
            enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="col-md-12">

                <select name="ekraf_id" class="btn btn-outline-danger w-100" id="">
                    <option value="" selected disabled>Pilih Ekraf</option>
                    @foreach ($allEkraf as $ekraf)
                        <option {{ $produk->ekraf_id == $ekraf->id ? 'selected' : '' }} value="{{ $ekraf->id }}">
                            {{ $ekraf->nama }}</option>
                    @endforeach
                    @error('ekraf_id')
                        <div class="alert alert-primary" role="alert">
                            <strong>Alert</strong> {{ $message }}
                        </div>
                    @enderror
                </select>
            </div>
            <div class="logo-img" x-data="imageViewer()">
                <label class="mb-2">Upload Logo:</label>
                <div class="upload-image">
                    <div x-data="imageViewer()">
                        <div class="mb-2 row-cols-1">
                            <!-- Show the image -->
                            <template x-if="imageUrl">
                                <img :src="imageUrl" class="rounded"
                                    style="width: 20em; height: 20em; object-fit: cover; border: 1px dashed red"
                                    @click="triggerFileInput">
                            </template>

                            <!-- Show the gray box when image is not available -->
                            <template x-if="!imageUrl">

                                <img src="{{ \Storage::url($produk->image) }}" class="rounded"
                                    style="width: 20em; height: 20em; object-fit: cover; border: 1px dashed red"
                                    @click="triggerFileInput">
                            </template>

                            <!-- Image file selector -->
                            <input class="mt-2" type="file" name="image" accept="image/*" @change="fileChosen"
                                style="display: none" x-ref="fileInput">

                            <p class="text-muted" style="font-weight: 500; max-width: 20em;">Format gambar
                                dengan ukuran

                                minimum 300 x 300px (Untuk
                                gambar lebih optimal
                                gunakan ukuran minimum 700 x 700 px)</p>
                            @error('image')
                                <div class="alert alert-primary" role="alert">
                                    <strong>Alert</strong> {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <label for="inputproduk" class="form-label">Nama Produk</label>
                <input type="text" value="{{ $produk->nama }}" name="nama" required class="form-control"
                    id="inputproduk">
                @error('nama')
                    <div class="alert alert-primary" role="alert">
                        <strong>Alert</strong> {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="col-md-12">
                <div class="">
                    <label for="inputproduk" class="form-label">Deskripsi</label>
                </div>
                <textarea name="desc" class="form-control" id="" cols="40" rows="10">{{ $produk->desc }}</textarea>
                @error('desc')
                    <div class="alert alert-primary" role="alert">
                        <strong>Alert</strong> {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="col-md-12">
                <label for="inputtype" class="form-label">Nama Tipe</label>
                <input type="text" name="tipe" value="{{ $produk->tipe }}" required class="form-control"
                    id="inputtype">
                @error('tipe')
                    <div class="alert alert-primary" role="alert">
                        <strong>Alert</strong> {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="col-md-12">
                <label for="selectEvent" class="form-label">Harga Jual</label>
                <input type="number" name="harga_jual" value="{{ $produk->harga_jual }}" class="form-control"
                    id="inputproduk">
                @error('harga_jual')
                    <div class="alert alert-primary" role="alert">
                        <strong>Alert</strong> {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="ekspor">
                <label for="">Dapat di Ekspor?</label>
                <div class="form-check mx-3">
                    <input class="form-check-input" {{ $produk->ekspor == 'Ya' ? 'checked' : '' }} name="ekspor"
                        type="radio" value="Ya" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                        Ya
                    </label>
                </div>
                <div class="form-check mx-3">
                    <input class="form-check-input" name="ekspor" type="radio"
                        {{ $produk->ekspor == 'Tidak' ? 'checked' : '' }} value="Tidak" id="flexCheckChecked">
                    <label class="form-check-label" for="flexCheckChecked">
                        Tidak
                    </label>
                </div>
                @error('ekspor')
                    <div class="alert alert-primary" role="alert">
                        <strong>Alert</strong> {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="modal-bawah col-12 p-3 gap-3 d-flex justify-content-end">
                <a href="/manage/produk" class="text-decoration-none">
                    <button type="button" class="btn btn-light">Batalkan</button>
                </a>
                <button type="submit" class="btn btn-danger">Simpan</button>
            </div>
    </div>
    </form>
@endsection
