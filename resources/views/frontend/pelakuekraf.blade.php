@extends('frontend/layouts/main')

@section('metaseo')
    @include('meta::manager', [
        'title' => 'Pelaku Ekraf - Ekraf Cianjur',
        'image' => asset('image/metaimages'),
        'description' =>
            'EKRAF CIANJUR adalah lembaga yang dibentuk untuk mendorong kolaborasi pemangku kepentingan ekonomi kreatif dalam melahirkan kreativitas dan inovasi yang memberikan nilai tambah dan meningkatkan kualitas hidup masyarakat Cianjur',
    ])
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('style/pelakuekraf.css') }}">
@endsection

@section('container')
    <div class="container mt-3 mb-5">
        <div class="row">
            @include('frontend.partials.persub')
            <div class="col ps-lg-5">
                {{-- Content --}}
                <div class="row ">
                    @if ($allEkraf->count() > 0)
                        @foreach ($allEkraf as $ekraf)
                            <div class="col-lg-4 p-0">
                                <a href="/ekraf/detail/{{ $ekraf->id }}/{{ Str::slug($ekraf->nama) }}" class="">
                                    <div class="card m-1"
                                        style="box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px; height: 19em; ">
                                        <div class="card-head p-3">
                                            <img src="{{ \Storage::url($ekraf->logo) }}" class="card-img-top"
                                                style="height: 10em; object-fit: scale-down" alt="...">
                                            <div class="info-kategori">
                                                <p>{{ $ekraf->category->nama }}</p>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <a href="/ekraf/detail/{{ $ekraf->id }}/{{ Str::slug($ekraf->nama) }}"
                                                class="text-decoration-none text-black">{{ $ekraf->nama }}</a>
                                            <p class="fw-light text-muted card-text fs-6 "><span><i
                                                        class="bi text-danger bi-geo-alt-fill"></i></span>
                                                {{ Str::substr($ekraf->alamat, 0, 50) }}
                                                @if (strlen($ekraf->alamat) > 55)
                                                    ...
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                        <div class="d-flex justify-content-center mt-2">
                            {{ $allEkraf->render() }}
                        </div>
                    @else
                        <div class="col">
                            @include('frontend.partials.datanull')
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>

    {{-- Konten --}}
@endsection
