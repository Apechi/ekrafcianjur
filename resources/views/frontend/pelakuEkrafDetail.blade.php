@extends('frontend/layouts/main')



@section('style')
    <link rel="stylesheet" href="{{ asset('style/pelakuekraf.css') }}">

    <style>
        .card-wrapper {
            transition-timing-function: ease-in-out;
            transition: 0.3s;
        }

        .card-wrapper:hover {
            background-color: rgb(237, 232, 253)
        }
    </style>
@endsection

@section('container')
    <div class="container mt-4">
        {{-- Detail Produk Ekraf --}}

        <div class="p-3 p-md-0">
            <div class="row rounded shadow p-3">
                <div class="col-auto">
                    <img src="{{ \Storage::url($ekraf->logo) }}" style=" height:64px" alt="" srcset=""
                        style="object-fit: contain">
                </div>
                <div class="col">
                    <a class="text-decoration-none text-black"
                        href="/ekraf/detail/{{ $ekraf->id }}/{{ Str::slug($ekraf->nama) }}">
                        <div class="fw-bold">{{ $ekraf->nama }}</div>
                    </a>
                    <div class="text-muted text-smaller">{{ $ekraf->category->nama }}</div>
                </div>
                <div class="col-auto mt-3 mt-lg-0">
                    <span class="primary"><i class="bi bi-geo-alt-fill px-1"></i>{{ $ekraf->alamat }}</span>
                </div>
            </div>

            <div class="row rounded p-2" style="background-color:rgb(240, 240, 240)">
                <div class="col-md mb-4">
                    <div class="text-muted">Nama Pemilik</div>
                    <span class="fw-bold">{{ $ekraf->nama_pemilik }}</span>
                </div>
                <div class="col-md mb-4">
                    <div class="text-muted">no. HP</div>
                    <span class="fw-bold">{{ $ekraf->no_telp }}</span>
                </div>
                <div class="col-md mb-4">
                    <div class="text-muted">Email</div>
                    <span class="fw-bold">{{ $ekraf->email }}</span>
                </div>
                <div class="col-md mb-4">
                    <div class="text-muted">Website</div>
                    <span class="fw-bold">{{ $ekraf->website ?? '-' }}</span>
                </div>
                <div class="col-md mb-4">
                    <div class="text-muted">Jumlah Produk</div>
                    <span class="fw-bold">{{ $ekraf->ekrafProducts->count() }}</span>
                </div>
            </div>
        </div>

        @yield('detailbody')




        <hr>

    </div>

    {{-- Konten --}}
@endsection
