@extends('frontend.layouts.mainLogin')
@section('metaseo')
    @include('meta::manager', [
        'title' => 'Login',
        'image' => asset('image/metaimages'),
    ])
@endsection
@section('container')
    <div class="px-3 px-lg-0 m-3" style="height:100vh">
        <div class="row h-100 align-items-center justify-content-center ">
            <div class="col-lg-5">
                <div class="card shadow">
                    <div class="card-body d-flex flex-column align-items-center">
                        <img class="mb-4" src="\image\img0.png" width="100px" srcset="">
                        <form method="POST" action="{{ route('login') }}" class="w-100 px-5">
                            @csrf
                            <div class="form-floating mb-3">
                                <input type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email" autofocus
                                    placeholder="Email">
                                <label for="floatingInput">Email Address</label>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating">
                                <input type="password" class="form-control @error('password') is-invalid @enderror"
                                    name="password" required autocomplete="current-password" id="floatingPassword"
                                    placeholder="Password">
                                <label for="floatingPassword">Password</label>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-check m-3">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember">
                                <label class="form-check-label" for="remember">
                                    Ingat Saya
                                </label>
                            </div>
                            <div class="text-center mt-3">
                                <button type="submit" class="btn btn-danger w-100">Log In</button>
                            </div>
                            @if (Route::has('password.request'))
                                <div>
                                    <p class="mt-4 text-center"><a class=" text-danger"
                                            href="{{ route('password.request') }}">Lupa Password?</a>
                                    </p>
                                </div>
                            @endif
                        </form>

                    </div>
                    <div class="d-flex gap-2 justify-content-end align-items-center px-5"
                        style="height:75px; width:100%; background-color:rgb(226, 226, 226)">
                        <p class="mb-0 text-decoration-none text-black">Belum punya akun?
                        </p>
                        <a href="register" class="mb-0 text-danger" style="font-weight: bold">REGISTER</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
