@extends('frontend.layouts.main')

<!-- Button untuk memicu modal -->
<button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#myModal">
  Booking gedung ini
</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-danger text-white text-center">
          <h5 class="modal-title" id="exampleModalLabel">Mari temukan booking gedung anda</h5>
          <button type="button" class="btn-close " data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form class="row g-3">
            <div class="col-md-12">
              <label for="inputEmail4" class="form-label my-2">Nama Lengkap</label>
              <input type="email" class="form-control" id="inputEmail4">
            </div>
            <div class="col-md-12">
              <label for="inputName" class="form-label my-2">Email</label>
              <input type="text" class="form-control" id="inputName">
            </div>

            {{-- belum beres --}}
            <div class="col-md-12">
              <div class="row justify-content-center">
                <div class="col-md-12">
                  <form action="#" method="POST">
                    <div class="mb-3">
                      <label for="tanggal" class="my-2">Tanggal:</label>
                      <input type="date" id="tanggal" name="tanggal" class="form-control" required>
                    </div>
                    <div class="mb-3">
                      <label for="waktu" class="my-2">Waktu:</label>
                      <input type="time" id="waktu" name="waktu" class="form-control" required>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            
            <div class="col-md-12 my-2">
              <label for="selectEvent" class="form-label my-2">pembayaran</label>
              <select class="form-select w-100" id="selectEvent">
                <option selected>Pilih Angka Pembayaran</option>
                <option value="1">Rp.30.000.00</option>
                <option value="2">Rp.100.000.00</option>
                <option value="3">Rp.500.000.00</option>
              </select>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tutup</button>
          <button type="button" class="btn btn-danger">Simpan</button>
        </div>
      </div>
    </div>
  </div>