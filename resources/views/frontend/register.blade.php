@extends('frontend.layouts.mainLogin')
@section('metaseo')
    @include('meta::manager', [
        'title' => 'Daftarkan',
        'image' => asset('image/metaimages'),
    ])
@endsection
@section('container')
    <div class="px-3 m-3" style="height:100vh">
        <div class="row h-100 align-items-center justify-content-center ">
            <div class="col-md-5">
                <div class="card shadow">
                    <div class="card-body d-flex flex-column align-items-center">
                        <img class="mb-4" src="\image\img0.png" width="100px" srcset="">
                        <form method="POST" action="{{ route('register') }}" class="w-100 px-3">
                            @csrf
                            <div class="form-floating mb-3">
                                <input id="floatingInput" type="text"
                                    class="form-control @error('name') is-invalid @enderror" name="name"
                                    value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nama">

                                <label for="floatingInput">Nama</label>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <input id="floatingInput" type="email"
                                    class="form-control @error('email') is-invalid @enderror" name="email"
                                    value="{{ old('email') }}" required autocomplete="email" placeholder="Email Address">

                                <label for="floatingInput">Email Address</label>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <input id="floatingPassword" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password" required
                                    autocomplete="new-password" placeholder="Password">

                                <label for="floatingPassword">Password</label>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-floating mb-3">
                                <input id="floatingPassword" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password"
                                    placeholder="Confirm Password">
                                <label for="floatingPassword">Confirm Password</label>
                            </div>

                            <div class="form-floating mb-3">
                                <select class="form-select" name="category_id" id="floatingSelect"
                                    aria-label="Floating label select example">
                                    <option selected disabled>Pilih Kategori</option>
                                    @foreach ($category as $categories)
                                        <option value="{{ $categories->id }}">{{ $categories->nama }}</option>
                                    @endforeach
                                </select>
                                <label for="floatingSelect">Jenis Usaha</label>
                                @error('error')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>


                            <div class="form-floating mb-3"
                                style="transform:scale(0.8);
                            transform-origin:0 0;">
                                {!! NoCaptcha::renderJs() !!}
                                {!! NoCaptcha::display() !!}

                            </div>
                            @error('g-recaptcha-response')
                                <div class="alert text-danger m-0 p-0" role="alert">
                                    <strong>reCAPTCHA field is required</strong>
                                </div>
                            @enderror

                            <div class="text-center mt-3 mb-3">
                                <button type="submit" class="btn btn-danger w-100">Register</button>
                            </div>
                        </form>
                    </div>
                    <div class="d-flex gap-3 justify-content-end align-items-center px-5"
                        style="height:75px; width:100%; background-color:rgb(226, 226, 226)">
                        <p class="mb-0">Sudah punya akun?</p>
                        <a class="mb-0 text-danger" href="login" style="font-weight: bold">LOGIN</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
