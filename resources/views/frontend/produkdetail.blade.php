@extends('frontend/layouts/main')

@section('style')
    <link rel="stylesheet" href="{{ asset('style/pelakuekraf.css') }}">
@endsection

@section('container')
    <div class="container mt-5">
        {{-- Detail Produk Ekraf --}}


        <div class="p-3 p-md-0">
            <div class="row rounded shadow p-3">
                <div class="col-auto">
                    <img src="\image\img6.png" style=" height:64px" alt="" srcset="">
                </div>
                <div class="col">
                    <div class="fw-bold">Nama Pelaku Ekraf</div>
                    <div class="text-muted text-smaller">Kategori</div>
                </div>
                <div class="col-md-2 mt-3 mt-md-0">
                    <span class="primary"><i class="bi bi-geo-alt-fill"></i>di Warung LEbak</span>
                </div>
            </div>

            <div class="row rounded p-2" style="background-color:rgb(240, 240, 240)">
                <div class="col-md mb-4">
                    <div class="text-muted">Nama Pemilik</div>
                    <span class="fw-bold">opik</span>
                </div>
                <div class="col-md mb-4">
                    <div class="text-muted">no. HP</div>
                    <span class="fw-bold">0857546363</span>
                </div>
                <div class="col-md mb-4">
                    <div class="text-muted">Email</div>
                    <span class="fw-bold">opik@gmail.com</span>
                </div>
                <div class="col-md mb-4">
                    <div class="text-muted">Website</div>
                    <span class="fw-bold">LoliChan</span>
                </div>
                <div class="col-md mb-4">
                    <div class="text-muted">Jumlah Produk</div>
                    <span class="fw-bold">20</span>
                </div>
            </div>


        </div>

        <div class="produk-list row p-3">
            <div class="bg-light rounded col-flex col-lg-3 col-sm-6">
                <div class="product-box text-decoration-none">
                    <img class="img-fluid"
                        src="https://kreasijabar.id/uploads/ekraf_products/fa65fa215f8453a3cd11a130246863e5.png">
                    <div class="product-info mt-3 text-black">
                        <a href="http://" class="text-decoration-none text-black">
                            <h4 class="m-0">produk 1</h4>
                        </a>
                        <p id="desc" class="collapse mt-2">Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                            Voluptates, ipsum.</p>
                    </div>
                </div>
                <div class="input">

                    <button class="btn btn-link arrow-button w-100" data-bs-toggle="collapse" data-bs-target="#desc"
                        aria-expanded="false" aria-controls="desc">
                        <i class="bi bi-chevron-down"></i>
                    </button>
                </div>
            </div>
        </div>

        <hr>

    </div>

    {{-- Konten --}}
@endsection
