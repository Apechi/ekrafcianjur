@extends('frontend/layouts/main')

@section('metaseo')
    @include('meta::manager', [
        'title' => 'User Setting - Ekraf Cianjur',
        'image' => asset('image/metaimages'),
    ])
@endsection

@section('container')
    {{-- @section('style')
<link rel="stylesheet" href="{{ asset('style/kelolaekraf.css') }}">
@endsection --}}

    <div class="container shadow p-4">
        <h4 class="4 fw-bold">Detail Profile User</h4>
        <hr>
        <form class="dasboard-cruds-main" method="post" action="/auth/usersetting/update" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="main-form media">
                <div class="form-fields mt-2 ">
                    <div class="form-group">
                        <label class="mt-2">Nama Lengkap</label>
                        <input class="form-control" type="text" name="nama_lengkap" placeholder=""
                            value="{{ $user->nama_lengkap }}">
                        @error('nama_lengkap')
                            <div class="alert alert-primary" role="alert">
                                <strong>Alert</strong> {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group mt-3">
                        <label class="mt-2">Email</label>
                        <input class="form-control" type="text" name="email" placeholder=""
                            value="{{ $user->email }}">
                        @error('email')
                            <div class="alert alert-primary" role="alert">
                                <strong>Alert</strong> {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="fs-3 mt-4">Ubah Kata Sandi Anda</div>
                    <p style="font-style: italic">Silahkan kosongi bagian ini bila tidak ingin mengubah kata sandi.</p>
                    <div class="form-group mt-3">
                        <label class="mt-2">Kata Sandi Lama</label>
                        <input class="form-control" type="text" name="old_password" placeholder="">
                        @error('old_password')
                            <div class="alert alert-primary" role="alert">
                                <strong>Alert</strong> {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group mt-3">
                        <label class="mt-2">Masukkan Kata Sandi Baru</label>
                        <input class="form-control" type="text" name="password" placeholder="">
                        @error('password')
                            <div class="alert alert-primary" role="alert">
                                <strong>Alert</strong> {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group mt-3">
                        <label class="mt-2">Konfirmasi Kata Sandi Baru</label>
                        <input class="form-control" type="text" name="password_confirmation" placeholder="">
                        @error('password_confirmation')
                            <div class="alert alert-primary" role="alert">
                                <strong>Alert</strong> {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <input type="hidden" name="category_id" value="{{ $user->category_id }}">
                @error('category_id')
                    <div class="alert alert-primary" role="alert">
                        <strong>Alert</strong> {{ $message }}
                    </div>
                @enderror
                @error('roles')
                    <div class="alert alert-primary" role="alert">
                        <strong>Alert</strong> {{ $message }}
                    </div>
                @enderror
                <div class="d-flex justify-content-end mt-3">
                    <a href="/manage/ekraf" class="text-decoration-none">
                        <button type="button" class="btn btn-light  float-right me-3">Batalkan</button>
                    </a>
                    <button type="submit" class="btn btn-danger float-right me-3">Simpan</button>
                </div>
            </div>
        </form>
    </div>
@endsection
