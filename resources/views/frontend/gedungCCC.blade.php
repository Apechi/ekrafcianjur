@extends('frontend/layouts/main')

@section('metaseo')
    @include('meta::manager', [
        'title' => 'Gedung Creative Center Cianjur - Ekraf Cianjur',
        'image' => asset('image/metaimages'),
        'description' =>
            'EKRAF CIANJUR adalah lembaga yang dibentuk untuk mendorong kolaborasi pemangku kepentingan ekonomi kreatif dalam melahirkan kreativitas dan inovasi yang memberikan nilai tambah dan meningkatkan kualitas hidup masyarakat Cianjur',
    ])
@endsection

@section('banner')
    {{-- Carousel Img --}}
    @include('frontend.partials.curoselgedung')
@endsection

@section('container')
    <div class="container">
        <div class="row mt-5 mb-3 mb-md-5 justify-content-center">
            <div class="col-md-5 pb-3 pb-md-0">
                <h4 class="align-self-start">Kegiatan Gedung</h4>
                {!! $gedung->kegiatan ?? '' !!}
            </div>
            <div class="col-md-7 row p-0">
                <div class="col-7" style="">
                    <div class="img-wrapper">
                        <img class="rounded-4" src="{{ \Storage::url($gedung->gambar[0]->image) ?? '' }}"
                            style="width: 100%; height:350px; object-fit: cover" alt="">
                    </div>
                </div>
                <div class="col-5">
                    <div class="row-2 mb-2 ">
                        <img class="rounded-4" src="{{ \Storage::url($gedung->gambar[1]->image) ?? '' }}"
                            style="object-fit: cover" width="100%" height="170px" alt="">
                    </div>
                    <div class="row-2 ">
                        <img class="rounded-4" src="{{ \Storage::url($gedung->gambar[2]->image) ?? '' }}"
                            style="object-fit: cover" width="100%" height="170px" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-7">
                <img class="rounded-4" src="{{ \Storage::url($gedung->gambar[3]->image) ?? '' }}" style="object-fit: cover"
                    width="100%" height="400px" alt="">
            </div>
            <div class="col-md-4 mt-3 mt-md-0">
                <div class="fasilitas">
                    <h2 class="mb-3">Fasilitas Gedung</h2>
                    <div class="falitisas-list row">
                        @foreach ($gedung->fasilitas as $item)
                            <p class="col-6 col-md-12"><span class="me-3"><i
                                        class="bi bi-chevron-down"></i></span>{{ $item->fasilitas ?? '' }}
                            </p>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
        <div class="col text-center">
            <a href="https://www.linkedin.com/company/madtive-studio/" target="_blank" class="text-decoration-none">
                <button class="btn btn-warning fs-5 text-white mb-5">Booking Gedung</button>
            </a>
        </div>
        <h3 class="text-center mt-5 mb-3"> Hubungi Kami </h3>
        <div class="row p-0 mb-5">
            <div class="col-md-6 p-2">
                <div class="rounded border bg-light  text-center p-4" style="height: 20em">
                    <i class="bi bi-geo-alt-fill text-danger" style="font-size: 50px"></i>
                    <h2 class="m-0 mt-3">Alamat</h2>
                    <div class="fs-6 text-muted">{{ $gedung->alamat ?? '' }}</div>
                    <a href="https://www.google.com/maps/search/?api=1&query={{ urlencode($gedung->alamat) ?? '' }}"
                        target="blank">
                        <button class="btn btn-danger fs-6 mt-4"><span><i class="bi bi-send-fill"></i></span> Lihat
                            Map</button>
                    </a>
                </div>
            </div>
            <div class="col-md-6 p-2">
                <div class="rounded border bg-light  text-center p-4" style="height: 20em">
                    <i class="bi bi-whatsapp text-danger" style="font-size: 50px"></i>
                    <h2 class="m-0 mt-3">WhatsApp</h2>
                    <div class="fs-6 text-muted">Ada pertanyaan ? Yuk konsultasikan dengan tim kami melalui WA / Telp
                    </div>
                    <a href="http://wa.me/{{ $gedung->no_telp ?? '' }}" target="blank">
                        <button class="btn btn-danger fs-6 mt-4"><span><i class="bi bi-send-fill"></i></span> Kontak
                            Kami</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
