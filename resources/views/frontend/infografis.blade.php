@extends('frontend/layouts/main')

@section('metaseo')
    @include('meta::manager', [
        'title' => 'Infografis - Ekraf Cianjur',
        'description' =>
            'EKRAF CIANJUR adalah lembaga yang dibentuk untuk mendorong kolaborasi pemangku kepentingan ekonomi kreatif dalam melahirkan kreativitas dan inovasi yang memberikan nilai tambah dan meningkatkan kualitas hidup masyarakat Cianjur',
        'image' => asset('image/metaimages'),
    ])
@endsection

@section('container')
    <div class="container my-5">
        <div class="row ">
            <div class="col-lg-3 h-100 p-0 shadow rounded">
                <a class="d-flex text-decoration-none align-items-center"></a>
                <ul class="nav flex-column">
                    <form id="myForm" class="isikategori pb-1 px-2" action="" method="GET">
                        <li class="nav-item pb-1">
                            <a class="ms-2 d-flex text-primary text-decoration-none p-2" data-bs-toggle="collapse"
                                href="#Kecamatan" role="button" aria-expanded="true" aria-controls="collapseExample">
                                <span class="me-auto d-sm-inline">Kecamatan</span>
                                <i class="bi bi-chevron-down"></i>
                            </a>
                            <div class="ms-2 mt-2 collapse mb-3 show" id="Kecamatan">
                                <div class="scrollable-list px-2"
                                    style="max-height: 100vh; overflow-y: auto; overflow-x: hidden">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="kecamatan" value=""
                                            {{ $allKecamatan->first()->id == $kecamatanFilter ? '' : 'checked' }}
                                            id="kecamatan_all">
                                        <label class="form-check-label" for="kecamatan_all">
                                            Semua Kecamatan
                                        </label>
                                    </div>
                                    @foreach ($allKecamatan as $kecamatan)
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="kecamatan"
                                                {{ $kecamatan->id == $kecamatanFilter ? 'checked' : '' }}
                                                value="{{ $kecamatan->id }}" id="kecamatan_{{ $kecamatan->id }}">
                                            <label class="form-check-label" for="kecamatan_{{ $kecamatan->id }}">
                                                {{ $kecamatan->nama }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <div class="nav-link active d-flex text-primary ps-1" aria-current="page">
                                <button type="submit" class="btn btn-primary"><i class="bi bi-filter"></i> Filter</button>
                            </div>
                        </li>
                    </form>
                </ul>
            </div>

            <div class="col border p-3 ms-md-5 mt-3 mt-md-0">
                <div class="kecamatan">
                    <h4>Total Ekraf di <span class="text-danger">
                            @if ($dataKecamatan->where('id', $request->kecamatan)->count() < 1)
                                Semua Kecamatan
                            @else
                                {{ $dataKecamatan->where('id', $request->kecamatan)->first()->nama }}
                            @endif
                        </span>
                    </h4>
                    <div class="chart mt-3 card p-2" style="position: relative; height:50%; width:100%">
                        <canvas class="img-fluid" id="kecamatanChart"></canvas>
                    </div>
                </div>
                <div class="subsektor mt-4">
                    <h4>Total Sub Sektor di <span class="text-primary">
                            @if ($dataKecamatan->where('id', $request->kecamatan)->count() < 1)
                                Semua Kecamatan
                            @else
                                {{ $dataKecamatan->where('id', $request->kecamatan)->first()->nama }}
                            @endif
                        </span>
                    </h4>
                    <div class="chart mt-3 card p-2" style="position: relative; height:50%; width:100%">
                        <canvas class="img-fluid" id="subsektorChart"></canvas>
                    </div>
                </div>
                <div class="kategori mt-4">
                    <h4>Total Kategori Ekraf di <span class="text-warning">
                            @if ($dataKecamatan->where('id', $request->kecamatan)->count() < 1)
                                Semua Kecamatan
                            @else
                                {{ $dataKecamatan->where('id', $request->kecamatan)->first()->nama }}
                            @endif
                        </span>
                    </h4>
                    <div class="chart mt-3 card p-2" style="position: relative; height:50%; width:100%">
                        <canvas class="img-fluid" id="kategoriChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        const ctx = document.getElementById('kecamatanChart');
        let dataKecamatan = []

        @foreach ($dataKecamatan as $kecamatan)
            dataKecamatan.push({
                label: "{{ $kecamatan->nama }}",
                value: "{{ $kecamatan->allEkraf->count() }}"
            });
        @endforeach

        let labelKecamatan = []
        let valueKecamatan = []

        dataKecamatan.forEach((item) => {
            labelKecamatan.push(item.label);
            valueKecamatan.push(item.value);
        });


        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labelKecamatan,
                datasets: [{
                    label: 'Total Ekraf',
                    data: valueKecamatan,
                    backgroundColor: ['#FF6384'],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                },
                responsive: true,
                maintainAspectRatio: false

            }
        });
    </script>
    <script>
        const ctxSub = document.getElementById('subsektorChart');
        let dataSubsektor = []

        @foreach ($dataSubsektor as $subsektors)
            dataSubsektor.push({
                label: "{{ $subsektors->nama }}",
                value: "{{ $subsektors->allEkraf->count() }}"
            });
        @endforeach

        let labelsubsektor = []
        let valuesubsektor = []

        dataSubsektor.forEach((item) => {
            labelsubsektor.push(item.label);
            valuesubsektor.push(item.value);
        });


        new Chart(ctxSub, {
            type: 'bar',
            data: {
                labels: labelsubsektor,
                datasets: [{
                    label: 'Total Ekraf',
                    data: valuesubsektor,
                    backgroundColor: ['#007bff'],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                },
                responsive: true,
                maintainAspectRatio: false

            }
        });
    </script>
    <script>
        const ctxKat = document.getElementById('kategoriChart');
        let dataKategori = []

        @foreach ($dataKategori as $katogoris)
            dataKategori.push({
                label: "{{ $katogoris->nama }}",
                value: "{{ $katogoris->allEkraf->count() }}"
            });
        @endforeach

        let labelkategori = []
        let valuekategori = []

        dataKategori.forEach((item) => {
            labelkategori.push(item.label);
            valuekategori.push(item.value);
        });


        new Chart(ctxKat, {
            type: 'bar',
            data: {
                labels: labelkategori,
                datasets: [{
                    label: 'Total Ekraf',
                    data: valuekategori,
                    backgroundColor: ['#ffc107'],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                },
                responsive: true,
                maintainAspectRatio: false

            }
        });
    </script>

    <script>
        const radioButtons = document.querySelectorAll('input[type="radio"][name="kecamatan"]');
        radioButtons.forEach(function(radioButton) {
            radioButton.addEventListener('click', function() {
                document.getElementById('myForm').submit();
            });
        });
    </script>
@endsection
