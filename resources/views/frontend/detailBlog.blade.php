@extends('frontend.layouts.main')

@section('metaseo')
    @include('meta::manager', [
        'title' => $artikel->title,
        'description' => strip_tags(Str::substr($artikel->body, 0, 150)) . '...',
        'image' => Storage::url($artikel->image),
    ])
@endsection

@section('container')
    <div class="container mt-5">
        <div class="col mb-5">
            <div>
                <img src="{{ \Storage::url($artikel->image) }}" class="rounded" width="100%" height="400px"
                    style="object-fit: cover" alt="">
            </div>
        </div>

        <div class="col" style="margin-bottom:200px">
            <div class="row">
                <div class="col-md-7 border-end">
                    <h1>{{ $artikel->title }}</h1>
                    <p class="text-black-50">{{ $artikel->createdDate }}</p>
                    <div class="mt-3" style="font-weight: 400">
                        {!! $artikel->body ?? '-' !!}
                    </div>
                    <div class="details mt-3">
                        <h6>Penulis: {{ $artikel->writer }} </h6>
                    </div>
                </div>
                {{-- Komentar --}}
                <div class="col-md-4 ms-md-4 mt-3 mt-md-0">
                    <h5>Komentar</h5>
                    <hr>
                    <div class="col">
                        <div id="disqus_thread"></div>
                        <script>
                            /**
                             *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                             *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */

                            var disqus_config = function() {
                                this.page.url = '{{ url()->current() }}'; // Replace PAGE_URL with your page's canonical URL variable
                                this.page.identifier =
                                    'artikel/{{ $artikel->id }}'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                            };

                            (function() { // DON'T EDIT BELOW THIS LINE
                                var d = document,
                                    s = d.createElement('script');
                                s.src = 'https://ekrafcianjur.disqus.com/embed.js';
                                s.setAttribute('data-timestamp', +new Date());
                                (d.head || d.body).appendChild(s);
                            })
                            ();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments
                                powered by Disqus.</a></noscript>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
