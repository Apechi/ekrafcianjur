@extends('frontend.layouts.main')

@section('metaseo')
    @include('meta::manager', [
        'title' => 'Management - Ekraf Cianjur',
        'image' => asset('image/metaimages'),
    ])
@endsection


@section('container')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-xl-2 border border-1 @desktop
min-vh-100
@enddesktop">
                <div class="d-flex flex-column align-items-start px-3 p-3 text-white">
                    <a href="/" class="d-flex pb-3 mb-md-3 me-md-auto">
                        <img src="/image/img0.png" style="height: 50%;" width="70%;">
                    </a>
                    <ul class="nav flex-column mb-sm-auto mb-0 align-items-start" id="menu">
                        <li class="nav-item">
                            <a href="/" class="nav-link px-0">
                                <i class="bi bi-house-door-fill text-danger fs-5"></i> <span
                                    class="ms-1 d-sm-inline text-danger">Home</span>
                            </a>
                        </li>

                        <li class="w-100">
                            <a href="/manage/ekraf" class="nav-link px-0 pt-0"><i
                                    class="bi bi-list text-danger fs-5"></i><span
                                    class="d-sm-inline text-danger  mx-2">Daftar
                                    Ekraf</span></a>
                        </li>
                        <li class="w-100">
                            <a href="/manage/produk" class="nav-link px-0 pt-0"> <i class="bi bi-shop text-danger fs-5">
                                </i> <span class="d-sm-inline text-danger mx-1">Produk Ekraf</span></a>
                        </li>

                        <li>
                            <a href="/auth/usersetting" class="text-decoration-none my-2">
                                <i class="bi bi-gear-fill text-danger fs-5"></i> <span
                                    class="ms-1 d-sm-inline text-danger mx-5">Setting</span></a>
                        </li>
                        <li>
                            <form action="{{ route('logout') }}" method="post">
                                @csrf
                                <button class="my-2 btn border border-0 p-0" type="submit"><i
                                        class="bi bi-box-arrow-left text-danger fs-5"></i>
                                    <span class="ms-1 d-sm-inline text-danger mx-5">Logout</span></a></button>

                            </form>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-12 ps-4 col-xl-10 mt-3 mb-5">
                @yield('menu')
            </div>
        </div>
    </div>
@endsection
