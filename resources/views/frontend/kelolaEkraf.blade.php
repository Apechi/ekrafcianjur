@extends('frontend/layouts/main')

@section('container')
@section('style')
<link rel="stylesheet" href="{{ asset('style/kelolaekraf.css') }}">
@endsection

<div class="container">
  <hr>
  <h4 class="mx-3 my-5">Registrasi Usaha/Kegiatan Ekraf Baru </h4>
  <form class="dasboard-cruds-main" method="POST" enctype="multipart/form-data">
    <div class="main-form media">
      <div class="logo-img">
        <label>Upload Logo(*)</label>
        <div class="upload-img">
          <label>
            <input type="file" id="input-photo" name="photo" accept=".jpg,.jpeg,.png">
          </label>
          <img class="preview-img" src="" style="display:none;" />
          <div class="content">
            <img src="\image\231392.webp">
            <b>+ Pilih File Logo</b>
          </div>
        </div>
        <div class="texts">
          <p>
            Format gambar .jpg .jpeg .png dan ukuran minimum 300 x 300px (Untuk gambar optimal gunakan ukuran minimum 700 x 700 px)
          </p>
        </div>
      </div>
      <div class="form-fields">
        <div class="form-group">
          <label>Nama Usaha/Kegiatan(*)</label>
          <input class="form-control" type="text" name="title" placeholder="" value="">
        </div>
        <div class="form-group">
          <label>Kategori Usaha(*)</label>
          <select class="custom-select form-control" name="ekraf_category_id">
            <option value="" selected>Pilih Kategori</option>
            <option value="1">Pelaku Bisnis</option>
            <option value="2">Komunitas Bisnis</option>
            <option value="3">Usaha/Bisnis</option>
            <option value="4">Lembaga Pendidikan</option>
          </select>
        </div>
        <div class="form-group">
          <label>Sub Sektor Usaha(*)</label>
          <select class="custom-select form-control" name="sector_id">
            <option value="" selected>Pilih Sub Sektor</option>
            <option value="1">Kuliner</option>
            <option value="2">Fashion</option>
          </select>
        </div>
        <div class="form-group">
          <label>Alamat Usaha/Kegiatan(*)</label>
          <input class="form-control" type="text" name="location_address" placeholder="" value="">
        </div>
        <div class="form-group">
          <label>Kota/Kecamatan(*)</label>
          <select class="custom-select form-control" name="city_id">
            <option value="" selected>Pilih Kota</option>
            <option value="22">Bandung</option>
            <option value="23">Bandung</option>
          </select>
        </div>
        <div class="form-group">
          <label>Kecamatan(*)</label>
          <select class="custom-select form-control" name="subdistrict_id">
            <option value="" selected>Pilih Kecamatan</option>
          </select>
        </div>
      </div>
      <div class="form-fields">
        <div class="form-group">
          <label>Lokasi pada Google Maps ()</label>
          <p class="small">Cari tempat usaha/kegiatan Anda pada google maps (Kemudian tahan dan geser pin map untuk meletakan ke posisi yang lebih akurat) </p>
          <div class="map-container">
            <div id="map" class="mb-1" style="height:400px;"></div>
            <div class="d-flex mt-2">
              <span class="mr-1">Koordinat:</span>
              <input id="xlatlng" placeholder="(Geser pin map diatas)" type="text" name="location_coordinate" value="" readonly>
            </div>
          </div>
        </div>
      </div>
      <div class="form-fields">
        <div class="form-group">
          <label>Nama Pemilik()</label>
          <input class="form-control" type="text" name="owner" placeholder="" value="">
        </div>
        <div class="form-group">
          <label>Email()</label>
          <input class="form-control" type="email" name="email" placeholder="" value="">
        </div>
        <div class="form-group">
          <label>No. Telp/HP()</label>
          <input class="form-control" type="text" name="phone" placeholder="" value="">
        </div>
        <div class="form-group">
          <label>Website()</label>
          <input class="form-control" type="text" name="website" placeholder="" value="">
        </div>
        <div class="form-group">
          <label>Deskripsi()</label>
          <textarea class="form-control" rows="5" name="description"></textarea>
        </div>
      </div>
    </div>
    <div class="form-group buttons">
      <input type="submit" class="btn" value="Lanjutkan">
    </div>
  </form>
</div>
@endsection