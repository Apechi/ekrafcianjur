@extends('frontend.detailUser')

@section('menu')
    <div class="wrapper">
        <h4 class="text-right">Daftar Produk</h4>
        <form action="" method="get">
            <div class="row">
                <div class="col-md-4 p-1">
                    <input type="text" class="form-control-plaintext" style="border-bottom: 1px solid gray"
                        placeholder="Cari Data Ekraf...." value="{{ $search }}" name="search"
                        aria-label="Cari Data Ekraf....">
                </div>
                <div class="col-md-2 p-1">
                    <button type="submit" class="btn btn-primary">
                        <i class="bi bi-search"></i>
                    </button>
                </div>
                <div class="col-md-5 p-1 d-flex justify-content-end">
                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#produkmodal">
                        <i class="bi bi-plus"></i>Tambahkan Produk
                    </button>
                </div>
            </div>
        </form>
        <hr class="mt-3">
        <div class="data ">
            @foreach ($allproduk as $produk)
                <div class="card m-2 btn btn-outline-light border-0 p-3 rounded-3"
                    style="text-align: start; border-bottom: 1px solid; box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px; @mobile
font-size: 60%
@elsemobile
font-size: 80%
@endmobile">
                    <div class="row">
                        <div class="image col-2 p-1" style="max-width: 7em; max-height: 7em;">
                            <img src="{{ \Storage::url($produk->image) }}" class="img-fluid" alt="" srcset="">
                        </div>
                        <div class="productname col-2 p-1">
                            <p class="m-0 fs-6">{{ $produk->nama }}</p>
                            <p style="font-weight: 400" class="">{{ $produk->tipe }}</p>
                            <p style="font-weight: 400" class="text-muted">{{ $produk->ekraf->nama }}</p>
                        </div>
                        <div class="produk col-2 p-1">
                            @if ($produk->ekspor == 'Ya')
                                <p class="text-success">Berpotensi Untuk Di Ekspor</p>
                            @else
                                <p class="text-danger">Tidak Berpotensi Untuk Di Ekspor</p>
                            @endif
                        </div>
                        <div class="location col-4 p-1">
                            <p class="px-2 text-primary" style="font-weight: bold">Rp
                                {{ number_format($produk->harga_jual) }}</p>
                        </div>
                        <div class="action col-md-2 col-12">
                            <div class="d-flex gap-2 justify-content-end">
                                <a href="/manage/produk/{{ $produk->id }}">
                                    <div class="btn img-fluid btn-warning"><i class="bi bi-pencil"></i> Edit</div>
                                </a>
                                <form action="{{ route('products.delete', $produk->id) }}" method="POST"
                                    onsubmit="return confirm('Anda yakin ingin menghapus produk ini?')">
                                    @csrf @method('DELETE')
                                    <button type="submit" class="btn btn-delete btn-danger m-1" data-bs-toggle="modal"
                                        data-bs-target="#modalDelete"><i class="bi bi-trash text-center"></i></button>
                                </form>



                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <!-- Modal -->
    <div class="modal modal-md fade" id="produkmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Daftarkan Produk Mu</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    @include('frontend.detailproduk')
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- Button trigger modal -->
