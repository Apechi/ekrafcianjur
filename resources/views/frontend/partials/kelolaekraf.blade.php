@extends('frontend.detailUser')

@section('menu')
    <div class="wrapper">
        <h4 class="text-right">Daftar Ekraf</h4>
        <form action="" method="get">
            <div class="row">
                <div class="col-md-4 p-1">
                    <input type="text" name="search" class="form-control-plaintext" style="border-bottom: 1px solid gray"
                        placeholder="Cari Data Ekraf...." value="{{ $search }}" aria-label="Cari Data Ekraf....">
                </div>
                <div class="col-md-2 p-1">
                    <button type="submit" class="btn btn-primary">
                        <i class="bi bi-search"></i>
                    </button>
                </div>
                <div class="col-md-5 p-1 d-flex justify-content-end">
                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        <i class="bi bi-plus"></i> Tambahkan Ekraf
                    </button>
                </div>
            </div>
        </form>
        <hr class="mt-3">
        <div class="data">
            @foreach ($allekraf as $ekraf)
                <div class="card m-2 btn btn-outline-light border-0 p-3 rounded-3"
                    style="text-align: start; border-bottom: 1px solid; box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px; @mobile
font-size: 60%
@elsemobile
font-size: 80%
@endmobile">
                    <div class="row">
                        <div class="image col-2 p-1" style="max-width: 7em; max-height: 7em;">
                            <img src="{{ \Storage::url($ekraf->logo) }}" class="img-fluid" alt="" srcset="">
                        </div>
                        <div class="productname col-2 p-1">
                            <p>{{ $ekraf->nama }}</p>
                            <p class="text-muted">{{ $ekraf->category->nama }}</p>
                        </div>
                        <div class="produk col-2 p-1">
                            <p>Total Produk: {{ $ekraf->ekrafProducts->count() }}</p>
                        </div>
                        <div class="location col-4 p-1">
                            <p class="px-2" style="font-weight: 400"><i
                                    class="bi bi-geo-alt-fill text-primary px-1"></i>{{ $ekraf->alamat }}</p>
                        </div>
                        <div class="action col-md-2 col-12">
                            <div class="d-flex gap-2 justify-content-end">
                                <a href="/ekraf/detail/{{ $ekraf->id }}/{{ Str::slug($ekraf->nama) }}" class="">
                                    <div class="btn img-fluid btn-primary"><i class="bi bi-eye"></i> Lihat</div>
                                </a>
                                <a href="/manage/ekraf/edit/{{ $ekraf->id }}">
                                    <button type="button" class="btn img-fluid btn-warning"><i class="bi bi-pencil"></i>
                                        Edit</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="modal modal-xl fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Daftarkan Ekraf Mu</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    @include('frontend.tambahEkraf')
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- Button trigger modal -->

<!-- Modal -->
