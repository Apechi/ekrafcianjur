{{-- bootstrap --}}
<nav class="navbar position-sticky top-0 ininavbar navbar-expand-lg bg-body-tertiary shadow">
    <div class="container">
        <a class="navbar-brand" href="#"><img src="{{ asset('image/img0.png') }}" style="height: 3em"></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse pt-3 pt-lg-0" id="navbarSupportedContent">
            <ul class="navbar-nav ms-lg-5 me-auto gap-3 d-lg-flex justify-content-lg-center align-items-lg-center">
                <li class="nav-item">
                    <a class="nav-link p-0 {{ request()->is('/') ? 'active text-danger' : '' }} " aria-current="page"
                        href="/">Home</a>
                </li>
                <div class="dropdown">
                    <a class="text-decoration-none dropdown-toggle p-0 {{ request()->is('blog') || request()->is('event') || request()->is('infografis') ? 'text-danger' : 'text-muted' }}"
                        href="infokreasi" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Infokreasi
                    </a>
                    <ul class="dropdown-menu" style="">
                        <li><a class="dropdown-item btn btn-outline-danger rounded-0" href="/blog">Blog</a></li>
                        <li><a class="dropdown-item btn btn-outline-danger rounded-0" href="/event">Event Ekraf</a>
                        </li>
                        <li><a class="dropdown-item btn btn-outline-danger rounded-0" href="/infografis">Infografis</a>
                        </li>
                    </ul>
                </div>
                <li class="dropdown">
                    <a class="text-decoration-none dropdown-toggle p-0 {{ request()->is('ekraf') || request()->is('produk') ? 'text-danger' : 'text-muted' }}"
                        href="dataekraf" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Data Ekraf
                    </a>
                    <div>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item btn btn-outline-danger rounded-0" href="/ekraf">Pelaku
                                    Ekraf</a>
                            </li>
                            <li><a class="dropdown-item btn btn-outline-danger rounded-0" href="/produk">Produk
                                    Ekraf</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link p-0 {{ request()->is('gedungCCC') ? 'text-danger' : '' }}"
                        href="/gedungCCC">GedungCCC</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link p-0 {{ request()->is('hubungiKami') ? 'text-danger' : '' }}"
                        href="/kontak">Hubungi Kami</a>
                </li>
                </li>
            </ul>
            <div class="inibutton d-lg-flex gap-2 pt-3 pt-lg-0">
                @guest
                    <a href="/login" class="text-decoration-none">
                        <button type="button" class="btn my-1 my-md-0 btn-danger">Login / Daftarkan</button>
                    </a>
                @endguest
                @auth
                    @if (Auth::user()->hasRole(['admin', 'super-admin']))
                        <a href="/admimin/home">
                            <button type="button" class="btn my-1 my-md-0 btn-primary">Admin Panel</button>
                        </a>
                    @endif
                    <form action="{{ route('logout') }}" method="post">
                        @csrf
                        <button type="submit" class="btn my-1 my-md-0 btn-outline-danger">Logout</button>
                    </form>
                    <a href="/manage/ekraf">
                        <button type="button" class="btn my-1 my-md-0 btn-danger">Kelola Ekraf</button>
                    </a>
                @endauth
            </div>
        </div>
    </div>
</nav>
