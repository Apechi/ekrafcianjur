 {{-- belah kanan --}}
 <div class="col-md-4">
     <h4 class="mt-2 mt-md-0"> Blog Artikel Terbaru : </h4>
     @foreach ($recentArticles as $artikelterbaru)
         <a href="blog/detail/{{ $artikelterbaru->id }}" class="text-decoration-none text-black">
             <div class="card btn border-0 btn-outline-light text-center mt-2 text-black"
                 style="box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px;">
                 <div class="header" style="height: 20em">
                     <img src="{{ \Storage::url($artikelterbaru->image) }}" class="card-img-top float-right"
                         alt="..." height="100%" @handheld style="object-fit: contain"
                     @elsehandheld style="object-fit: cover" @endhandheld>
             </div>
             <div class="card-body d-flex flex-column justify-content-between" style="height: 100%">
                 <h5 class="card-title fs-5  m-0">{{ $artikelterbaru->title }}</h5>
                 <div class="sub-title pt-2" style="flex: 4">
                     <p class="card-text m-0 text-muted" style="font-weight: 400">
                         {{ strip_tags(Str::substr($artikelterbaru->body, 0, 150)) }}</p>
                 </div>
                 <div class="mt-2">
                     <a href="blog/detail/{{ $artikelterbaru->id }}"
                         class="text-danger  text-decoration-none m-0">Detail></a>
                 </div>
             </div>
         </div>
     </a>
 @endforeach
</div>
<div class="col-md-8">
 <h4 class="mb-2 mt-4"> Blog Dan Artikel </h4>
 @if ($allartikel->count() > 0)
     @foreach ($allartikel as $artikel)
         <div class="card mb-2 border-0"
             style="box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px;">
             <div class="row">
                 <div class="col-5">
                     @handheld
                         <div class="" style="height: 10em">
                             <img src="{{ \Storage::url($artikel->image) }}" class="rounded" alt="..."
                                 style="width: 100%; height: 100% ; object-fit: cover">
                         </div>
                     @elsehandheld
                         <div class="" style="height: 15em">
                             <img src="{{ \Storage::url($artikel->image) }}" class="rounded" alt="..."
                                 style="width: 100%; height: 100% ; object-fit: cover">
                         </div>
                     @endhandheld
                 </div>
                 <div class="col-7 ps-0">
                     <div class="card-body ps-0 d-flex flex-column justify-content-between" style="height: 100%">
                         @handheld
                             <h5 class="card-title m-0">{{ strip_tags(Str::substr($artikel->title, 0, 45)) }}
                                 @if (strlen($artikel->title) > 45)
                                     ...
                                 @endif
                             </h5>
                         @elsehandheld
                             <h5 class="card-title m-0">{{ $artikel->title }}</h5>
                         @endhandheld
                         <div class="sub-title pt-2" style="flex: 4">
                             @handheld
                             @elsehandheld
                                 <p class="card-text m-0" style="font-weight: 400">
                                     {{ strip_tags(Str::substr($artikel->body, 0, 200)) }}...</p>
                             @endhandheld


                         </div>
                         <div class="mt-1">
                             <a href="blog/detail/{{ $artikel->id }}"
                                 class="text-danger  text-decoration-none m-0">Detail></a>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     @endforeach
 @else
     @include('frontend.partials.datanull')
 @endif

 <div class="paginate">
     {{ $allartikel->render() }}
 </div>
</div>
