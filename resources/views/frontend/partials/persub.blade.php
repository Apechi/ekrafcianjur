<div class="col-lg-2 h-100 p-0 shadow rounded">
    {{-- <div class="bg-light p-2"> --}}
    <a class="d-flex text-decoration-none align-items-center  ">
        {{-- <span class="  d-none d-sm-inline">Filter</span>
    <i class="bi bi-arrow-clockwise md-5"></i> --}}
    </a>
    <ul class="nav flex-column mt-3">
        <form id="myForm" class="isikategori pb-1 px-2" action="" method="GET">
            <div class="input-group py-2">
                <input type="text" placeholder="Cari" value="{{ $search ?? '' }}" name="search" class="form-control">
                <button class="btn btn-danger"><i class="bi bi-search"></i></button>
            </div>
            <li class="nav-item pb-1">
                <a class="ms-2 d-flex text-danger text-decoration-none" data-bs-toggle="collapse" href="#kategori"
                    role="button" aria-expanded="false" aria-controls="collapseExample">
                    <span class=" me-auto d-sm-inline">Kategori</span>
                    <i class=" bi bi-chevron-down pr-1"></i>
                </a>
                <div class="ms-2 mt-2 collapse mb-3" id="kategori">
                    <div class="scrollable-list" style="max-height: 250px; overflow-y: auto; overflow-x: hidden">
                        @foreach ($allKategori as $kategori)
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="kategori[]"
                                    @php
echo isset($kategorifilter) && in_array($kategori->id, $kategorifilter) ? 'checked = "checked"' : '' @endphp
                                    value="{{ $kategori->id }}" id="kategori">
                                <label class="form-check-label" for="kategori">
                                    {{ $kategori->nama }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </li>
            <li class="nav-item pb-1">
                <a class="ms-2 d-flex text-danger text-decoration-none" data-bs-toggle="collapse" href="#Kecamatan"
                    role="button" aria-expanded="false" aria-controls="collapseExample">
                    <span class=" me-auto d-sm-inline">Kecamatan</span>
                    <i class=" bi bi-chevron-down"></i>
                </a>
                <div class="ms-2 mt-2 collapse mb-3" id="Kecamatan">
                    <div class="scrollable-list" style="max-height: 250px; overflow-y: auto; overflow-x: hidden">
                        @foreach ($allKecamatan as $kecamatan)
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="kecamatan[]"
                                    @php
echo isset($kecamatanfilter) && in_array($kecamatan->id, $kecamatanfilter) ? 'checked = "checked"' : '' @endphp
                                    value="{{ $kecamatan->id }}" id="kecamatan">
                                <label class="form-check-label" for="kecamatan">
                                    {{ $kecamatan->nama }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </li>
            <li class="nav-item pb-1">
                <a class="ms-2 d-flex text-danger text-decoration-none" data-bs-toggle="collapse" href="#subsektor"
                    role="button" aria-expanded="false" aria-controls="collapseExample">
                    <span class=" me-auto d-sm-inline">Sub Sektor</span>
                    <i class=" bi bi-chevron-down"></i>
                </a>
                <div class="ms-2 mt-2 collapse" id="subsektor">
                    <div class="scrollable-list" style="max-height: 250px; overflow-y: auto; overflow-x: hidden">
                        @foreach ($allSubSector as $subsektor)
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="subsektor[]"
                                    @php
echo isset($subsektorfilter) && in_array($subsektor->id, $subsektorfilter) ? 'checked = "checked"' : '' @endphp
                                    value="{{ $subsektor->id }}" id="subsektor">
                                <label class="form-check-label" for="subsektor">
                                    {{ $subsektor->nama }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </li>
            <li class="nav-item">
                <div class="nav-link active d-flex text-primary ps-1" aria-current="page">
                    <button type="submit" class="btn btn-danger"><i class="bi bi-filter">Filter</i></button>
                </div>
            </li>
        </form>
    </ul>
</div>
