{{-- FOOTER --}}
<footer class="site-footer p-4">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ \Storage::url($footerData->image) ?? '' }}" alt="Logo" class="logo" height="55px">
                <p class="mt-3"><span class="">{{ $footerData->about ?? '' }}</p>
            </div>
            <div class="col-lg-6 text-lg-right d-lg-flex justify-content-end">
                <div class="right">
                    <p class="mb-1">Temukan Kami Di:</p>
                    <ul class="list-inline d-flex fs-4">
                        @foreach ($socialmedias as $socialmedia)
                            <li class="list-inline-item"><a href="{{ $socialmedia->link ?? '' }}"
                                    class="bi bi-{{ $socialmedia->icon ?? '' }}"></a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <p class="text-center" style="font-weight: 400">Copyright Ⓒ {{ date('Y') }} PT Madtive Studio Indonesia. All
        Rights
        Reserved</p>
</footer>
