<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner vh-100">
        <div class="carousel-item active">
            <img class="d-block w-100 vh-100" src="{{ \Storage::url($gedung->image) ?? '' }}" style="object-fit: cover"
                alt="Gedung Creative Center">
            <div class="carousel-caption"
                style="@desktop
left: 5em;
@elsedesktop
left: 0; right:0;
@enddesktop margin-bottom: 5em; text-align: start">
                <div class="text-white p-3 rounded fs-6 @desktop
w-50
@enddesktop"
                    style="background-color: rgba(0, 0, 0, 0.6);">
                    <h2>{{ $gedung->nama ?? '' }}</h2>
                    <p style="font-weight: 300">{{ $gedung->desc }}</p>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- 
<div id="carouselExampleDark" class="carousel carousel-dark">
    <div class="carousel-inner">
        <div class="carousel-item active" data-bs-interval="10000">
            <img src="image/img1.jpg" class="d-block w-100" alt="">
            <div class="carousel-caption w-100" style="left: 0; right: 0;">
                <div class="curoselContent d-none align-items-end d-md-block d-lg-flex" style="padding:0px 75px;">
                    <div class="text-white p-3 rounded fs-6" style="  background-color: rgba(0, 0, 0, 0.562)">
                        <h2>{{ $gedung->nama }}</h2>
                        <p>{{ $gedung->desc }}</p>
                    </div>
                    <div class="d-flex justify-content-center ms-md-5">
                        <button class="d-none d-md-block btn btn-danger btn-lg p-3 fs-6">Info
                            Selengkapnya</button>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
