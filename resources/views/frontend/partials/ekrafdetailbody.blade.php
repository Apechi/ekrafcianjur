@extends('frontend.pelakuEkrafDetail')

@section('metaseo')
    @include('meta::manager', [
        'title' => $ekraf->nama . '- Ekraf Cianjur',
        'image' => Storage::url($ekraf->logo),
    ])
@endsection

@section('detailbody')
    <div id="carouselExample" class="carousel" data-bs-ride="carousel" data-bs-interval="9999999999">
        <div class="row mt-3">
            <button class="col-4 btn btn-light border-0 rounded-0" type="button" data-bs-target="#carouselExample"
                data-bs-slide-to="0">List Produk</button>
            <button class="col-4 btn btn-light border-0 rounded-0" type="button" data-bs-target="#carouselExample"
                data-bs-slide-to="1">Deskripsi</button>
            <button class="col-4 btn btn-light border-0 rounded-0" type="button" data-bs-target="#carouselExample"
                data-bs-slide-to="2">Maps</button>
        </div>
        <div class="carousel-inner mt-3">
            <div class="carousel-item active">
                @if ($ekraf->ekrafProducts->count() > 0)
                    <div class="produk-list row p-3">
                        @foreach ($ekraf->ekrafProducts as $produk)
                            <div class=" rounded p-2 col-flex col-lg-3 col-sm-6">
                                <div class="card-wrapper p-2 rounded"
                                    style="box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px;">
                                    <a href="/ekraf/detail/{{ $produk->ekraf->id }}/produk/{{ $produk->id }}"
                                        class="text-decoration-none">
                                        <div class="product-box text-decoration-none">
                                            <img class="rounded w-100 p-2" style="height: 8em; object-fit: contain"
                                                src="{{ \Storage::url($produk->image) }}">
                                            <div class="product-info mt-3 text-black">
                                                <h6 class="m-0">{{ $produk->nama }}</h2>
                                                    <p style="font-weight: 300" id="desc{{ $produk->id }}"
                                                        class="collapse mt-2">
                                                        {{ $produk->desc }}</p>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="input">
                                        <button class="btn btn-link arrow-button w-100" data-bs-toggle="collapse"
                                            data-bs-target="#desc{{ $produk->id }}" aria-expanded="false"
                                            aria-controls="desc{{ $produk->id }}">
                                            <i class="bi bi-chevron-down"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="text-center m-4">
                        <i class="bi bi-shop" style="font-size: 2rem;"></i>
                        <p class="mt-2">{{ $ekraf->nama }} Belum Memiliki Produk</p>
                    </div>
                @endif
            </div>
            <div class="carousel-item">
                <div class="desc-wrapper p-2">
                    <p class="m-0">{{ $ekraf->desc }}</p>
                </div>
            </div>
            <div class="carousel-item">
                <div class="map-wrapper p-2">
                    <iframe src="//maps.google.com/maps?q={{ $ekraf->coordinate }}&z=15&output=embed" width="100%"
                        height="500px"></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection
