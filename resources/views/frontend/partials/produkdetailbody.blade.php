@extends('frontend.pelakuEkrafDetail')

@section('metaseo')
    @include('meta::manager', [
        'title' => $produk->nama . '- Ekraf Cianjur',
        'description' => strip_tags(Str::substr($produk->desc, 0, 150)) . '...',
        'image' => Storage::url($produk->image),
    ])
@endsection

@section('detailbody')
    <div class="wrapper p-4">
        <div class="row">
            <div class="kiri col-md-6 col-lg-4">
                <div class="img-wrapper rounded"
                    style="box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.7) 0px 1px 2px 0px;">
                    <img class="p-2" style="object-fit: contain; height: 20em; width: 100% "
                        src="{{ \Storage::url($produk->image) }}" alt="{{ $produk->nama }}" srcset="">
                </div>
            </div>
            <div class="kanan col-lg-8 col-md-6 mt-3 mt-md-0">
                <div class="detail-wrapper">
                    <h3>{{ $produk->nama }}</h3>
                    <div class="content">
                        <p class="m-0">Harga: <span class="text-primary" style="font-weight: bold">Rp
                                {{ number_format($produk->harga_jual) }}</span></p>
                        <p class="m-0">Ekspor: <span
                                class="{{ $produk->ekspor == 'Ya' ? 'text-success' : 'text-danger' }}"
                                style="font-weight: bold">
                                {{ $produk->ekspor }}</span></p>
                        <p class="m-0">Tipe: <span class="text-muted" style="font-weight: bold">
                                {{ $produk->tipe }}</span></p>
                        <p class="m-0" style="max-width: fit-content;">Deskripsi: </p>
                        <p style="font-weight: 300">
                            {{ $produk->desc }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
