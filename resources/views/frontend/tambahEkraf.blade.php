@section('style')
    <link rel="stylesheet" href="{{ asset('style/kelolaekraf.css') }}">
@endsection

<div class="container">
    <form class="dasboard-cruds-main pt-0" action="{{ route('manageEkrafs.store') }}" method="POST"
        enctype="multipart/form-data">
        @csrf
        <div class="main-form media">
            <div class="logo-img" x-data="imageViewer()">
                <label class="mb-2">Upload Logo:</label>
                <div class="upload-image">
                    <div x-data="imageViewer()">
                        <div class="mb-2 row-cols-1">
                            <!-- Show the image -->
                            <template x-if="imageUrl">
                                <img :src="imageUrl" class="rounded"
                                    style="width: 20em; height: 20em; object-fit: cover; border: 1px dashed red"
                                    @click="triggerFileInput">
                            </template>

                            <!-- Show the gray box when image is not available -->
                            <template x-if="!imageUrl">

                                <div class="rounded d-flex justify-content-center align-items-center"
                                    style="width: 20em; height: 20em; border: 1px dashed red" @click="triggerFileInput">
                                    <p class="text-danger">+ Pilih Logo</p>
                                </div>
                            </template>

                            <!-- Image file selector -->
                            <input class="mt-2" type="file" name="logo" accept="image/*" @change="fileChosen"
                                style="display: none" x-ref="fileInput">

                            <p class="text-muted" style="font-weight: 500; max-width: 20em;">Format gambar dengan ukuran

                                minimum 300 x 300px (Untuk
                                gambar lebih optimal
                                gunakan ukuran minimum 700 x 700 px)</p>
                            @error('logo')
                                <div class="alert alert-primary" role="alert">
                                    <strong>Alert</strong> {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-fields">
                <div class="form-group">
                    <label>Nama Usaha/Kegiatan:</label>
                    <input class="form-control" type="text" name="nama" placeholder=""
                        value="{{ old('nama') }}">
                    @error('nama')
                        <div class="alert alert-primary" role="alert">
                            <strong>Alert</strong> {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Kategori Usaha:</label>
                    <select class="custom-select form-control" name="category_id">
                        <option value="" disabled selected>Pilih Kategori</option>
                        @foreach ($allcategory as $category)
                            <option value="{{ $category->id }}">{{ $category->nama }}</option>
                        @endforeach
                    </select>
                    @error('category_id')
                        <div class="alert alert-primary" role="alert">
                            <strong>Alert</strong> {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Sub Sektor Usaha:</label>
                    <select class="custom-select form-control" name="sub_sector_id">
                        <option value="" disable selected>Pilih Sub Sektor</option>
                        @foreach ($allsubsektor as $subsektor)
                            <option value="{{ $subsektor->id }}">{{ $subsektor->nama }}</option>
                        @endforeach
                    </select>
                    @error('sub_sector_id')
                        <div class="alert alert-primary" role="alert">
                            <strong>Alert</strong> {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Alamat Usaha/Kegiatan:</label>
                    <input class="form-control" type="text" name="alamat" placeholder=""
                        value="{{ old('alamat') }}">
                    @error('alamat')
                        <div class="alert alert-primary" role="alert">
                            <strong>Alert</strong> {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Kota/Kecamatan:</label>
                    <select class="custom-select form-control" name="kecamatan_id">
                        <option value="" disabled selected>Pilih kecamatan</option>
                        @foreach ($allkecamatan as $kecamatan)
                            <option value="{{ $kecamatan->id }}">{{ $kecamatan->nama }}</option>
                        @endforeach
                    </select>
                    @error('kecamatan_id')
                        <div class="alert alert-primary" role="alert">
                            <strong>Alert</strong> {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
            <div class="form-fields">
                <div class="form-group">
                    <label>Lokasi pada Google Maps ()</label>
                    <p class="small">Cari tempat usaha/kegiatan Anda pada google maps (Kemudian tahan dan geser pin map
                        untuk meletakan ke posisi yang lebih akurat) </p>
                    <div class="map-container">
                        <div id="map" class="mb-1" style="height:400px;"></div>
                        <div class="d-flex mt-2">
                            <span class="mr-1">Koordinat:</span>
                            <input id="xlatlng" placeholder="(Geser pin map diatas)" style="pointer-events: none"
                                type="text" name="coordinate" value="{{ old('coordinate') }}">
                            @error('coordinate')
                                <div class="alert alert-primary" role="alert">
                                    <strong>Alert</strong> {{ $message }}
                                </div>
                            @enderror
                            <input type="hidden" id="latitudeInput">
                            <input type="hidden" id="longitudeInput">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-fields">
                <div class="form-group">
                    <label>Nama Pemilik:</label>
                    <input class="form-control" type="text" name="nama_pemilik" placeholder=""
                        value="{{ old('nama_pemilik') }}">
                    @error('nama_pemilik')
                        <div class="alert alert-primary" role="alert">
                            <strong>Alert</strong> {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Email:</label>
                    <input class="form-control" type="email" name="email" placeholder=""
                        value="{{ old('email') }}">
                    @error('email')
                        <div class="alert alert-primary" role="alert">
                            <strong>Alert</strong> {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>No. Telp/HP:</label>
                    <input class="form-control" type="text" name="no_telp" placeholder=""
                        value="{{ old('no_telp') }}">
                    @error('no_telp')
                        <div class="alert alert-primary" role="alert">
                            <strong>Alert</strong> {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Website:</label>
                    <input class="form-control" type="text" name="website" placeholder=""
                        value="{{ old('website') }}">
                    @error('website')
                        <div class="alert alert-primary" role="alert">
                            <strong>Alert</strong> {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Deskripsi:</label>
                    <textarea class="form-control" rows="5" value="{{ old('desc') }}" name="desc"></textarea>
                    @error('desc')
                        <div class="alert alert-primary" role="alert">
                            <strong>Alert</strong> {{ $message }}
                        </div>
                    @enderror
                </div>
                <input type="text" hidden value="{{ Auth::user()->id }}" name="user_id">
            </div>
        </div>
        <div class="form-group buttons">
            <input type="submit" class="btn" value="Lanjutkan">
        </div>
    </form>
</div>
@section('script')
    <script>
        var latitude = -6.820762;
        var longitude = 107.142960;
        var zoomLevel = 18;

        var map = L.map('map').setView([latitude, longitude], zoomLevel);

        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 20,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(map);;

        var marker = L.marker([latitude, longitude], {
            draggable: true
        }).addTo(map);

        $('#exampleModal').on('shown.bs.modal', function() {
            setTimeout(function() {
                map.invalidateSize();
            }, 10);
        });

        marker.on('dragend', function(e) {
            var latitude = e.target.getLatLng().lat;
            var longitude = e.target.getLatLng().lng;
            document.getElementById("latitudeInput").value = latitude;
            document.getElementById("longitudeInput").value = longitude;
            document.getElementById("xlatlng").value = latitude + ", " + longitude;
        });
    </script>
@endsection
