@extends('frontend.layouts.main')

@section('metaseo')
    @include('meta::manager', [
        'title' => $event->title,
        'description' => strip_tags(Str::substr($event->body, 0, 150)) . '...',
        'image' => Storage::url($event->image),
    ])
@endsection

@section('container')
    <div class="container mt-5">
        <div class="row mb-md-4 mb-0">
            <div class="col-md-7 rounded" style="height: 25em; overflow: hidden;">
                <img src="{{ \Storage::url($event->image) }}" class="rounded w-100" style="object-fit: cover; height: 100%"
                    alt="">
            </div>
            <div class="col-md-4 mt-3">
                <h3 class="fw-bold mb-3">{{ $event->title }}</h3>
                <ul class="ps-0 mb-2">
                    <li class="d-flex gap-2">
                        <span class="text-danger"><i class="bi bi-collection"></i></span>
                        <p>{{ $event->eventCategory->nama }}</p>
                    </li>
                    <li class="d-flex gap-2">
                        <span class="text-danger"><i class="bi bi-cash"></i></span>
                        <p>{{ $event->pembiayaan }}</p>
                    </li>
                    <li class="d-flex gap-2">
                        <span class="text-danger"><i class="bi bi-geo-alt-fill"></i></span>
                        <p>{{ $event->lokasi }}, {{ $event->alamat }}</p>
                    </li>
                    <li class="d-flex gap-2">
                        <span class="text-danger"><i class="bi bi-people-fill"></i></span>
                        <p>{{ $event->eventMembers->count() }} Anggota mengikuti event ini</p>
                    </li>
                    <li class="d-flex gap-2">
                        <span class="text-danger"><i class="bi bi-calendar"></i></span>
                        <p>{{ $event->waktuMulai }} - {{ $event->waktuSelesai }}</p>
                    </li>
                </ul>

                @if ($isMember)
                    <div class="col">
                        <button type="button" disabled class="btn btn-danger fs-5 text-white mt-0 mt-md-3 mb-3">Anda
                            sudah
                            terdaftar
                            di event ini</button>
                    </div>
                @else
                    @guest
                        <div class="col">
                            <a href="/login" class="text-decoration-none">
                                <button class="btn btn-danger fs-5 text-white mt-0 mt-md-3 mb-3">Login untuk daftar event
                                    ini</button>
                            </a>
                        </div>
                    @endguest
                    @auth
                        <div class="col">
                            <button type="button" class="btn btn-danger fs-5 text-white mt-0 mt-md-3 mb-3"
                                data-bs-toggle="modal" data-bs-target="#myModal">Daftar Event Ini</button>
                        </div>
                    @endauth
                @endif



                {{-- <div class="primary d-flex gap-3 align-items-center">
                    <h5 class="fw-bold mb-0">Share:</h5>
                    <div class="d-flex align-items-center gap-2">
                        <span class="text-primary"><i class="fs-4 md-3 bi bi-instagram"></i></span>
                        <span class="text-primary"><i class="fs-4 md-3 bi bi-whatsapp"></i></span>
                        <span class="text-primary"><i class="fs-4 md-3 bi bi-youtube"></i></span>
                    </div>
                </div> --}}
            </div>
        </div>
        <div class="col" style="margin-bottom:200px">
            <div class="row">
                <div class="col-md-7 border-end">
                    <div class="body">
                        {!! $event->body !!}
                    </div>
                </div>

                {{-- Komentar --}}
                <div class="col-md-4 mt-3 mt-md-0">
                    <h5>Komentar</h5>
                    <hr>
                    <div class="col">
                        <div id="disqus_thread"></div>
                        <script>
                            /**
                             *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                             *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */

                            var disqus_config = function() {
                                this.page.url = '{{ url()->current() }}'; // Replace PAGE_URL with your page's canonical URL variable
                                this.page.identifier =
                                    'event/{{ $event->id }}'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                            };

                            (function() { // DON'T EDIT BELOW THIS LINE
                                var d = document,
                                    s = d.createElement('script');
                                s.src = 'https://ekrafcianjur.disqus.com/embed.js';
                                s.setAttribute('data-timestamp', +new Date());
                                (d.head || d.body).appendChild(s);
                            })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments
                                powered by Disqus.</a></noscript>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    @include('frontend.form')
@endsection
