@section('style')
    {{-- <link rel="stylesheet" href="home.css">   --}}
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
@endsection

@extends('frontend.layouts.main')


@section('metaseo')
    @include('meta::manager', [
        'title' => 'Blog - Ekraf Cianjur',
        'description' =>
            'EKRAF CIANJUR adalah lembaga yang dibentuk untuk mendorong kolaborasi pemangku kepentingan ekonomi kreatif dalam melahirkan kreativitas dan inovasi yang memberikan nilai tambah dan meningkatkan kualitas hidup masyarakat Cianjur',
        'image' => asset('image/metaimages'),
    ])
@endsection

@section('container')
    {{-- Blog Dan Articel --}}

    <div class="container mb-5" style="font-size: 80%">
        <div class="row pt-3 pt-md-5 justify-content-between">
            @mobile
                @include('frontend.partials.mobileblog')
            @elsemobile
                <div class="col-md-8">
                    <h4 class="mb-3"> Blog Dan Artikel </h4>
                    @if ($allartikel->count() > 0)
                        @foreach ($allartikel as $artikel)
                            <a href="blog/detail/{{ $artikel->id }}/{{ Str::slug($artikel->title) }}"
                                class="text-decoration-none">
                                <div class="mb-2 border-0 btn btn-outline-light text-start text-black"
                                    style="box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px;">
                                    <div class="row">
                                        <div class="col-5">
                                            @handheld
                                                <div class="" style="height: 10em">
                                                    <img src="{{ \Storage::url($artikel->image) }}" class="rounded" alt="..."
                                                        style="width: 100%; height: 100% ; object-fit: cover">
                                                </div>
                                            @elsehandheld
                                                <div class="" style="height: 13em">
                                                    <img src="{{ \Storage::url($artikel->image) }}" class="rounded" alt="..."
                                                        style="width: 100%; height: 100% ; object-fit: cover">
                                                </div>
                                            @endhandheld
                                        </div>
                                        <div class="col-7 ps-0">
                                            <div class="card-body ps-0 d-flex flex-column justify-content-between"
                                                style="height: 100%">
                                                @handheld
                                                    <h5 class="card-title m-0">{{ strip_tags(Str::substr($artikel->title, 0, 45)) }}
                                                        @if (strlen($artikel->title) > 45)
                                                            ...
                                                        @endif
                                                    </h5>
                                                @elsehandheld
                                                    <h5 class="card-title m-0">{{ $artikel->title }}</h5>
                                                @endhandheld
                                                <div class="sub-title pt-2" style="flex: 4">
                                                    @handheld
                                                    @elsehandheld
                                                        <p class="card-text m-0 text-muted" style="font-weight: 400">
                                                            {{ strip_tags(Str::substr($artikel->body, 0, 200)) }}...</p>
                                                    @endhandheld


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    @else
                        @include('frontend.partials.datanull')
                    @endif

                    <div class="paginate">
                        {{ $allartikel->render() }}
                    </div>
                </div>
                {{-- belah kanan --}}
                <div class="col-md-4">
                    <h4 class="mt-3 mt-md-0"> Terbaru : </h4>
                    @foreach ($recentArticles as $artikelterbaru)
                        <a href="blog/detail/{{ $artikelterbaru->id }}/{{ Str::slug($artikelterbaru->title) }}"
                            class="text-decoration-none">
                            <div class="card btn border-0 btn-outline-light text-black text-center mt-3"
                                style="box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px;">
                                <div class="header" style="height: 20em">
                                    <img src="{{ \Storage::url($artikelterbaru->image) }}" class="card-img-top float-right"
                                        alt="..." height="100%" @handheld style="object-fit: contain"
                                    @elsehandheld style="object-fit: cover" @endhandheld>
                            </div>
                            <div class="card-body d-flex flex-column justify-content-between" style="height: 100%">
                                <h5 class="card-title fs-5  m-0">{{ $artikelterbaru->title }}</h5>
                                <div class="sub-title pt-2" style="flex: 4">
                                    <p class="card-text text-muted m-0" style="font-weight: 400">
                                        {{ strip_tags(Str::substr($artikelterbaru->body, 0, 150)) }}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        @endmobile

    </div>
</div>
@endsection



@section('script')
<!-- Swiper JS -->
<script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>

<!-- Initialize Swiper -->
<script>
    var swiper = new Swiper(".mySwiper", {
        effect: "coverflow",
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 1,
        spaceBetween: 4,
        loop: true,
        slidesPerView: "auto",
        coverflowEffect: {
            rotate: 0,
            stretch: 0,
            depth: 250,
            modifier: 1,
            slideShadows: true,
        },
        pagination: {
            el: ".swiper-pagination",
        },
    });
</script>
@endsection
