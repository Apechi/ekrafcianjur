<!-- Modal -->
@auth

    <div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <form class="row g-3" action="{{ route('memberevent.store', ['eventid' => $eventid]) }}" method="POST">
            @csrf
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger text-white">
                        <h5 class="modal-title" id="exampleModalLabel">Daftar menjadi peserta
                        </h5>
                        <button type="button" class="btn-close " data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <div class="col-md-12">
                            <label for="inputEmail4" class="form-label">Nama Lengkap</label>
                            <input type="text" name="nama_lengkap" style="pointer-events: none"
                                class="form-control text-muted" value="{{ Auth::user()->nama_lengkap }}" id="inputEmail4">
                        </div>
                        <div class="col-md-12">
                            <label for="inputName" class="form-label">Email</label>
                            <input type="text" name="email" style="pointer-events: none"
                                class="form-control text-muted" id="inputName" value="{{ Auth::user()->email }}">
                        </div>
                        <div class="col-md-12">
                            <label for="inputPhone" required class="form-label">Nomor HP/Whatsapp</label>
                            <input type="text" required name="no_telp" class="form-control" id="inputPhone">
                            @error('no_telp')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="col-md-12">
                            <label for="selectEvent" class="form-label">Bila Anda terlibat sebagai Pelaku Ekonomi Kreatif,
                                pilih sub sektor yang Anda pilih</label>
                            <select class="form-select w-100" required name="sub_sector_id" id="selectEvent">
                                <option value="" disabled selected>Pilih Subsektor</option>
                                @foreach ($allSubsektor as $subsektor)
                                    <option value="{{ $subsektor->id }}">{{ $subsektor->nama }}</option>
                                @endforeach
                            </select>
                            @error('sub_sector_id')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="col-md-12">
                            <label for="selectEvent" class="form-label">Desa/Kecamatan</label>
                            <select class="form-select w-100" required name="kecamatan_id" id="selectEvent">
                                <option disabled selected>Pilih Desa</option>
                                @foreach ($allKecamatan as $kecamatans)
                                    <option value="{{ $kecamatans->id }}">{{ $kecamatans->nama }}</option>
                                @endforeach
                            </select>
                            @error('kecamatan_id')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-danger">Daftar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endauth
