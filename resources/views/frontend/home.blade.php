@extends('frontend.layouts.main')

@section('metaseo')
    @include('meta::manager', [
        'title' => 'Ekraf Cianjur',
        'description' =>
            'EKRAF CIANJUR adalah lembaga yang dibentuk untuk mendorong kolaborasi pemangku kepentingan ekonomi kreatif dalam melahirkan kreativitas dan inovasi yang memberikan nilai tambah dan meningkatkan kualitas hidup masyarakat Cianjur',
        'image' => asset('image/metaimages'),
    ])
@endsection

@section('style')
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
    <style>
        .directory {
            color: #CA252B;
        }

        .sektor i {
            /* color: #CA252B; */
        }

        .sektor h6 {
            text-align: center;
        }

        .kategori-logo {
            transition: fill 0.3s;
        }

        .kartu-kategori:hover .kategori-logo {
            filter: brightness(0) invert(1);
        }
    </style>
@endsection

@section('container')
    {{-- Carousel Img --}}
    <div id="carouselExampleDark" class="carousel carousel-dark slide">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true"
                aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            @foreach ($banner as $banners)
                <div class="carousel-item active" data-bs-interval="10000">
                    <a href="https://{{ $banners->link }}">
                        <img style="object-fit: cover; height: 100vh;" src="{{ \Storage::url($banners->image) }}"
                            class="bannerimage w-100" alt="">
                    </a>
                </div>
            @endforeach
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    {{-- direktori --}}
    <div class="container directory" style="max-width: 986px;">
        <div class="directory d-md-flex justify-content-between align-items-center mt-5">
            <h4 class="m-0" style="color: black"> Direktori Kreasi Cianjur </h4>
            <a href="/manage/ekraf" class="text-decoration-none">
                <h6 class="text-danger"> Tambah Data + </h6>
            </a>
        </div>
        <div class="row mt-2">
            @foreach ($kategori as $kategoris)
                <div class="col-6 col-md-3 p-2">
                    <a href="/ekraf?search=&kategori%5B%5D={{ $kategoris->id }}"
                        class="kartu-kategori card border-0 btn btn-outline-danger bg-tersiary rounded-4"
                        style="box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;">
                        <i class="fs-1 pb-3">
                            <img class="kategori-logo img-fluid" width="30px" src="{{ \Storage::url($kategoris->image) }}"
                                alt="">
                        </i>
                        <h6 class="card-title m-0">{{ $kategoris->nama }}</h6>
                        <p class="card-text pb-1" style="text-align: center">
                            {{ $ekraf->where('category_id', $loop->index + 2)->count() }}
                        </p>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
    {{-- 17 SEKTOR --}}
    <div class="container sektor mt-5" style="max-width: 1000px">
        <div class="17-sektor d-md-flex justify-content-between">
            <h3 class=""> <span class="text-warning fs-2">{{ $subsektor->count() }}</span> Sektor </h3>
        </div>
        <div class="row px-2">
            @foreach ($subsektor as $subsektors)
                <div class="col-6 col-md-2 p-2">
                    <a href="ekraf?search=&subsektor%5B%5D={{ $subsektors->id }}"
                        class="kartu-kategori text-decoration-none" style="">
                        <div class="wrapper card border-0 btn btn-outline-danger rounded-4"
                            style="box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;">
                            <i class=" fs-1 pb-3">
                                <img class="kategori-logo img-fluid" width="25px"
                                    src="{{ \Storage::url($subsektors->image) }}" alt="" srcset="">
                            </i>
                            <h6 class="card-title m-0">{{ $subsektors->nama }}</h6>
                            <p class="card-text pb-1" style="text-align: center">
                                {{ $subsektors->allEkraf->count() }}</p>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>

    {{-- Berita Dan Blog --}}
    <div class="container mt-5" style="">
        <div class="justify-content-between align-items-center">
            <h4 class="">Beberapa Berita Dan Blog</h4>
        </div>
        <div class="row swiper mySwiper my-3">
            <div class="col swiper-wrapper">
                @foreach ($artikel as $artikels)
                    <div class="card swiper-slide rounded-4 shadow " style="overflow: hidden; width: 20rem;">
                        <img src="{{ \Storage::url($artikels->image) }}" class="card-img-top img-fluit" alt="..."
                            style=" object-fit: cover;" height="200em">
                        <div class="card-body text-center">
                            <h5 class=" card-title">{{ $artikels->title }}</h5>
                            <p class=" card-text" style="font-weight: 400">
                                {{ Str::substr(strip_tags($artikels->body), 0, 100) }}...</p>
                            <a href="blog/detail/{{ $artikels->id }}/{{ Str::slug($artikels->title) }}"
                                class=" text-danger mx-auto text-decoration-none">Detail></a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="text-center">

            <a href="blog" class=" btn btn-danger">
                Lihat Semua
            </a>
        </div>

    </div>

    {{-- Kami Bekerjasama Dengan --}}
    <div class="container mt-5" style="overflow: hidden">
        <h4>Kami Bekerjasama Dengan</h4>
        <div class="owl-carousel owl-theme my-4">
            @foreach ($partner as $item)
                <div class="item d-flex justify-content-center align-items-center">
                    <img src="{{ \Storage::url($item->image) }}" class="rounded-circle" style="max-width: 7em">
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            responsive: {
                0: {
                    items: 3
                },
                600: {
                    items: 5
                },
                1000: {
                    items: 6
                }
            }
        })
    </script>

    <!-- Swiper JS -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper(".mySwiper", {
            effect: "coverflow",
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 1,
            spaceBetween: 4,
            loop: true,
            slidesPerView: "auto",
            coverflowEffect: {
                rotate: 0,
                stretch: 0,
                depth: 250,
                modifier: 1,
                slideShadows: true,
            },
            pagination: {
                el: ".swiper-pagination",
            },
        });
    </script>
@endsection
