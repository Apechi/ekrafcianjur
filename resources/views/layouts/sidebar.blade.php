<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary  elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('/') }}" class="brand-link">
        <img src="{{ asset('image/logofoot.png') }}" class="img-fluid" alt="Logo Ekonomi Kreative Cianjur"
            class="brand-image bg-white img-circle">
        <span class="brand-text font-weight-light"></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu">

                @auth
                    <li class="nav-item">
                        <a href="{{ route('home') }}" class="nav-link">
                            <i class="nav-icon icon ion-md-pulse"></i>
                            <p>
                                Dashboard
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon icon ion-md-paper-plane"></i>
                            <p>
                                Aplikasi
                                <i class="nav-icon right icon ion-md-arrow-round-back"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon icon ion-md-apps"></i>
                                    <p>
                                        Ekraf
                                        <i class="nav-icon right icon ion-md-arrow-round-back"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    @can('view-any', App\Models\Ekraf::class)
                                        <li class="nav-item">
                                            <a href="{{ route('all-ekraf.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Ekraf</p>
                                            </a>
                                        </li>
                                    @endcan
                                    @can('view-any', App\Models\EkrafProduct::class)
                                        <li class="nav-item">
                                            <a href="{{ route('ekraf-products.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Produk Ekraf</p>
                                            </a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>

                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon icon ion-md-apps"></i>
                                    <p>
                                        Gedung
                                        <i class="nav-icon right icon ion-md-arrow-round-back"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    @can('view-any', App\Models\Gedung::class)
                                        <li class="nav-item">
                                            <a href="{{ route('all-gedung.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Gedung</p>
                                            </a>
                                        </li>
                                    @endcan
                                    @can('view-any', App\Models\FasilitasGedung::class)
                                        <li class="nav-item">
                                            <a href="{{ route('all-fasilitas-gedung.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Fasilitas Gedung</p>
                                            </a>
                                        </li>
                                    @endcan
                                    @can('view-any', App\Models\GedungImage::class)
                                        <li class="nav-item">
                                            <a href="{{ route('gedung-images.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Gambar Gedung</p>
                                            </a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>

                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon icon ion-md-apps"></i>
                                    <p>
                                        Event
                                        <i class="nav-icon right icon ion-md-arrow-round-back"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    @can('view-any', App\Models\Event::class)
                                        <li class="nav-item">
                                            <a href="{{ route('events.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Event</p>
                                            </a>
                                        </li>
                                    @endcan
                                    @can('view-any', App\Models\EventCategory::class)
                                        <li class="nav-item">
                                            <a href="{{ route('event-categories.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Kategori Event</p>
                                            </a>
                                        </li>
                                    @endcan
                                    @can('view-any', App\Models\EventMember::class)
                                        <li class="nav-item">
                                            <a href="{{ route('event-members.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Member Event</p>
                                            </a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>

                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon icon ion-md-apps"></i>
                                    <p>
                                        Transaksi
                                        <i class="nav-icon right icon ion-md-arrow-round-back"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    @can('view-any', App\Models\BookingHeader::class)
                                        <li class="nav-item">
                                            <a href="{{ route('booking-headers.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Booking Header</p>
                                            </a>
                                        </li>
                                    @endcan
                                    @can('view-any', App\Models\BookingDetail::class)
                                        <li class="nav-item">
                                            <a href="{{ route('booking-details.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Booking Detail</p>
                                            </a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>

                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon icon ion-md-apps"></i>
                                    <p>
                                        Kategori-Kategori
                                        <i class="nav-icon right icon ion-md-arrow-round-back"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    @can('view-any', App\Models\Category::class)
                                        <li class="nav-item">
                                            <a href="{{ route('categories.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Kategori Ekraf</p>
                                            </a>
                                        </li>
                                    @endcan
                                    @can('view-any', App\Models\Kecamatan::class)
                                        <li class="nav-item">
                                            <a href="{{ route('all-kecamatan.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Kecamatan</p>
                                            </a>
                                        </li>
                                    @endcan
                                    @can('view-any', App\Models\SubSector::class)
                                        <li class="nav-item">
                                            <a href="{{ route('sub-sectors.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Sub Sektor</p>
                                            </a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>

                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon icon ion-md-apps"></i>
                                    <p>
                                        Lainnya
                                        <i class="nav-icon right icon ion-md-arrow-round-back"></i>
                                    </p>
                                </a>

                                <ul class="nav nav-treeview">

                                    @can('view-any', App\Models\Article::class)
                                        <li class="nav-item">
                                            <a href="{{ route('articles.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Artikel</p>
                                            </a>
                                        </li>
                                    @endcan
                                    @can('view-any', App\Models\Banner::class)
                                        <li class="nav-item">
                                            <a href="{{ route('banners.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Banner</p>
                                            </a>
                                        </li>
                                    @endcan
                                    @can('view-any', App\Models\User::class)
                                        <li class="nav-item">
                                            <a href="{{ route('users.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Users</p>
                                            </a>
                                        </li>
                                    @endcan
                                    @can('view-any', App\Models\Socialmedia::class)
                                        <li class="nav-item">
                                            <a href="{{ route('socialmedias.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Sosial Media</p>
                                            </a>
                                        </li>
                                    @endcan
                                    @can('view-any', App\Models\Contactsbox::class)
                                        <li class="nav-item">
                                            <a href="{{ route('contactsboxes.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Hasil Submit Kontak</p>
                                            </a>
                                        </li>
                                    @endcan
                                    @can('view-any', App\Models\ContactInformation::class)
                                        <li class="nav-item">
                                            <a href="{{ route('contact-informations.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Informasi Kontak</p>
                                            </a>
                                        </li>
                                    @endcan
                                    @can('view-any', App\Models\Partner::class)
                                        <li class="nav-item">
                                            <a href="{{ route('partners.index') }}" class="nav-link">
                                                <i class="nav-icon icon ion-md-radio-button-off"></i>
                                                <p>Partner</p>
                                            </a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>
                        </ul>
                    </li>



                    @if (Auth::user()->can('view-any', Spatie\Permission\Models\Role::class) ||
                            Auth::user()->can('view-any', Spatie\Permission\Models\Permission::class))
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon icon ion-md-key"></i>
                                <p>
                                    Access Management
                                    <i class="nav-icon right icon ion-md-arrow-round-back"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('view-any', Spatie\Permission\Models\Role::class)
                                    <li class="nav-item">
                                        <a href="{{ route('roles.index') }}" class="nav-link">
                                            <i class="nav-icon icon ion-md-radio-button-off"></i>
                                            <p>Roles</p>
                                        </a>
                                    </li>
                                @endcan

                                @can('view-any', Spatie\Permission\Models\Permission::class)
                                    <li class="nav-item">
                                        <a href="{{ route('permissions.index') }}" class="nav-link">
                                            <i class="nav-icon icon ion-md-radio-button-off"></i>
                                            <p>Permissions</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endif
                @endauth

                <li class="nav-item">
                    <a href="https://adminlte.io/docs/3.1//index.html" target="_blank" class="nav-link">
                        <i class="nav-icon icon ion-md-help-circle-outline"></i>
                        <p>Docs</p>
                    </a>
                </li>

                @auth
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="nav-icon icon ion-md-exit"></i>
                            <p>{{ __('Logout') }}</p>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @endauth
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
