<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\EkrafController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\GedungController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\KecamatanController;
use App\Http\Controllers\SubSectorController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\EventMemberController;
use App\Http\Controllers\EkrafProductController;
use App\Http\Controllers\EventCategoryController;
use App\Http\Controllers\BookingHeaderController;
use App\Http\Controllers\BookingDetailController;
use App\Http\Controllers\ContactInformationController;
use App\Http\Controllers\ContactsboxController;
use App\Http\Controllers\FasilitasGedungController;
use App\Http\Controllers\Frontend\BlogController;
use App\Http\Controllers\Frontend\EkrafController as FrontendEkrafController;
use App\Http\Controllers\Frontend\EventController as FrontendEventController;
use App\Http\Controllers\Frontend\GedungController as FrontendGedungController;
use App\Http\Controllers\Frontend\HomeController as FrontendHomeController;
use App\Http\Controllers\Frontend\InfografisController;
use App\Http\Controllers\Frontend\KontakController;
use App\Http\Controllers\Frontend\ProductController;
use App\Http\Controllers\GedungImageController;
use App\Http\Controllers\ManageEkrafController;
use App\Http\Controllers\ManageProductController;
use App\Http\Controllers\PartnerController;
use App\Http\Controllers\SocialmediaController;

use App\Http\Controllers\UserController;
use App\Http\Controllers\UserSettingController;
use App\Models\Category;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admimin', function () {
    return view('welcome');
});

Auth::routes();

Route::prefix('/admimin')
    ->middleware(['auth', 'is.admin'])
    ->group(function () {
        Route::get('home', [HomeController::class, 'index'])->name('home');

        Route::resource('roles', RoleController::class);
        Route::resource('permissions', PermissionController::class);
        Route::resource('users', UserController::class);
        Route::resource('gedung-images', GedungImageController::class);
        Route::resource('mediasosial', SocialmediaController::class)->names([
            'index' => 'socialmedias.index',
            'create' => 'socialmedias.create',
            'store' => 'socialmedias.store',
            'show' => 'socialmedias.show',
            'edit' => 'socialmedias.edit',
            'update' => 'socialmedias.update',
            'destroy' => 'socialmedias.destroy',

        ]);
        Route::resource('kotaksubmit', ContactsboxController::class)->names([
            'index' => 'contactsboxes.index',
            'create' => 'contactsboxes.create',
            'store' => 'contactsboxes.store',
            'show' => 'contactsboxes.show',
            'edit' => 'contactsboxes.edit',
            'update' => 'contactsboxes.update',
            'destroy' => 'contactsboxes.destroy',
        ]);
        Route::resource('informasikontak', ContactInformationController::class)->names([
            'index' => 'contact-informations.index',
            'create' => 'contact-informations.create',
            'store' => 'contact-informations.store',
            'show' => 'contact-informations.show',
            'edit' => 'contact-informations.edit',
            'update' => 'contact-informations.update',
            'destroy' => 'contact-informations.destroy',
        ]);
        Route::resource('partner', PartnerController::class)->names([
            'index' => 'partners.index',
            'create' => 'partners.create',
            'show' => 'partners.show',
            'store' => 'partners.store',
            'edit' => 'partners.edit',
            'update' => 'partners.update',
            'destroy' => 'partners.destroy',
        ]);
        Route::resource('kategori', CategoryController::class)->names([
            'index' => 'categories.index',
            'create' => 'categories.create',
            'store' => 'categories.store',
            'show' => 'categories.show',
            'edit' => 'categories.edit',
            'update' => 'categories.update',
            'destroy' => 'categories.destroy',
        ]);
        Route::resource('kecamatan', KecamatanController::class)->names([
            'index' => 'all-kecamatan.index',
            'create' => 'all-kecamatan.create',
            'store' => 'all-kecamatan.store',
            'show' => 'all-kecamatan.show',
            'edit' => 'all-kecamatan.edit',
            'update' => 'all-kecamatan.update',
            'destroy' => 'all-kecamatan.destroy',
        ]);
        Route::resource('sub_sektor', SubSectorController::class)->names([
            'index' => 'sub-sectors.index',
            'create' => 'sub-sectors.create',
            'store' => 'sub-sectors.store',
            'show' => 'sub-sectors.show',
            'edit' => 'sub-sectors.edit',
            'update' => 'sub-sectors.update',
            'destroy' => 'sub-sectors.destroy',
        ]);
        Route::resource('ekraf', EkrafController::class)->names([
            'index' => 'all-ekraf.index',
            'create' => 'all-ekraf.create',
            'store' => 'all-ekraf.store',
            'show' => 'all-ekraf.show',
            'edit' => 'all-ekraf.edit',
            'update' => 'all-ekraf.update',
            'destroy' => 'all-ekraf.destroy',

        ]);
        Route::resource('produk_ekraf', EkrafProductController::class)->names([
            'index' => 'ekraf-products.index',
            'create' => 'ekraf-products.create',
            'store' => 'ekraf-products.store',
            'show' => 'ekraf-products.show',
            'edit' => 'ekraf-products.edit',
            'update' => 'ekraf-products.update',
            'destroy' => 'ekraf-products.destroy',
        ]);
        Route::resource('kategori_event', EventCategoryController::class)->names([
            'index' => 'event-categories.index',
            'create' => 'event-categories.create',
            'store' => 'event-categories.store',
            'show' => 'event-categories.show',
            'edit' => 'event-categories.edit',
            'update' => 'event-categories.update',
            'destroy' => 'event-categories.destroy',
        ]);
        Route::resource('artikel', ArticleController::class)->names([
            'index' => 'articles.index',
            'create' => 'articles.create',
            'store' => 'articles.store',
            'show' => 'articles.show',
            'edit' => 'articles.edit',
            'update' => 'articles.update',
            'destroy' => 'articles.destroy',
        ]);

        Route::resource('member_event', EventMemberController::class)->names([
            'index' => 'event-members.index',
            'create' => 'event-members.create',
            'store' => 'event-members.store',
            'show' => 'event-members.show',
            'edit' => 'event-members.edit',
            'update' => 'event-members.update',
            'destroy' => 'event-members.destroy',
        ]);

        Route::resource('gedung', GedungController::class)->names([
            'index' => 'all-gedung.index',
            'create' => 'all-gedung.create',
            'store' => 'all-gedung.store',
            'show' => 'all-gedung.show',
            'edit' => 'all-gedung.edit',
            'update' => 'all-gedung.update',
            'destroy' => 'all-gedung.destroy',
        ]);

        Route::resource('fasilitas_gedung', FasilitasGedungController::class)->names([
            'index' => 'all-fasilitas-gedung.index',
            'create' => 'all-fasilitas-gedung.create',
            'store' => 'all-fasilitas-gedung.store',
            'show' => 'all-fasilitas-gedung.show',
            'edit' => 'all-fasilitas-gedung.edit',
            'update' => 'all-fasilitas-gedung.update',
            'destroy' => 'all-fasilitas-gedung.destroy',
        ]);

        Route::resource('banner', BannerController::class)->names([
            'index' => 'banners.index',
            'create' => 'banners.create',
            'store' => 'banners.store',
            'show' => 'banners.show',
            'edit' => 'banners.edit',
            'update' => 'banners.update',
            'destroy' => 'banners.destroy',
        ]);

        Route::resource('booking_header', BookingHeaderController::class)->names([
            'index' => 'booking-headers.index',
            'create' => 'booking-headers.create',
            'store' => 'booking-headers.store',
            'show' => 'booking-headers.show',
            'edit' => 'booking-headers.edit',
            'update' => 'booking-headers.update',
            'destroy' => 'booking-headers.destroy',
        ]);

        Route::resource('booking_detail', BookingDetailController::class)->names([
            'index' => 'booking-details.index',
            'create' => 'booking-details.create',
            'store' => 'booking-details.store',
            'show' => 'booking-details.show',
            'edit' => 'booking-details.edit',
            'update' => 'booking-details.update',
            'destroy' => 'booking-details.destroy',
        ]);

        Route::resource('event', EventController::class)->names([
            'index' => 'events.index',
            'create' => 'events.create',
            'store' => 'events.store',
            'show' => 'events.show',
            'edit' => 'events.edit',
            'update' => 'events.update',
            'destroy' => 'events.destroy',
        ]);
    });









//Ini Untuk User

Route::get('login', function () {
    return view('frontend.login');
})->name('login');



Route::get('register', function () {
    $category = Category::all();
    return view('frontend.register', compact('category'));
})->name('register');

Route::get('/', [FrontendHomeController::class, 'getData']);


//blog
Route::get('blog', [BlogController::class, 'index']);
Route::get('blog/detail/{id}/{slug}', [BlogController::class, 'show']);

//eventEkraf
Route::get('/event', [FrontendEventController::class, 'index']);
Route::get('/event/detail/{eventid}/{slug}', [FrontendEventController::class, 'show']);

//memberekraf
Route::post('event/join/{eventid}', [FrontendEventController::class, 'daftarMember'])->name('memberevent.store');


//Ekraf
Route::get('/ekraf', [FrontendEkrafController::class, 'index']);
Route::get('ekraf/detail/{id}/{slug}', [FrontendEkrafController::class, 'show']);



//Product
Route::get('/produk', [ProductController::class, 'index']);
Route::get('ekraf/detail/{ekrafid}/produk/{produkid}', [ProductController::class, 'show']);

//HubungiKami
Route::get('/kontak', [KontakController::class, 'index']);
Route::post('/kontak', [KontakController::class, 'store']);

//infografis
Route::get('/infografis', [InfografisController::class, 'index']);

Route::get('/gedungCCC', [FrontendGedungController::class, 'index']);

//kelola produk
Route::delete('manage/produk/delete/{produk_ekraf}', [ManageProductController::class, 'destroy'])->name('products.delete');
Route::get('/manage/produk', [ManageProductController::class, 'index']);
Route::post('/manage/produk/create', [ManageProductController::class, 'store'])->name('products.store');
Route::get('/manage/produk/{id}', [ManageProductController::class, 'edit']);
Route::put('/manage/produk/update/{id}', [ManageProductController::class, 'update'])->name('products.update');


//kelola Ekraf
Route::get('/manage/ekraf', [ManageEkrafController::class, 'index'])->name('manageEkrafs.index');
Route::post('/manage/ekraf', [ManageEkrafController::class, 'store'])->name('manageEkrafs.store');
Route::get('/manage/ekraf/edit/{id}', [ManageEkrafController::class, 'show']);
Route::put('/manage/ekraf/update/{id}', [ManageEkrafController::class, 'update']);


//User Setting
Route::get('auth/usersetting', [UserSettingController::class, 'index']);
Route::put('auth/usersetting/update', [UserSettingController::class, 'update']);


Route::get('auth/form', function () {
    return view('frontend.form');
});
Route::get('auth/kelolaEkraf', function () {
    return view('frontend.kelolaEkraf');
});
Route::get('auth/form', function () {
    return view('frontend.form');
});
Route::get('auth/detailbooking', function () {
    return view('frontend.detailbooking');
});
Route::get('auth/detailproduk', function () {
    return view('frontend.detailproduk');
});
