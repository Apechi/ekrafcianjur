<?php

return [
    'common' => [
        'actions' => 'Actions',
        'create' => 'Create',
        'edit' => 'Edit',
        'update' => 'Update',
        'new' => 'New',
        'cancel' => 'Cancel',
        'attach' => 'Attach',
        'detach' => 'Detach',
        'save' => 'Save',
        'delete' => 'Delete',
        'delete_selected' => 'Delete selected',
        'search' => 'Search...',
        'back' => 'Back to Index',
        'are_you_sure' => 'Are you sure?',
        'no_items_found' => 'No items found',
        'created' => 'Successfully created',
        'saved' => 'Saved successfully',
        'removed' => 'Successfully removed',
    ],

    'kategori' => [
        'name' => 'Kategori',
        'index_title' => 'List Kategori',
        'new_title' => 'Kategori Baru',
        'create_title' => 'Buat Kategori',
        'edit_title' => 'Edit Kategori',
        'show_title' => 'Tampilkan Kategori',
        'inputs' => [
            'nama' => 'Nama',
            'image' => 'Image',
        ],
    ],

    'kecamatan' => [
        'name' => 'Kecamatan',
        'index_title' => 'List Kecamatan',
        'new_title' => 'Kecamatan Baru',
        'create_title' => 'Tambah Kecamatan',
        'edit_title' => 'Edit Kecamatan',
        'show_title' => 'Tampilkan Kecamatan',
        'inputs' => [
            'nama' => 'Nama',
        ],
    ],

    'ekraf' => [
        'name' => 'Ekraf',
        'index_title' => 'List Ekraf',
        'new_title' => 'Ekraf Baru',
        'create_title' => 'Tambah Ekraf',
        'edit_title' => 'Edit Ekraf',
        'show_title' => 'Tampilkan Ekraf',
        'inputs' => [
            'nama' => 'Nama',
            'logo' => 'Logo',
            'alamat' => 'Alamat',
            'coordinate' => 'Coordinate',
            'nama_pemilik' => 'Nama Pemilik',
            'email' => 'Email',
            'no_telp' => 'No Telp',
            'website' => 'Website',
            'desc' => 'Desc',
            'category_id' => 'Category',
            'sub_sector_id' => 'Sub Sector',
            'kecamatan_id' => 'Kecamatan',
            'user_id' => 'User',
        ],
    ],

    'produk_ekraf' => [
        'name' => 'Produk Ekraf',
        'index_title' => 'List Produk',
        'new_title' => 'Produk Ekraf Baru',
        'create_title' => 'Tambah Produk Ekraf',
        'edit_title' => 'Edit Produk Ekraf',
        'show_title' => 'Tampilkan Produk Ekraf',
        'inputs' => [
            'image' => 'Image',
            'nama' => 'Nama',
            'tipe' => 'Tipe',
            'harga_jual' => 'Harga Jual',
            'ekspor' => 'Ekspor',
            'desc' => 'Desc',
            'ekraf_id' => 'Ekraf',
        ],
    ],

    'kategori_event' => [
        'name' => 'Kategori Event',
        'index_title' => 'List Kategori Event',
        'new_title' => 'Kategori Event Baru',
        'create_title' => 'Tambah Kategori Event',
        'edit_title' => 'Edit Kategori Event',
        'show_title' => 'Tampilkan Kategori Event',
        'inputs' => [
            'nama' => 'Nama',
        ],
    ],

    'articles' => [
        'name' => 'Articles',
        'index_title' => 'List Artikel',
        'new_title' => 'Artikel Baru',
        'create_title' => 'Buat Artikel',
        'edit_title' => 'Edit Artikel',
        'show_title' => 'Tampilkan Artikel',
        'inputs' => [
            'image' => 'Image',
            'title' => 'Title',
            'body' => 'Body',
            'writer' => 'Writer',
            'published_at' => 'Published At',
        ],
    ],

    'member_event' => [
        'name' => 'Member Event',
        'index_title' => 'List Member Event',
        'new_title' => 'Event Member Baru',
        'create_title' => 'Tambah Member Event',
        'edit_title' => 'Edit Member Event',
        'show_title' => 'Tampilkan Event Member',
        'inputs' => [
            'user_id' => 'User',
            'nama_lengkap' => 'Nama Lengkap',
            'email' => 'Email',
            'no_telp' => 'No Telp',
            'sub_sector_id' => 'Sub Sector',
            'kecamatan_id' => 'Kecamatan',
            'event_id' => 'Event',
        ],
    ],

    'gedung' => [
        'name' => 'Gedung',
        'index_title' => 'List Gedung',
        'new_title' => 'Gedung Baru',
        'create_title' => 'Tambah Gedung',
        'edit_title' => 'Edit Gedung',
        'show_title' => 'Tampilkan Gedung',
        'inputs' => [
            'image' => 'Image',
            'nama' => 'Nama',
            'desc' => 'Desc',
            'kegiatan' => 'Kegiatan',
            'harga' => 'Harga',
            'alamat' => 'Alamat',
            'no_telp' => 'No Telp',
        ],
    ],

    'fasilitas_gedung' => [
        'name' => 'Fasilitas Gedung',
        'index_title' => 'List Fasilitas Gedung',
        'new_title' => 'Fasilitas Gedung Baru',
        'create_title' => 'Tambah Fasilitas Gedung',
        'edit_title' => 'Edit Fasilitas Gedung',
        'show_title' => 'Tampilkan Fasilitas Gedung',
        'inputs' => [
            'gedung_id' => 'Gedung Id',
            'fasilitas' => 'Fasilitas',
        ],
    ],

    'banner' => [
        'name' => 'Banner',
        'index_title' => 'List Banner',
        'new_title' => 'Banner Baru',
        'create_title' => 'Tambah Banner',
        'edit_title' => 'Edit Banner',
        'show_title' => 'Tampilkan Banner',
        'inputs' => [
            'image' => 'Image',
            'link' => 'Link',
        ],
    ],

    'booking_header' => [
        'name' => 'Booking Header',
        'index_title' => 'List Header Booking',
        'new_title' => 'Booking Header Baru',
        'create_title' => 'Tambah Header Booking',
        'edit_title' => 'Edit Header Booking',
        'show_title' => 'Tampilkan Header Booking',
        'inputs' => [
            'date' => 'Date',
            'total_harga' => 'Total Harga',
            'status' => 'Status',
            'payment_type' => 'Payment Type',
            'card_number' => 'Card Number',
            'bank' => 'Bank',
            'user_id' => 'User',
            'gedung_id' => 'Gedung',
        ],
    ],

    'booking_detail' => [
        'name' => 'Booking Detail',
        'index_title' => 'List Detail Booking',
        'new_title' => 'Detail Booking Baru',
        'create_title' => 'Tambah Detail Booking',
        'edit_title' => 'Edit Detail Booking',
        'show_title' => 'Tampilkan Detail Booking',
        'inputs' => [
            'booking_header_id' => 'Booking Header',
            'harga_unit' => 'Harga Unit',
            'sub_total' => 'Sub Total',
            'durasi' => 'Durasi',
        ],
    ],

    'event' => [
        'name' => 'Event',
        'index_title' => 'List Event',
        'new_title' => 'Event Baru',
        'create_title' => 'Buat Event',
        'edit_title' => 'Edit Event',
        'show_title' => 'Tampilkan Event',
        'inputs' => [
            'title' => 'Title',
            'body' => 'Body',
            'tanggal' => 'Tanggal',
            'pembiayaan' => 'Pembiayaan',
            'lokasi' => 'Lokasi',
            'alamat' => 'Alamat',
            'waktu_mulai' => 'Waktu Mulai',
            'waktu_selesai' => 'Waktu Selesai',
            'image' => 'Image',
            'event_category_id' => 'Event Category',
            'kecamatan_id' => 'Kecamatan',
        ],
    ],

    'sub_sectors' => [
        'name' => 'Sub Sectors',
        'index_title' => 'List Sub Sektor',
        'new_title' => 'Sub Sektor Baru',
        'create_title' => 'Tambah Sub Sektor',
        'edit_title' => 'Edit Sub Sektor',
        'show_title' => 'Tampilkan Sub Sektor',
        'inputs' => [
            'nama' => 'Nama',
            'image' => 'Image',
        ],
    ],

    'users' => [
        'name' => 'Users',
        'index_title' => 'Users List',
        'new_title' => 'New User',
        'create_title' => 'Create User',
        'edit_title' => 'Edit User',
        'show_title' => 'Show User',
        'inputs' => [
            'nama_lengkap' => 'Nama Lengkap',
            'email' => 'Email',
            'password' => 'Password',
            'category_id' => 'Category',
        ],
    ],

    'sosial_media' => [
        'name' => 'Sosial Media',
        'index_title' => 'List Sosial Media',
        'new_title' => 'Tambah Sosial Media',
        'create_title' => 'Buat Informasi Sosial Media',
        'edit_title' => 'Edit Sosial Media',
        'show_title' => 'Tampilkan Sosial Media',
        'inputs' => [
            'icon' => 'Icon',
            'link' => 'Link',
        ],
    ],

    'hasil_submit_kontak' => [
        'name' => 'Hasil Submit Kontak',
        'index_title' => 'List Kontakan',
        'new_title' => 'Kontakan Baru',
        'create_title' => 'Buat Kontakan',
        'edit_title' => 'Edit Kontakan',
        'show_title' => 'Tampilkan Kontakan',
        'inputs' => [
            'nama' => 'Nama',
            'email' => 'Email',
            'message' => 'Message',
        ],
    ],

    'informasi_kontak' => [
        'name' => 'Informasi Kontak',
        'index_title' => 'List Informasi Kontak',
        'new_title' => 'Data Informasi Kontak Baru',
        'create_title' => 'Buat Informasi Kontak',
        'edit_title' => 'Edit Informasi Kontak',
        'show_title' => 'Tampilkan Informasi Kontak',
        'inputs' => [
            'image' => 'Image',
            'about' => 'About',
            'email' => 'Email',
            'coordinate' => 'Coordinate',
            'address' => 'Address',
            'no_telp' => 'No Telp',
        ],
    ],

    'partner' => [
        'name' => 'Partner',
        'index_title' => 'List Partner',
        'new_title' => 'Partner Baru',
        'create_title' => 'Buat Partner',
        'edit_title' => 'Edit Partner',
        'show_title' => 'Tampilkan Partner',
        'inputs' => [
            'nama' => 'Nama',
            'image' => 'Image',
        ],
    ],

    'gambar_gedung' => [
        'name' => 'Gambar Gedung',
        'index_title' => 'List Gambar Gedung',
        'new_title' => 'Gambar Gedung Baru',
        'create_title' => 'Buat Gambar Gedung',
        'edit_title' => 'Edit Gambar Gedung',
        'show_title' => 'Tampilan Gambar Gedung',
        'inputs' => [
            'image' => 'Image',
            'gedung_id' => 'Gedung',
        ],
    ],

    'roles' => [
        'name' => 'Roles',
        'index_title' => 'Roles List',
        'create_title' => 'Create Role',
        'edit_title' => 'Edit Role',
        'show_title' => 'Show Role',
        'inputs' => [
            'name' => 'Name',
        ],
    ],

    'permissions' => [
        'name' => 'Permissions',
        'index_title' => 'Permissions List',
        'create_title' => 'Create Permission',
        'edit_title' => 'Edit Permission',
        'show_title' => 'Show Permission',
        'inputs' => [
            'name' => 'Name',
        ],
    ],
];
