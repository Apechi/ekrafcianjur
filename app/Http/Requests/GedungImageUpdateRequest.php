<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GedungImageUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => ['nullable', 'image', 'max:2024'],
            'gedung_id' => ['required', 'exists:gedung,id'],
        ];
    }
}
