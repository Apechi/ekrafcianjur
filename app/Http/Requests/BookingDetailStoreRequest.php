<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingDetailStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'booking_header_id' => ['required', 'exists:booking_headers,id'],
            'harga_unit' => ['required', 'digits_between:1,255'],
            'sub_total' => ['required', 'digits_between:1,255'],
            'durasi' => ['required', 'numeric'],
        ];
    }
}
