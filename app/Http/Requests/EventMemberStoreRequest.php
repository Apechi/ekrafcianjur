<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventMemberStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_lengkap' => ['required', 'max:255', 'string'],
            'email' => ['required', 'email'],
            'no_telp' => ['required', 'digits_between:1,255', 'string'],
            'sub_sector_id' => ['required', 'exists:sub_sectors,id'],
            'kecamatan_id' => ['required', 'exists:kecamatan,id'],
        ];
    }
}
