<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => ['nullable', 'image', 'max:1124'],
            'title' => ['required', 'max:255', 'string'],
            'body' => ['required', 'string'],
            'writer' => ['required', 'max:255', 'string'],
            'published_at' => ['required', 'date'],
        ];
    }
}
