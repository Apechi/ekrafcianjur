<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GedungUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => ['nullable', 'image', 'max:2024'],
            'nama' => ['required', 'max:255', 'string'],
            'desc' => ['required', 'max:255', 'string'],
            'kegiatan' => ['required', 'max:255', 'string'],
            'harga' => ['required', 'digits_between:1,255', 'numeric'],
            'alamat' => ['required', 'max:255', 'string'],
            'no_telp' => ['required', 'max:255', 'string', 'digits_between:1,255'],
        ];
    }
}
