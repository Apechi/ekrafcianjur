<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactInformationUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => ['image', 'max:1024', 'nullable'],
            'about' => ['required', 'max:255', 'string'],
            'email' => ['required', 'max:255', 'string'],
            'coordinate' => ['required', 'max:255', 'string'],
            'address' => ['required', 'max:255', 'string'],
            'no_telp' => ['required', 'max:255', 'string'],
        ];
    }
}
