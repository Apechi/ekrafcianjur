<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EkrafUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo' => ['max:1024', 'image'],
            'nama' => ['required', 'max:255', 'string'],
            'alamat' => ['required', 'max:255', 'string'],
            'coordinate' => ['required', 'max:255', 'string'],
            'nama_pemilik' => ['required', 'max:255', 'string'],
            'email' => ['required', 'email'],
            'no_telp' => ['required', 'digits_between:1,255', 'string'],
            'website' => ['required', 'max:255', 'string'],
            'desc' => ['required', 'max:255', 'string'],
            'category_id' => ['required', 'exists:categories,id'],
            'sub_sector_id' => ['required', 'exists:sub_sectors,id'],
            'kecamatan_id' => ['required', 'exists:kecamatan,id'],
            'user_id' => ['required', 'exists:users,id'],
        ];
    }
}
