<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EkrafProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => ['nullable', 'image', 'max:3024'],
            'nama' => ['required', 'max:255', 'string'],
            'tipe' => ['required', 'max:255', 'string'],
            'harga_jual' => ['required', 'digits_between:1,255'],
            'ekspor' => ['required', 'in:Ya,Tidak'],
            'desc' => ['required', 'max:255', 'string'],
            'ekraf_id' => ['required', 'exists:ekraf,id'],
        ];
    }
}
