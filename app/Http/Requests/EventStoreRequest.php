<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'max:255', 'string'],
            'body' => ['required', 'string'],
            'tanggal' => ['required', 'date'],
            'pembiayaan' => ['required', 'max:255', 'string'],
            'lokasi' => ['required', 'max:255', 'string'],
            'alamat' => ['required', 'max:255', 'string'],
            'waktu_mulai' => ['required', 'date'],
            'waktu_selesai' => ['required', 'date'],
            'image' => ['nullable', 'image', 'max:1024'],
            'event_category_id' => ['required', 'exists:event_categories,id'],
            'kecamatan_id' => ['required', 'exists:kecamatan,id'],
        ];
    }
}
