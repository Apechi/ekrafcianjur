<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingHeaderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => ['required', 'date'],
            'total_harga' => ['required', 'digits_between:1,255'],
            'status' => ['required', 'in:Pending,Selesai'],
            'payment_type' => ['required', 'in:Tunai,Debit'],
            'card_number' => ['nullable', 'digits_between:1,255', 'string'],
            'bank' => ['nullable', 'max:255', 'string'],
            'user_id' => ['required', 'exists:users,id'],
            'gedung_id' => ['required', 'exists:gedung,id'],
        ];
    }
}
