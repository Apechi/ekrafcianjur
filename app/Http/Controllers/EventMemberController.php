<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Event;
use App\Models\SubSector;
use App\Models\Kecamatan;
use App\Models\EventMember;
use Illuminate\Http\Request;
use App\Http\Requests\EventMemberStoreRequest;
use App\Http\Requests\EventMemberUpdateRequest;

class EventMemberController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EventMember::class);

        $search = $request->get('search', '');

        $eventMembers = EventMember::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view(
            'app.event_members.index',
            compact('eventMembers', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EventMember::class);

        $users = User::pluck('nama_lengkap', 'id');
        $subSectors = SubSector::pluck('nama', 'id');
        $allKecamatan = Kecamatan::pluck('nama', 'id');
        $events = Event::pluck('title', 'id');

        return view(
            'app.event_members.create',
            compact('users', 'subSectors', 'allKecamatan', 'events')
        );
    }

    /**
     * @param \App\Http\Requests\EventMemberStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventMemberStoreRequest $request)
    {
        $this->authorize('create', EventMember::class);

        $validated = $request->validated();

        $eventMember = EventMember::create($validated);

        return redirect()
            ->route('event-members.edit', $eventMember)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EventMember $eventMember
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EventMember $member_event)
    {
        $this->authorize('view', $member_event);

        return view('app.event_members.show', [
            'eventMember' => $member_event
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EventMember $eventMember
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EventMember $member_event)
    {
        $this->authorize('update', $member_event);

        $users = User::pluck('nama_lengkap', 'id');
        $subSectors = SubSector::pluck('nama', 'id');
        $allKecamatan = Kecamatan::pluck('nama', 'id');
        $events = Event::pluck('title', 'id');

        return view(
            'app.event_members.edit',
            [
                'eventMember' => $member_event,
                'users' => $users,
                'subSectors' => $subSectors,
                'allKecamatan' => $allKecamatan,
                'events' => $events
            ]
        );
    }

    /**
     * @param \App\Http\Requests\EventMemberUpdateRequest $request
     * @param \App\Models\EventMember $eventMember
     * @return \Illuminate\Http\Response
     */
    public function update(
        EventMemberUpdateRequest $request,
        EventMember $member_event
    ) {
        $this->authorize('update', $member_event);

        $validated = $request->validated();

        $member_event->update($validated);

        return redirect()
            ->route('event-members.edit', $member_event)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EventMember $eventMember
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EventMember $member_event)
    {
        $this->authorize('delete', $member_event);

        $member_event->delete();

        return redirect()
            ->route('event-members.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
