<?php

namespace App\Http\Controllers;

use App\Models\Contactsbox;
use Illuminate\Http\Request;
use App\Http\Requests\ContactsboxStoreRequest;
use App\Http\Requests\ContactsboxUpdateRequest;

class ContactsboxController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Contactsbox::class);

        $search = $request->get('search', '');

        $contactsboxes = Contactsbox::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view(
            'app.contactsboxes.index',
            compact('contactsboxes', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Contactsbox::class);

        return view('app.contactsboxes.create');
    }

    /**
     * @param \App\Http\Requests\ContactsboxStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactsboxStoreRequest $request)
    {
        $this->authorize('create', Contactsbox::class);

        $validated = $request->validated();

        $contactsbox = Contactsbox::create($validated);

        return redirect()
            ->route('contactsboxes.edit', $contactsbox)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Contactsbox $contactsbox
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Contactsbox $kotaksubmit)
    {
        $this->authorize('view', $kotaksubmit);

        return view('app.contactsboxes.show', [
            'contactsbox' => $kotaksubmit
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Contactsbox $contactsbox
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Contactsbox $kotaksubmit)
    {
        $this->authorize('update', $kotaksubmit);

        return view('app.contactsboxes.edit', [
            'contactsbox' => $kotaksubmit
        ]);
    }

    /**
     * @param \App\Http\Requests\ContactsboxUpdateRequest $request
     * @param \App\Models\Contactsbox $contactsbox
     * @return \Illuminate\Http\Response
     */
    public function update(
        ContactsboxUpdateRequest $request,
        Contactsbox $kotaksubmit
    ) {
        $this->authorize('update', $kotaksubmit);

        $validated = $request->validated();

        $kotaksubmit->update($validated);

        return redirect()
            ->route('contactsboxes.edit', $kotaksubmit)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Contactsbox $contactsbox
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Contactsbox $kotaksubmit)
    {
        $this->authorize('delete', $kotaksubmit);

        $kotaksubmit->delete();

        return redirect()
            ->route('contactsboxes.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
