<?php

namespace App\Http\Controllers;

use App\Models\Kecamatan;
use Illuminate\Http\Request;
use App\Http\Requests\KecamatanStoreRequest;
use App\Http\Requests\KecamatanUpdateRequest;

class KecamatanController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Kecamatan::class);

        $search = $request->get('search', '');

        $allKecamatan = Kecamatan::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view(
            'app.all_kecamatan.index',
            compact('allKecamatan', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Kecamatan::class);

        return view('app.all_kecamatan.create');
    }

    /**
     * @param \App\Http\Requests\KecamatanStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(KecamatanStoreRequest $request)
    {
        $this->authorize('create', Kecamatan::class);

        $validated = $request->validated();

        $kecamatan = Kecamatan::create($validated);

        return redirect()
            ->route('all-kecamatan.edit', $kecamatan)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Kecamatan $kecamatan
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Kecamatan $kecamatan)
    {
        $this->authorize('view', $kecamatan);

        return view('app.all_kecamatan.show', compact('kecamatan'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Kecamatan $kecamatan
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Kecamatan $kecamatan)
    {
        $this->authorize('update', $kecamatan);

        return view('app.all_kecamatan.edit', compact('kecamatan'));
    }

    /**
     * @param \App\Http\Requests\KecamatanUpdateRequest $request
     * @param \App\Models\Kecamatan $kecamatan
     * @return \Illuminate\Http\Response
     */
    public function update(
        KecamatanUpdateRequest $request,
        Kecamatan $kecamatan
    ) {
        $this->authorize('update', $kecamatan);

        $validated = $request->validated();

        $kecamatan->update($validated);

        return redirect()
            ->route('all-kecamatan.edit', $kecamatan)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Kecamatan $kecamatan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Kecamatan $kecamatan)
    {
        $this->authorize('delete', $kecamatan);

        $kecamatan->delete();

        return redirect()
            ->route('all-kecamatan.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
