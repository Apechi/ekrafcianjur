<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Gedung;
use Illuminate\Http\Request;
use App\Models\BookingHeader;
use App\Http\Requests\BookingHeaderStoreRequest;
use App\Http\Requests\BookingHeaderUpdateRequest;

class BookingHeaderController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', BookingHeader::class);

        $search = $request->get('search', '');
        $status = $request->get('status');
        $paymenttype = $request->get('paymenttype');
        $gedung = $request->get('gedung');

        $gedungoption = Gedung::get();

        $query = BookingHeader::query();

        if ($status) {
            $query->where([
                'status' => $status,

            ]);
        }
        if ($paymenttype) {
            $query->where([
                'payment_type' => $paymenttype,
            ]);
        }
        if ($gedung) {
            $query->where([
                'gedung_id' => $gedung
            ]);
        }

        $bookingHeaders = $query->search($search)
            ->latest()
            ->paginate(5)
            ->appends([
                'search' => $search,
                'status' => $status,
                'payment_type' => $paymenttype
            ]);

        return view(
            'app.booking_headers.index',
            compact('bookingHeaders', 'search', 'gedungoption')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', BookingHeader::class);

        $users = User::pluck('nama_lengkap', 'id');
        $allGedung = Gedung::get();
        $today = date('Y-m-d');

        return view(
            'app.booking_headers.create',
            compact('users', 'allGedung', 'today')
        );
    }

    /**
     * @param \App\Http\Requests\BookingHeaderStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookingHeaderStoreRequest $request)
    {
        $this->authorize('create', BookingHeader::class);

        $validated = $request->validated();

        BookingHeader::create($validated);

        return redirect()
            ->route('booking-headers.index')
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BookingHeader $bookingHeader
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, BookingHeader $booking_header)
    {
        $this->authorize('view', $booking_header);

        return view('app.booking_headers.show', [
            'bookingHeader' => $booking_header
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BookingHeader $bookingHeader
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, BookingHeader $booking_header)
    {
        $this->authorize('update', $booking_header);

        $users = User::pluck('nama_lengkap', 'id');
        $allGedung = Gedung::get();

        return view(
            'app.booking_headers.edit',

            [
                'users' => $users,
                'allGedung' => $allGedung,
                'bookingHeader' => $booking_header
            ]
        );
    }

    /**
     * @param \App\Http\Requests\BookingHeaderUpdateRequest $request
     * @param \App\Models\BookingHeader $bookingHeader
     * @return \Illuminate\Http\Response
     */
    public function update(
        BookingHeaderUpdateRequest $request,
        BookingHeader $booking_header
    ) {
        $this->authorize('update', $booking_header);

        $validated = $request->validated();

        $booking_header->update($validated);

        return redirect()
            ->route('booking-headers.edit', $booking_header)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BookingHeader $bookingHeader
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, BookingHeader $booking_header)
    {
        $this->authorize('delete', $booking_header);

        $booking_header->delete();

        return redirect()
            ->route('booking-headers.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
