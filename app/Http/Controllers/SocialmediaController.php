<?php

namespace App\Http\Controllers;

use App\Models\Socialmedia;
use Illuminate\Http\Request;
use App\Http\Requests\SocialmediaStoreRequest;
use App\Http\Requests\SocialmediaUpdateRequest;

class SocialmediaController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Socialmedia::class);

        $search = $request->get('search', '');

        $socialmedias = Socialmedia::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view(
            'app.socialmedias.index',
            compact('socialmedias', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Socialmedia::class);

        return view('app.socialmedias.create');
    }

    /**
     * @param \App\Http\Requests\SocialmediaStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SocialmediaStoreRequest $request)
    {
        $this->authorize('create', Socialmedia::class);

        $validated = $request->validated();

        $socialmedia = Socialmedia::create($validated);

        return redirect()
            ->route('socialmedias.edit', $socialmedia)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Socialmedia $socialmedia
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Socialmedia $mediasosial)
    {
        $this->authorize('view', $mediasosial);

        return view('app.socialmedias.show', [
            'socialmedia' => $mediasosial
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Socialmedia $socialmedia
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Socialmedia $mediasosial)
    {
        $this->authorize('update', $mediasosial);

        return view('app.socialmedias.edit', [
            'socialmedia' => $mediasosial
        ]);
    }

    /**
     * @param \App\Http\Requests\SocialmediaUpdateRequest $request
     * @param \App\Models\Socialmedia $socialmedia
     * @return \Illuminate\Http\Response
     */
    public function update(
        SocialmediaUpdateRequest $request,
        Socialmedia $mediasosial
    ) {
        $this->authorize('update', $mediasosial);

        $validated = $request->validated();

        $mediasosial->update($validated);

        return redirect()
            ->route('socialmedias.edit', $mediasosial)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Socialmedia $socialmedia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Socialmedia $mediasosial)
    {
        $this->authorize('delete', $mediasosial);

        $mediasosial->delete();

        return redirect()
            ->route('socialmedias.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
