<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Ekraf;
use App\Models\Category;
use App\Models\SubSector;
use App\Models\Kecamatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\EkrafStoreRequest;
use App\Http\Requests\EkrafUpdateRequest;

class EkrafController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Ekraf::class);

        $search = $request->get('search', '');

        $allEkraf = Ekraf::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view('app.all_ekraf.index', compact('allEkraf', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Ekraf::class);

        $categories = Category::pluck('nama', 'id');
        $subSectors = SubSector::pluck('nama', 'id');
        $allKecamatan = Kecamatan::pluck('nama', 'id');
        $users = User::pluck('nama_lengkap', 'id');

        return view(
            'app.all_ekraf.create',
            compact('categories', 'subSectors', 'allKecamatan', 'users')
        );
    }

    /**
     * @param \App\Http\Requests\EkrafStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EkrafStoreRequest $request)
    {
        $this->authorize('create', Ekraf::class);

        $validated = $request->validated();
        if ($request->hasFile('logo')) {
            $validated['logo'] = $request->file('logo')->store('public');
        }

        $ekraf = Ekraf::create($validated);

        return redirect()
            ->route('all-ekraf.edit', $ekraf)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Ekraf $ekraf
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Ekraf $ekraf)
    {
        $this->authorize('view', $ekraf);

        return view('app.all_ekraf.show', compact('ekraf'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Ekraf $ekraf
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Ekraf $ekraf)
    {
        $this->authorize('update', $ekraf);

        $categories = Category::pluck('nama', 'id');
        $subSectors = SubSector::pluck('nama', 'id');
        $allKecamatan = Kecamatan::pluck('nama', 'id');
        $users = User::pluck('nama_lengkap', 'id');

        return view(
            'app.all_ekraf.edit',
            compact(
                'ekraf',
                'categories',
                'subSectors',
                'allKecamatan',
                'users'
            )
        );
    }

    /**
     * @param \App\Http\Requests\EkrafUpdateRequest $request
     * @param \App\Models\Ekraf $ekraf
     * @return \Illuminate\Http\Response
     */
    public function update(EkrafUpdateRequest $request, Ekraf $ekraf)
    {
        $this->authorize('update', $ekraf);

        $validated = $request->validated();
        if ($request->hasFile('logo')) {
            if ($ekraf->logo) {
                Storage::delete($ekraf->logo);
            }

            $validated['logo'] = $request->file('logo')->store('public');
        }

        $ekraf->update($validated);

        return redirect()
            ->route('all-ekraf.edit', $ekraf)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Ekraf $ekraf
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Ekraf $ekraf)
    {
        $this->authorize('delete', $ekraf);

        if ($ekraf->logo) {
            Storage::delete($ekraf->logo);
        }

        $ekraf->delete();

        return redirect()
            ->route('all-ekraf.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
