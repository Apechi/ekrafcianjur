<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\EkrafStoreRequest;
use App\Http\Requests\EkrafUpdateRequest;
use App\Models\Category;
use App\Models\Ekraf;
use App\Models\EkrafProduct;
use App\Models\Kecamatan;
use App\Models\SubSector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ManageEkrafController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if (!Auth::user()) {
            return redirect('/login');
        }

        $search = $request->get('search', '');

        $allekraf = Ekraf::search($search)->where('user_id', Auth::user()->id)->with('category')->with('ekrafProducts')->get();
        $allcategory = Category::all();
        $allkecamatan = Kecamatan::all();
        $allsubsektor = SubSector::all();

        return view('frontend.partials.kelolaekraf', compact('allekraf', 'allcategory', 'allkecamatan', 'allsubsektor', 'search'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EkrafStoreRequest $request)
    {
        if (!Auth::user()) {
            return redirect('/login');
        }

        $validated = $request->validated();
        if ($request->hasFile('logo')) {
            $validated['logo'] = $request->file('logo')->store('public');
        }


        $ekraf = Ekraf::create($validated);


        return redirect('/manage/ekraf');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {



        $ekraf = Ekraf::findOrFail($id);
        $allcategory = Category::all();
        $allsubsektor = SubSector::all();
        $allkecamatan = Kecamatan::all();

        return view('frontend.editekraf', compact('ekraf', 'allcategory', 'allsubsektor', 'allkecamatan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EkrafUpdateRequest $request, $id)
    {

        $ekraf = Ekraf::findOrFail($id);

        $validated = $request->validated();
        $validated['user_id'] = Auth::user()->id;

        if ($request->hasFile('logo')) {
            if ($ekraf->logo) {
                Storage::delete($ekraf->logo);
            }

            $validated['logo'] = $request->file('logo')->store('public');
        }

        $ekraf->update($validated);

        return redirect(route('manageEkrafs.index'))->with('success', 'Ekraf Berhasil Di Edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
