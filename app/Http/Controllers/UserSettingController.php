<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\FrontEndUserUpdateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserSettingController extends Controller
{
    public function index()
    {
        if (Auth::user()) {
            redirect('/login');
        }
        $user = Auth::user();

        return view('frontend.detailsetting', compact('user'));
    }



    public function update(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);

        $validated = $request->validate([
            'nama_lengkap' => ['required', 'max:255', 'string'],
            'email' => [
                'required',
                Rule::unique('users', 'email')->ignore($user),
                'email',
            ],
            'password' => ['nullable', 'min:6', 'confirmed'],
            'category_id' => ['required', 'exists:categories,id'],
            'roles' => 'array',
        ]);

        $old_pass = $request->get('old_password');

        if ($old_pass) {
            # code...
            if (!Hash::check($request['old_password'], Auth::user()->password)) {
                return redirect('/auth/usersetting')->with('error', 'Password Lama Salah');
            }
        }

        if (empty($validated['password'])) {
            $validated['password'] = Auth::user()->password;
        } else {
            $validated['password'] = Hash::make($validated['password']);
        }

        $user->update($validated);


        return redirect('/auth/usersetting')->with('success', 'Profile Berhasil Di Ganti');
    }
}
