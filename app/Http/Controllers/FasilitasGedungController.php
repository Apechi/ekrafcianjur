<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FasilitasGedung;
use App\Http\Requests\FasilitasGedungStoreRequest;
use App\Http\Requests\FasilitasGedungUpdateRequest;
use App\Models\Gedung;

class FasilitasGedungController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', FasilitasGedung::class);

        $search = $request->get('search', '');

        $gedungId = $request->get('gedung_id');

        $query = FasilitasGedung::query();

        if ($gedungId) {
            $query->where('gedung_id', $gedungId);
        }

        $allFasilitasGedung = $query->search($search)
            ->latest()
            ->paginate(5)
            ->appends(['search' => $search, 'gedung_id' => $gedungId]);

        $gedungOptions = Gedung::pluck('nama', 'id');

        return view(
            'app.all_fasilitas_gedung.index',
            compact('allFasilitasGedung', 'search', 'gedungOptions', 'gedungId')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', FasilitasGedung::class);
        $gedung = Gedung::all();

        return view('app.all_fasilitas_gedung.create', compact('gedung'));
    }

    /**
     * @param \App\Http\Requests\FasilitasGedungStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(FasilitasGedungStoreRequest $request)
    {
        $this->authorize('create', FasilitasGedung::class);

        $validated = $request->validated();

        $fasilitasGedung = FasilitasGedung::create($validated);

        return redirect()
            ->route('all-fasilitas-gedung.edit', $fasilitasGedung)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\FasilitasGedung $fasilitasGedung
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, FasilitasGedung $fasilitas_gedung)
    {
        $this->authorize('view', $fasilitas_gedung);

        return view(
            'app.all_fasilitas_gedung.show',
            [
                'fasilitasGedung' => $fasilitas_gedung
            ]
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\FasilitasGedung $fasilitasGedung
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, FasilitasGedung $fasilitas_gedung)
    {
        $this->authorize('update', $fasilitas_gedung);
        $gedung = Gedung::all();

        return view(
            'app.all_fasilitas_gedung.edit',
            [
                'fasilitasGedung' => $fasilitas_gedung,
                'gedung' => $gedung
            ]
        );
    }

    /**
     * @param \App\Http\Requests\FasilitasGedungUpdateRequest $request
     * @param \App\Models\FasilitasGedung $fasilitasGedung
     * @return \Illuminate\Http\Response
     */
    public function update(
        FasilitasGedungUpdateRequest $request,
        FasilitasGedung $fasilitas_gedung
    ) {
        $this->authorize('update', $fasilitas_gedung);

        $validated = $request->validated();

        $fasilitas_gedung->update($validated);

        return redirect()
            ->route('all-fasilitas-gedung.edit', $fasilitas_gedung)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\FasilitasGedung $fasilitasGedung
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, FasilitasGedung $fasilitas_gedung)
    {
        $this->authorize('delete', $fasilitas_gedung);

        $fasilitas_gedung->delete();

        return redirect()
            ->route('all-fasilitas-gedung.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
