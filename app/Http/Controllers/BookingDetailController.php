<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BookingDetail;
use App\Models\BookingHeader;
use App\Http\Requests\BookingDetailStoreRequest;
use App\Http\Requests\BookingDetailUpdateRequest;

class BookingDetailController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', BookingDetail::class);

        $search = $request->get('search', '');
        $bookingHeaderFilter = $request->get('booking_header');

        $bookingHeaderOption = BookingHeader::get();

        $query = BookingDetail::query();

        if ($bookingHeaderFilter) {
            $query->where('booking_header_id', $bookingHeaderFilter);
        }

        $bookingDetails = $query->search($search)
            ->latest()
            ->paginate(5)
            ->appends(['search' => $search, 'booking_header_id' => $bookingHeaderFilter]);

        return view(
            'app.booking_details.index',
            compact('bookingDetails', 'search', 'bookingHeaderOption')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', BookingDetail::class);

        $bookingHeaders = BookingHeader::get();

        return view('app.booking_details.create', compact('bookingHeaders'));
    }

    /**
     * @param \App\Http\Requests\BookingDetailStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookingDetailStoreRequest $request)
    {
        $this->authorize('create', BookingDetail::class);

        $validated = $request->validated();

        $bookingDetail = BookingDetail::create($validated);

        return redirect()
            ->route('booking-details.edit', $bookingDetail)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BookingDetail $bookingDetail
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, BookingDetail $booking_detail)
    {
        $this->authorize('view', $booking_detail);

        return view('app.booking_details.show', [
            'bookingDetail' => $booking_detail
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BookingDetail $bookingDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, BookingDetail $booking_detail)
    {
        $this->authorize('update', $booking_detail);

        $bookingHeaders = BookingHeader::get();

        return view(
            'app.booking_details.edit',
            [
                'bookingDetail' => $booking_detail,
                'bookingHeaders' => $bookingHeaders
            ]
        );
    }

    /**
     * @param \App\Http\Requests\BookingDetailUpdateRequest $request
     * @param \App\Models\BookingDetail $bookingDetail
     * @return \Illuminate\Http\Response
     */
    public function update(
        BookingDetailUpdateRequest $request,
        BookingDetail $booking_detai
    ) {
        $this->authorize('update', $booking_detai);

        $validated = $request->validated();

        $booking_detai->update($validated);

        return redirect()
            ->route('booking-details.edit', $booking_detai)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\BookingDetail $bookingDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, BookingDetail $booking_detail)
    {
        $this->authorize('delete', $booking_detail);

        $booking_detail->delete();

        return redirect()
            ->route('booking-details.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
