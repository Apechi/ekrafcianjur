<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Ekraf;
use App\Models\Kecamatan;
use App\Models\SubSector;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EkrafController extends Controller
{


    public function index(Request $request)
    {


        $allKecamatan = Kecamatan::all();
        $allKategori = Category::all();
        $allSubSector = SubSector::all();


        $search = $request->input('search');
        $kecamatanfilter = $request->input('kecamatan');
        $kategorifilter = $request->input('kategori');
        $subsektorfilter = $request->input('subsektor');


        $query = Ekraf::query();

        if ($kecamatanfilter) {
            $query->where('kecamatan_id', $kecamatanfilter);
        }
        if ($kategorifilter) {
            $query->where('category_id', $kategorifilter);
        }
        if ($subsektorfilter) {
            $query->where('sub_sector_id', $subsektorfilter);
        }


        $allEkraf = $query
            ->with('category')
            ->search($search)
            ->latest()
            ->paginate(9)
            ->appends([
                'search' => $search,
                'kecamatan' => $kecamatanfilter,
                'kategori' => $kategorifilter,
                'subsektor' => $subsektorfilter,
            ]);



        return view('frontend.pelakuekraf', compact(
            'allEkraf',
            'allKecamatan',
            'allKategori',
            'allSubSector',
            'kecamatanfilter',
            'kategorifilter',
            'subsektorfilter',
            'search'
        ));
    }



    public function show($id, $slug)
    {
        $ekraf = Ekraf::findOrFail($id);

        $ekraf->with(['ekrafProducts', 'category']);

        if (Str::slug($ekraf->nama) != $slug) {
            return abort(404);
        }


        return view('frontend.partials.ekrafdetailbody', compact('ekraf'));
    }
}
