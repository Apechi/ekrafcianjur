<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Ekraf;
use App\Models\EkrafProduct;
use App\Models\Kecamatan;
use App\Models\SubSector;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $allKecamatan = Kecamatan::all();
        $allKategori = Category::all();
        $allSubSector = SubSector::all();


        $search = $request->input('search');
        $kecamatanfilter = $request->input('kecamatan');
        $kategorifilter = $request->input('kategori');
        $subsektorfilter = $request->input('subsektor');

        $query = EkrafProduct::with('ekraf');

        if ($kecamatanfilter) {
            $query->whereHas('ekraf', function ($q) use ($kecamatanfilter) {
                $q->where('kecamatan_id', $kecamatanfilter);
            });
        }

        if ($kategorifilter) {
            $query->whereHas('ekraf', function ($q) use ($kategorifilter) {
                $q->where('category_id', $kategorifilter);
            });
        }

        if ($subsektorfilter) {
            $query->whereHas('ekraf', function ($q) use ($subsektorfilter) {
                $q->where('sub_sector_id', $subsektorfilter);
            });
        }


        $allProduk = $query
            ->search($search)
            ->latest()
            ->paginate(9)
            ->appends([
                'search' => $search,
                'kecamatan' => $kecamatanfilter,
                'kategori' => $kategorifilter,
                'subsektor' => $subsektorfilter,
            ]);




        return view('frontend.produkekraf', compact(
            'allProduk',
            'allKecamatan',
            'allKategori',
            'allSubSector',
            'kecamatanfilter',
            'kategorifilter',
            'subsektorfilter',
            'search'
        ));
    }


    public function show($ekrafid, $produkid)
    {


        $ekraf = Ekraf::with('ekrafProducts')->findOrFail($ekrafid);

        foreach ($ekraf->ekrafProducts as $key) {
            if ($key->id == $produkid) {
                $produk = EkrafProduct::findOrFail($produkid);


                return view('frontend.partials.produkdetailbody', compact('ekraf', 'produk'));
            }
        }
    abort(404);
    }
}
