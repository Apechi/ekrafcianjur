<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Banner;
use App\Models\Category;
use App\Models\Ekraf;
use App\Models\Partner;
use Illuminate\Support\Str;
use App\Models\SubSector;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function getData()
    {
        $kategori = Category::all();
        $ekraf = Ekraf::all();

        //artikel
        $artikel = Article::limit(5)->latest()->get();

        $subsektor = SubSector::orderBy('nama', 'asc')->with('allEkraf')->get();

        //banner
        $banner = Banner::all();

        $partner = Partner::all();

        return view('frontend.home', compact(
            'kategori',
            'ekraf',
            'subsektor',
            'artikel',
            'banner',
            'partner'
        ));
    }
}
