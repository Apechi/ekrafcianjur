<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactsboxStoreRequest;
use App\Models\ContactInformation;
use App\Models\Contactsbox;
use Illuminate\Http\Request;

class KontakController extends Controller
{
    public function index()
    {

        $information = ContactInformation::latest()->first();


        return view('frontend.hubungikami', compact('information'));
    }

    public function store(Request $request)
    {

        $validated = $request->validate([
            'nama' => ['required', 'max:255', 'string'],
            'email' => ['required', 'email'],
            'message' => ['required', 'max:255', 'string'],
            'g-recaptcha-response' => ['required', 'captcha'],
        ]);

        $contactsbox = Contactsbox::create($validated);

        return redirect()->back()->with('success', 'Pesan Berhasil Terkirim');
    }
}
