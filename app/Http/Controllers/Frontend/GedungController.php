<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\FasilitasGedung;
use App\Models\Gedung;
use Illuminate\Http\Request;

class GedungController extends Controller
{
    public function index()
    {

        $gedung = Gedung::with('fasilitas')->first();

        return view('frontend.gedungCCC', compact('gedung'));
    }
}
