<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Kecamatan;
use App\Models\SubSector;
use Illuminate\Http\Request;

class InfografisController extends Controller
{

    public function index(Request $request)
    {

        $allKecamatan = Kecamatan::all();

        $kecamatanFilter = $request->input('kecamatan');

        $queryKecamatan = Kecamatan::query();
        $querySubsektor = SubSector::query();
        $queryKategori = Category::query();

        if ($kecamatanFilter) {
            $queryKecamatan->where('id', $kecamatanFilter);
            $querySubsektor->join('ekraf', 'sub_sectors.id', '=', 'ekraf.sub_sector_id')
                ->where('ekraf.kecamatan_id', $kecamatanFilter);
            $queryKategori->join('ekraf', 'categories.id', '=', 'ekraf.category_id')
                ->where('ekraf.kecamatan_id', $kecamatanFilter);
        }

        $dataKecamatan = $queryKecamatan->with('allEkraf')->get();
        $dataSubsektor = $querySubsektor->with('allEkraf')->get();
        $dataKategori = $queryKategori->with('allEkraf')->get();


        return view('frontend.infografis', compact('allKecamatan', 'dataKecamatan', 'dataSubsektor', 'dataKategori', 'kecamatanFilter', 'request'));
    }
}
