<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    public function index()
    {

        $allartikel = Article::orderBy('title', 'asc')
            ->paginate(5);

        $recentArticles = Article::orderBy('created_at', 'desc')
            ->take(3)
            ->get();


        return view('frontend.blog', compact(
            'allartikel',
            'recentArticles',
        ));
    }


    public function show($id, $slug)
    {


        $artikel = Article::findOrFail($id);

        if (Str::slug($artikel->title) != $slug) {
            return abort(404);
        }



        $artikel->createdDate = Carbon::parse($artikel->created_at)->format('l, d F Y, H:i:s');

        return view('frontend.detailBlog', compact('artikel', 'artikel'));
    }
}
