<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\EventMemberStoreRequest;
use App\Models\Category;
use App\Models\Event;
use App\Models\EventCategory;
use App\Models\EventMember;
use App\Models\Kecamatan;
use App\Models\SubSector;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class EventController extends Controller
{

    public function index(Request $request)
    {

        $kecamatanFilter = $request->get('kecamatan');
        $categoryFilter = $request->get('kategori');
        $kecamatan = Kecamatan::all();
        $category = EventCategory::all();

        $query = Event::query();


        if ($kecamatanFilter) {
            $query = $query->where('kecamatan_id', $kecamatanFilter);
        }
        if ($categoryFilter) {
            $query = $query->where('event_category_id', $categoryFilter);
        }


        $event = $query
            ->with('eventCategory')
            ->latest()
            ->paginate(6)
            ->appends(
                [
                    'kecamatan' => $kecamatanFilter,
                    'kategori' => $categoryFilter,
                ]
            );


        $startDate = Carbon::now()->startOfWeek();
        $endDate = Carbon::now()->endOfWeek();

        $weeklyEvent = Event::whereBetween('tanggal', [$startDate, $endDate])
            ->take(6)
            ->get();

        return view('frontend.eventekraf', compact('event', 'kecamatan', 'category', 'weeklyEvent'));
    }

    public function show($eventid, $slug)
    {

        setlocale(LC_ALL, 'IND');

        $event = Event::with('eventMembers')
            ->with('eventCategory')
            ->findOrFail($eventid);

        $event->waktuMulai = Carbon::parse($event->waktu_mulai)->locale('id')->isoFormat('D MMMM Y, hh:mm');
        $event->waktuSelesai = Carbon::parse($event->waktu_selesai)->locale('id')->isoFormat('D MMMM Y, hh:mm');


        $allSubsektor = SubSector::all();
        $allKecamatan = Kecamatan::all();

        $isMember = false;
        if (Auth::check()) {
            foreach ($event->eventMembers as $membervalidate) {
                if ($membervalidate->user_id == Auth::user()->id) {
                    $isMember = true;
                    break;
                }
            }
        }

        if (Str::slug($event->title) != $slug) {
            return abort(404);
        }

        return view('frontend.detailevent', compact('event', 'allSubsektor', 'allKecamatan', 'eventid', 'isMember'));
    }


    public function daftarMember(EventMemberStoreRequest $request, $eventid)
    {
        if (!Auth::user()) {
            return redirect('/login');
        }


        $validated = $request->validated();

        $eventMember = EventMember::create([
            'user_id' => Auth::user()->id,
            'nama_lengkap' => $request->input('nama_lengkap'),
            'email' => $request->input('email'),
            'no_telp' => $request->input('no_telp'),
            'sub_sector_id' => $request->input('sub_sector_id'),
            'kecamatan_id' => $request->input('kecamatan_id'),
            'event_id' => $eventid,
        ]);

        return redirect('/event/detail/' . $eventid)->with('success', 'Kamu Berhasil Daftar ke Event');
    }
}
