<?php

namespace App\Http\Controllers;

use App\Models\Gedung;
use App\Models\GedungImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\GedungImageStoreRequest;
use App\Http\Requests\GedungImageUpdateRequest;

class GedungImageController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', GedungImage::class);

        $search = $request->get('search', '');

        $gedungImages = GedungImage::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view(
            'app.gedung_images.index',
            compact('gedungImages', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', GedungImage::class);

        $allGedung = Gedung::pluck('nama', 'id');

        return view('app.gedung_images.create', compact('allGedung'));
    }

    /**
     * @param \App\Http\Requests\GedungImageStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(GedungImageStoreRequest $request)
    {
        $this->authorize('create', GedungImage::class);

        $validated = $request->validated();
        if ($request->hasFile('image')) {
            $validated['image'] = $request->file('image')->store('public');
        }

        $gedungImage = GedungImage::create($validated);

        return redirect()
            ->route('gedung-images.edit', $gedungImage)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\GedungImage $gedungImage
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, GedungImage $gedungImage)
    {
        $this->authorize('view', $gedungImage);

        return view('app.gedung_images.show', compact('gedungImage'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\GedungImage $gedungImage
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, GedungImage $gedungImage)
    {
        $this->authorize('update', $gedungImage);

        $allGedung = Gedung::pluck('nama', 'id');

        return view(
            'app.gedung_images.edit',
            compact('gedungImage', 'allGedung')
        );
    }

    /**
     * @param \App\Http\Requests\GedungImageUpdateRequest $request
     * @param \App\Models\GedungImage $gedungImage
     * @return \Illuminate\Http\Response
     */
    public function update(
        GedungImageUpdateRequest $request,
        GedungImage $gedungImage
    ) {
        $this->authorize('update', $gedungImage);

        $validated = $request->validated();
        if ($request->hasFile('image')) {
            if ($gedungImage->image) {
                Storage::delete($gedungImage->image);
            }

            $validated['image'] = $request->file('image')->store('public');
        }

        $gedungImage->update($validated);

        return redirect()
            ->route('gedung-images.edit', $gedungImage)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\GedungImage $gedungImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, GedungImage $gedungImage)
    {
        $this->authorize('delete', $gedungImage);

        if ($gedungImage->image) {
            Storage::delete($gedungImage->image);
        }

        $gedungImage->delete();

        return redirect()
            ->route('gedung-images.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
