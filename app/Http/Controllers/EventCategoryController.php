<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EventCategory;
use App\Http\Requests\EventCategoryStoreRequest;
use App\Http\Requests\EventCategoryUpdateRequest;

class EventCategoryController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EventCategory::class);

        $search = $request->get('search', '');

        $eventCategories = EventCategory::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view(
            'app.event_categories.index',
            compact('eventCategories', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EventCategory::class);

        return view('app.event_categories.create');
    }

    /**
     * @param \App\Http\Requests\EventCategoryStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventCategoryStoreRequest $request)
    {
        $this->authorize('create', EventCategory::class);

        $validated = $request->validated();

        $eventCategory = EventCategory::create($validated);

        return redirect()
            ->route('event-categories.edit', $eventCategory)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EventCategory $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EventCategory $kategori_event)
    {
        $this->authorize('view', $kategori_event);

        return view('app.event_categories.show', [
            'eventCategory' => $kategori_event
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EventCategory $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EventCategory $kategori_event)
    {
        $this->authorize('update', $kategori_event);

        return view('app.event_categories.edit', [
            'eventCategory' => $kategori_event
        ]);
    }

    /**
     * @param \App\Http\Requests\EventCategoryUpdateRequest $request
     * @param \App\Models\EventCategory $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function update(
        EventCategoryUpdateRequest $request,
        EventCategory $kategori_event
    ) {
        $this->authorize('update', $kategori_event);

        $validated = $request->validated();

        $kategori_event->update($validated);

        return redirect()
            ->route('event-categories.edit', $kategori_event)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EventCategory $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EventCategory $kategori_event)
    {
        $this->authorize('delete', $kategori_event);

        $kategori_event->delete();

        return redirect()
            ->route('event-categories.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
