<?php

namespace App\Http\Controllers;

use App\Models\SubSector;
use Illuminate\Http\Request;
use App\Http\Requests\SubSectorStoreRequest;
use App\Http\Requests\SubSectorUpdateRequest;
use Illuminate\Support\Facades\Storage;

class SubSectorController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', SubSector::class);

        $search = $request->get('search', '');

        $subSectors = SubSector::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view('app.sub_sectors.index', compact('subSectors', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', SubSector::class);

        return view('app.sub_sectors.create');
    }

    /**
     * @param \App\Http\Requests\SubSectorStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubSectorStoreRequest $request)
    {
        $this->authorize('create', SubSector::class);

        $validated = $request->validated();
        if ($request->hasFile('image')) {
            $validated['image'] = $request->file('image')->store('public');
        }

        $subSector = SubSector::create($validated);

        return redirect()
            ->route('sub-sectors.edit', $subSector)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\SubSector $subSector
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, SubSector $sub_sektor)
    {
        $this->authorize('view', $sub_sektor);

        return view('app.sub_sectors.show', [
            'subSector' => $sub_sektor
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\SubSector $subSector
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, SubSector $sub_sektor)
    {
        $this->authorize('update', $sub_sektor);

        return view('app.sub_sectors.edit', [
            'subSector' => $sub_sektor
        ]);
    }

    /**
     * @param \App\Http\Requests\SubSectorUpdateRequest $request
     * @param \App\Models\SubSector $subSector
     * @return \Illuminate\Http\Response
     */
    public function update(
        SubSectorUpdateRequest $request,
        SubSector $sub_sektor
    ) {
        $this->authorize('update', $sub_sektor);

        $validated = $request->validated();
        if ($request->hasFile('image')) {
            if ($sub_sektor->image) {
                Storage::delete($sub_sektor->image);
            }

            $validated['image'] = $request->file('image')->store('public');
        }

        $sub_sektor->update($validated);

        return redirect()
            ->route('sub-sectors.edit', $sub_sektor)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\SubSector $subSector
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, SubSector $sub_sektor)
    {
        $this->authorize('delete', $sub_sektor);

        if ($sub_sektor->image) {
            Storage::delete($sub_sektor->image);
        }


        $sub_sektor->delete();

        return redirect()
            ->route('sub-sectors.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
