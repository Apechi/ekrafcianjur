<?php

namespace App\Http\Controllers;

use App\Models\Gedung;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\GedungStoreRequest;
use App\Http\Requests\GedungUpdateRequest;

class GedungController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Gedung::class);

        $search = $request->get('search', '');

        $allGedung = Gedung::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view('app.all_gedung.index', compact('allGedung', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Gedung::class);

        return view('app.all_gedung.create');
    }

    /**
     * @param \App\Http\Requests\GedungStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(GedungStoreRequest $request)
    {
        $this->authorize('create', Gedung::class);

        $validated = $request->validated();
        if ($request->hasFile('image')) {
            $validated['image'] = $request->file('image')->store('public');
        }

        $gedung = Gedung::create($validated);

        return redirect()
            ->route('all-gedung.edit', $gedung)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Gedung $gedung
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Gedung $gedung)
    {
        $this->authorize('view', $gedung);

        return view('app.all_gedung.show', compact('gedung'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Gedung $gedung
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Gedung $gedung)
    {
        $this->authorize('update', $gedung);

        return view('app.all_gedung.edit', compact('gedung'));
    }

    /**
     * @param \App\Http\Requests\GedungUpdateRequest $request
     * @param \App\Models\Gedung $gedung
     * @return \Illuminate\Http\Response
     */
    public function update(GedungUpdateRequest $request, Gedung $gedung)
    {
        $this->authorize('update', $gedung);

        $validated = $request->validated();
        if ($request->hasFile('image')) {
            if ($gedung->image) {
                Storage::delete($gedung->image);
            }

            $validated['image'] = $request->file('image')->store('public');
        }

        $gedung->update($validated);

        return redirect()
            ->route('all-gedung.edit', $gedung)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Gedung $gedung
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Gedung $gedung)
    {
        $this->authorize('delete', $gedung);

        if ($gedung->image) {
            Storage::delete($gedung->image);
        }

        $gedung->delete();

        return redirect()
            ->route('all-gedung.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
