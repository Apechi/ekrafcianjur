<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\EkrafProductStoreRequest;
use App\Http\Requests\EkrafProductUpdateRequest;
use App\Models\Ekraf;
use App\Models\EkrafProduct;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ManageProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if (!Auth::user()) {
            return redirect('/login');
        }

        $search = $request->get('search', '');

        $userid = Auth::user()->id;

        $allEkraf = Ekraf::where('user_id', $userid)->get();
        $allproduk = EkrafProduct::search($search)->with('ekraf')->whereHas('ekraf', function ($query) use ($userid) {
            $query->where('user_id', $userid);
        })->get();



        return view('frontend.partials.kelolaproduk', compact('allproduk', 'search', 'allEkraf'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EkrafProductStoreRequest $request)
    {
        try {

            if (!Auth::user()) {
                return redirect('/');
            }

            $validated = $request->validated();
            if ($request->hasFile('image')) {
                $validated['image'] = $request->file('image')->store('public');
            }

            $produkEkraf = EkrafProduct::create($validated);

            return redirect('/manage/produk')->with('success', 'Berhasil menambahkan produk');
        } catch (Exception $e) {
            return redirect('/manage/produk')->with('error', $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $produk = EkrafProduct::where('id', $id)->with('ekraf')->first();

        $allEkraf = Ekraf::where('user_id', Auth::user()->id)->get();

        if (!Auth::user() || $produk->ekraf->user_id != Auth::user()->id) {
            return redirect('/manage/produk');
        }


        return view('frontend.editProduk', compact('allEkraf', 'produk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Responses
     */
    public function update(EkrafProductUpdateRequest $request, $id)
    {
        $produk = EkrafProduct::where('id', $id)->with('ekraf')->first();

        if (!Auth::user() || $produk->ekraf->user_id != Auth::user()->id) {
            return redirect('/manage/produk');
        }

        $produk_ekraf = EkrafProduct::findOrFail($id);

        $validated = $request->validated();
        if ($request->hasFile('image')) {
            if ($produk_ekraf->image) {
                Storage::delete($produk_ekraf->image);
            }

            $validated['image'] = $request->file('image')->store('public');
        }

        $produk_ekraf->update($validated);
        return redirect('/manage/produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(EkrafProduct $produk_ekraf)
    {
        if (!Auth::user()) {
            return redirect('/login');
        }

        $produk = $produk_ekraf;

        if (!$produk) {
            return redirect()->back();
        }

        if ($produk->image) {
            Storage::delete($produk->image);
        }

        $produk->delete();

        return redirect()->back()->with('success', 'Produk Berhasil Di Hapus');
    }
}
