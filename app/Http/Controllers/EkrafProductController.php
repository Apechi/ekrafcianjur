<?php

namespace App\Http\Controllers;

use App\Models\Ekraf;
use App\Models\EkrafProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\EkrafProductStoreRequest;
use App\Http\Requests\EkrafProductUpdateRequest;

class EkrafProductController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', EkrafProduct::class);

        $search = $request->get('search', '');

        $ekrafProducts = EkrafProduct::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view(
            'app.ekraf_products.index',
            compact('ekrafProducts', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', EkrafProduct::class);

        $allEkraf = Ekraf::pluck('nama', 'id');

        return view('app.ekraf_products.create', compact('allEkraf'));
    }

    /**
     * @param \App\Http\Requests\EkrafProductStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EkrafProductStoreRequest $request)
    {
        $this->authorize('create', EkrafProduct::class);

        $validated = $request->validated();
        if ($request->hasFile('image')) {
            $validated['image'] = $request->file('image')->store('public');
        }

        $ekrafProduct = EkrafProduct::create($validated);

        return redirect()
            ->route('ekraf-products.edit', $ekrafProduct)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EkrafProduct $ekrafProduct
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EkrafProduct $produk_ekraf)
    {
        $this->authorize('view', $produk_ekraf);

        return view('app.ekraf_products.show', [
            'ekrafProduct' => $produk_ekraf
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EkrafProduct $ekrafProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, EkrafProduct $produk_ekraf)
    {
        $this->authorize('update', $produk_ekraf);

        $allEkraf = Ekraf::pluck('nama', 'id');

        return view(
            'app.ekraf_products.edit',
            [
                'allEkraf' => $allEkraf,
                'ekrafProduct' => $produk_ekraf
            ]
        );
    }

    /**
     * @param \App\Http\Requests\EkrafProductUpdateRequest $request
     * @param \App\Models\EkrafProduct $ekrafProduct
     * @return \Illuminate\Http\Response
     */
    public function update(
        EkrafProductUpdateRequest $request,
        EkrafProduct $produk_ekraf
    ) {
        $this->authorize('update', $produk_ekraf);

        $validated = $request->validated();
        if ($request->hasFile('image')) {
            if ($produk_ekraf->image) {
                Storage::delete($produk_ekraf->image);
            }

            $validated['image'] = $request->file('image')->store('public');
        }

        $produk_ekraf->update($validated);

        return redirect()
            ->route('ekraf-products.edit', $produk_ekraf)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\EkrafProduct $ekrafProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EkrafProduct $produk_ekraf)
    {
        $this->authorize('delete', $produk_ekraf);

        if ($produk_ekraf->image) {
            Storage::delete($produk_ekraf->image);
        }

        $produk_ekraf->delete();

        return redirect()
            ->route('ekraf-products.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
