<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ContactInformation;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ContactInformationStoreRequest;
use App\Http\Requests\ContactInformationUpdateRequest;

class ContactInformationController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', ContactInformation::class);

        $search = $request->get('search', '');



        $contactInformations = ContactInformation::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view(
            'app.contact_informations.index',
            compact('contactInformations', 'search',)
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', ContactInformation::class);

        if (ContactInformation::first()) {
            return redirect()->route('contact-informations.index');
        }

        return view('app.contact_informations.create');
    }

    /**
     * @param \App\Http\Requests\ContactInformationStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactInformationStoreRequest $request)
    {
        $this->authorize('create', ContactInformation::class);

        $validated = $request->validated();
        if ($request->hasFile('image')) {
            $validated['image'] = $request->file('image')->store('public');
        }

        $contactInformation = ContactInformation::create($validated);

        return redirect()
            ->route('contact-informations.edit', $contactInformation)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ContactInformation $contactInformation
     * @return \Illuminate\Http\Response
     */
    public function show(
        Request $request,
        ContactInformation $informasikontak
    ) {
        $this->authorize('view', $informasikontak);

        return view(
            'app.contact_informations.show',
            [
                'contactInformation' => $informasikontak
            ]
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ContactInformation $contactInformation
     * @return \Illuminate\Http\Response
     */
    public function edit(
        Request $request,
        ContactInformation $informasikontak
    ) {
        $this->authorize('update', $informasikontak);

        return view(
            'app.contact_informations.edit',
            [
                'contactInformation' => $informasikontak
            ]
        );
    }

    /**
     * @param \App\Http\Requests\ContactInformationUpdateRequest $request
     * @param \App\Models\ContactInformation $contactInformation
     * @return \Illuminate\Http\Response
     */
    public function update(
        ContactInformationUpdateRequest $request,
        ContactInformation $informasikontak
    ) {
        $this->authorize('update', $informasikontak);

        $validated = $request->validated();
        if ($request->hasFile('image')) {
            if ($informasikontak->image) {
                Storage::delete($informasikontak->image);
            }

            $validated['image'] = $request->file('image')->store('public');
        }

        $informasikontak->update($validated);

        return redirect()
            ->route('contact-informations.edit', $informasikontak)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ContactInformation $contactInformation
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Request $request,
        ContactInformation $informasikontak
    ) {
        $this->authorize('delete', $informasikontak);

        if ($informasikontak->image) {
            Storage::delete($informasikontak->image);
        }

        $informasikontak->delete();

        return redirect()
            ->route('contact-informations.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
