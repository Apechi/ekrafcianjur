<?php

namespace App\Policies;

use App\Models\User;
use App\Models\SubSector;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubSectorPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the subSector can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list subsectors');
    }

    /**
     * Determine whether the subSector can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\SubSector  $model
     * @return mixed
     */
    public function view(User $user, SubSector $model)
    {
        return $user->hasPermissionTo('view subsectors');
    }

    /**
     * Determine whether the subSector can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create subsectors');
    }

    /**
     * Determine whether the subSector can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\SubSector  $model
     * @return mixed
     */
    public function update(User $user, SubSector $model)
    {
        return $user->hasPermissionTo('update subsectors');
    }

    /**
     * Determine whether the subSector can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\SubSector  $model
     * @return mixed
     */
    public function delete(User $user, SubSector $model)
    {
        return $user->hasPermissionTo('delete subsectors');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\SubSector  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete subsectors');
    }

    /**
     * Determine whether the subSector can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\SubSector  $model
     * @return mixed
     */
    public function restore(User $user, SubSector $model)
    {
        return false;
    }

    /**
     * Determine whether the subSector can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\SubSector  $model
     * @return mixed
     */
    public function forceDelete(User $user, SubSector $model)
    {
        return false;
    }
}
