<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Gedung;
use Illuminate\Auth\Access\HandlesAuthorization;

class GedungPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the gedung can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list allgedung');
    }

    /**
     * Determine whether the gedung can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Gedung  $model
     * @return mixed
     */
    public function view(User $user, Gedung $model)
    {
        return $user->hasPermissionTo('view allgedung');
    }

    /**
     * Determine whether the gedung can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create allgedung');
    }

    /**
     * Determine whether the gedung can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Gedung  $model
     * @return mixed
     */
    public function update(User $user, Gedung $model)
    {
        return $user->hasPermissionTo('update allgedung');
    }

    /**
     * Determine whether the gedung can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Gedung  $model
     * @return mixed
     */
    public function delete(User $user, Gedung $model)
    {
        return $user->hasPermissionTo('delete allgedung');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Gedung  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete allgedung');
    }

    /**
     * Determine whether the gedung can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Gedung  $model
     * @return mixed
     */
    public function restore(User $user, Gedung $model)
    {
        return false;
    }

    /**
     * Determine whether the gedung can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Gedung  $model
     * @return mixed
     */
    public function forceDelete(User $user, Gedung $model)
    {
        return false;
    }
}
