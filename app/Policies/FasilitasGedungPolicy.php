<?php

namespace App\Policies;

use App\Models\User;
use App\Models\FasilitasGedung;
use Illuminate\Auth\Access\HandlesAuthorization;

class FasilitasGedungPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the fasilitasGedung can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list allfasilitasgedung');
    }

    /**
     * Determine whether the fasilitasGedung can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\FasilitasGedung  $model
     * @return mixed
     */
    public function view(User $user, FasilitasGedung $model)
    {
        return $user->hasPermissionTo('view allfasilitasgedung');
    }

    /**
     * Determine whether the fasilitasGedung can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create allfasilitasgedung');
    }

    /**
     * Determine whether the fasilitasGedung can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\FasilitasGedung  $model
     * @return mixed
     */
    public function update(User $user, FasilitasGedung $model)
    {
        return $user->hasPermissionTo('update allfasilitasgedung');
    }

    /**
     * Determine whether the fasilitasGedung can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\FasilitasGedung  $model
     * @return mixed
     */
    public function delete(User $user, FasilitasGedung $model)
    {
        return $user->hasPermissionTo('delete allfasilitasgedung');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\FasilitasGedung  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete allfasilitasgedung');
    }

    /**
     * Determine whether the fasilitasGedung can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\FasilitasGedung  $model
     * @return mixed
     */
    public function restore(User $user, FasilitasGedung $model)
    {
        return false;
    }

    /**
     * Determine whether the fasilitasGedung can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\FasilitasGedung  $model
     * @return mixed
     */
    public function forceDelete(User $user, FasilitasGedung $model)
    {
        return false;
    }
}
