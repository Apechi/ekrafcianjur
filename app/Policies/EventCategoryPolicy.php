<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EventCategory;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventCategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the eventCategory can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list eventcategories');
    }

    /**
     * Determine whether the eventCategory can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EventCategory  $model
     * @return mixed
     */
    public function view(User $user, EventCategory $model)
    {
        return $user->hasPermissionTo('view eventcategories');
    }

    /**
     * Determine whether the eventCategory can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create eventcategories');
    }

    /**
     * Determine whether the eventCategory can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EventCategory  $model
     * @return mixed
     */
    public function update(User $user, EventCategory $model)
    {
        return $user->hasPermissionTo('update eventcategories');
    }

    /**
     * Determine whether the eventCategory can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EventCategory  $model
     * @return mixed
     */
    public function delete(User $user, EventCategory $model)
    {
        return $user->hasPermissionTo('delete eventcategories');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EventCategory  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete eventcategories');
    }

    /**
     * Determine whether the eventCategory can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EventCategory  $model
     * @return mixed
     */
    public function restore(User $user, EventCategory $model)
    {
        return false;
    }

    /**
     * Determine whether the eventCategory can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EventCategory  $model
     * @return mixed
     */
    public function forceDelete(User $user, EventCategory $model)
    {
        return false;
    }
}
