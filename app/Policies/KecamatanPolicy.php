<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Kecamatan;
use Illuminate\Auth\Access\HandlesAuthorization;

class KecamatanPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the kecamatan can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list allkecamatan');
    }

    /**
     * Determine whether the kecamatan can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Kecamatan  $model
     * @return mixed
     */
    public function view(User $user, Kecamatan $model)
    {
        return $user->hasPermissionTo('view allkecamatan');
    }

    /**
     * Determine whether the kecamatan can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create allkecamatan');
    }

    /**
     * Determine whether the kecamatan can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Kecamatan  $model
     * @return mixed
     */
    public function update(User $user, Kecamatan $model)
    {
        return $user->hasPermissionTo('update allkecamatan');
    }

    /**
     * Determine whether the kecamatan can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Kecamatan  $model
     * @return mixed
     */
    public function delete(User $user, Kecamatan $model)
    {
        return $user->hasPermissionTo('delete allkecamatan');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Kecamatan  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete allkecamatan');
    }

    /**
     * Determine whether the kecamatan can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Kecamatan  $model
     * @return mixed
     */
    public function restore(User $user, Kecamatan $model)
    {
        return false;
    }

    /**
     * Determine whether the kecamatan can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Kecamatan  $model
     * @return mixed
     */
    public function forceDelete(User $user, Kecamatan $model)
    {
        return false;
    }
}
