<?php

namespace App\Policies;

use App\Models\User;
use App\Models\BookingHeader;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookingHeaderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the bookingHeader can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list bookingheaders');
    }

    /**
     * Determine whether the bookingHeader can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BookingHeader  $model
     * @return mixed
     */
    public function view(User $user, BookingHeader $model)
    {
        return $user->hasPermissionTo('view bookingheaders');
    }

    /**
     * Determine whether the bookingHeader can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create bookingheaders');
    }

    /**
     * Determine whether the bookingHeader can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BookingHeader  $model
     * @return mixed
     */
    public function update(User $user, BookingHeader $model)
    {
        return $user->hasPermissionTo('update bookingheaders');
    }

    /**
     * Determine whether the bookingHeader can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BookingHeader  $model
     * @return mixed
     */
    public function delete(User $user, BookingHeader $model)
    {
        return $user->hasPermissionTo('delete bookingheaders');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BookingHeader  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete bookingheaders');
    }

    /**
     * Determine whether the bookingHeader can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BookingHeader  $model
     * @return mixed
     */
    public function restore(User $user, BookingHeader $model)
    {
        return false;
    }

    /**
     * Determine whether the bookingHeader can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BookingHeader  $model
     * @return mixed
     */
    public function forceDelete(User $user, BookingHeader $model)
    {
        return false;
    }
}
