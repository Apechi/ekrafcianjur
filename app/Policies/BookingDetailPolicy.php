<?php

namespace App\Policies;

use App\Models\User;
use App\Models\BookingDetail;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookingDetailPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the bookingDetail can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list bookingdetails');
    }

    /**
     * Determine whether the bookingDetail can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BookingDetail  $model
     * @return mixed
     */
    public function view(User $user, BookingDetail $model)
    {
        return $user->hasPermissionTo('view bookingdetails');
    }

    /**
     * Determine whether the bookingDetail can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create bookingdetails');
    }

    /**
     * Determine whether the bookingDetail can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BookingDetail  $model
     * @return mixed
     */
    public function update(User $user, BookingDetail $model)
    {
        return $user->hasPermissionTo('update bookingdetails');
    }

    /**
     * Determine whether the bookingDetail can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BookingDetail  $model
     * @return mixed
     */
    public function delete(User $user, BookingDetail $model)
    {
        return $user->hasPermissionTo('delete bookingdetails');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BookingDetail  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete bookingdetails');
    }

    /**
     * Determine whether the bookingDetail can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BookingDetail  $model
     * @return mixed
     */
    public function restore(User $user, BookingDetail $model)
    {
        return false;
    }

    /**
     * Determine whether the bookingDetail can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\BookingDetail  $model
     * @return mixed
     */
    public function forceDelete(User $user, BookingDetail $model)
    {
        return false;
    }
}
