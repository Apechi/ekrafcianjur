<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Ekraf;
use Illuminate\Auth\Access\HandlesAuthorization;

class EkrafPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the ekraf can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list allekraf');
    }

    /**
     * Determine whether the ekraf can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Ekraf  $model
     * @return mixed
     */
    public function view(User $user, Ekraf $model)
    {
        return $user->hasPermissionTo('view allekraf');
    }

    /**
     * Determine whether the ekraf can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create allekraf');
    }

    /**
     * Determine whether the ekraf can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Ekraf  $model
     * @return mixed
     */
    public function update(User $user, Ekraf $model)
    {
        return $user->hasPermissionTo('update allekraf');
    }

    /**
     * Determine whether the ekraf can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Ekraf  $model
     * @return mixed
     */
    public function delete(User $user, Ekraf $model)
    {
        return $user->hasPermissionTo('delete allekraf');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Ekraf  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete allekraf');
    }

    /**
     * Determine whether the ekraf can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Ekraf  $model
     * @return mixed
     */
    public function restore(User $user, Ekraf $model)
    {
        return false;
    }

    /**
     * Determine whether the ekraf can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Ekraf  $model
     * @return mixed
     */
    public function forceDelete(User $user, Ekraf $model)
    {
        return false;
    }
}
