<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EventMember;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventMemberPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the eventMember can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list eventmembers');
    }

    /**
     * Determine whether the eventMember can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EventMember  $model
     * @return mixed
     */
    public function view(User $user, EventMember $model)
    {
        return $user->hasPermissionTo('view eventmembers');
    }

    /**
     * Determine whether the eventMember can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create eventmembers');
    }

    /**
     * Determine whether the eventMember can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EventMember  $model
     * @return mixed
     */
    public function update(User $user, EventMember $model)
    {
        return $user->hasPermissionTo('update eventmembers');
    }

    /**
     * Determine whether the eventMember can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EventMember  $model
     * @return mixed
     */
    public function delete(User $user, EventMember $model)
    {
        return $user->hasPermissionTo('delete eventmembers');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EventMember  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete eventmembers');
    }

    /**
     * Determine whether the eventMember can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EventMember  $model
     * @return mixed
     */
    public function restore(User $user, EventMember $model)
    {
        return false;
    }

    /**
     * Determine whether the eventMember can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EventMember  $model
     * @return mixed
     */
    public function forceDelete(User $user, EventMember $model)
    {
        return false;
    }
}
