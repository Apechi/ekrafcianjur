<?php

namespace App\Policies;

use App\Models\User;
use App\Models\EkrafProduct;
use Illuminate\Auth\Access\HandlesAuthorization;

class EkrafProductPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the ekrafProduct can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list ekrafproducts');
    }

    /**
     * Determine whether the ekrafProduct can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EkrafProduct  $model
     * @return mixed
     */
    public function view(User $user, EkrafProduct $model)
    {
        return $user->hasPermissionTo('view ekrafproducts');
    }

    /**
     * Determine whether the ekrafProduct can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create ekrafproducts');
    }

    /**
     * Determine whether the ekrafProduct can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EkrafProduct  $model
     * @return mixed
     */
    public function update(User $user, EkrafProduct $model)
    {
        return $user->hasPermissionTo('update ekrafproducts');
    }

    /**
     * Determine whether the ekrafProduct can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EkrafProduct  $model
     * @return mixed
     */
    public function delete(User $user, EkrafProduct $model)
    {
        return $user->hasPermissionTo('delete ekrafproducts');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EkrafProduct  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete ekrafproducts');
    }

    /**
     * Determine whether the ekrafProduct can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EkrafProduct  $model
     * @return mixed
     */
    public function restore(User $user, EkrafProduct $model)
    {
        return false;
    }

    /**
     * Determine whether the ekrafProduct can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\EkrafProduct  $model
     * @return mixed
     */
    public function forceDelete(User $user, EkrafProduct $model)
    {
        return false;
    }
}
