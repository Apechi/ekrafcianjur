<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Kecamatan extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['nama'];

    protected $searchableFields = ['*'];

    protected $table = 'kecamatan';

    public function allEkraf()
    {
        return $this->hasMany(Ekraf::class);
    }

    public function eventMembers()
    {
        return $this->hasMany(EventMember::class);
    }
}
