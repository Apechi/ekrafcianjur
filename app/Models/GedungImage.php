<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class GedungImage extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['image', 'gedung_id'];

    protected $searchableFields = ['*'];

    protected $table = 'gedung_images';

    public function gedung()
    {
        return $this->belongsTo(Gedung::class);
    }
}
