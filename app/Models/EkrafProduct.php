<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EkrafProduct extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'nama',
        'tipe',
        'harga_jual',
        'ekspor',
        'desc',
        'image',
        'ekraf_id',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'ekraf_products';

    public function ekraf()
    {
        return $this->belongsTo(Ekraf::class);
    }
}
