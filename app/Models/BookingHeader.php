<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BookingHeader extends Model
{
    use HasFactory;
    use Searchable;


    protected $fillable = [
        'date',
        'total_harga',
        'status',
        'payment_type',
        'card_number',
        'bank',
        'user_id',
        'gedung_id',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'booking_headers';

    protected $casts = [
        'date' => 'date',
        'id' => 'string'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            // Mendapatkan tanggal saat ini
            $currentDate = date('Ymd');

            // Mengambil transaksi terakhir untuk tanggal saat ini
            $lastTransaction = self::where('id', 'LIKE', $currentDate . '%')->orderBy('id', 'desc')->first();

            if ($lastTransaction) {
                // Mendapatkan nomor urut terakhir
                $lastSequence = intval(substr($lastTransaction->id, -4));
                $newSequence = $lastSequence + 1;
            } else {
                // Jika tidak ada transaksi sebelumnya, mulai dari 1
                $newSequence = 1;
            }

            // Generate the new ID with zero-padding
            $newId = $currentDate . str_pad($newSequence, 4, '0', STR_PAD_LEFT);

            $model->id = $newId;
        });
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function gedung()
    {
        return $this->belongsTo(Gedung::class);
    }

    public function bookingDetails()
    {
        return $this->hasMany(BookingDetail::class);
    }
}
