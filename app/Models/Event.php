<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Event extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'title',
        'body',
        'tanggal',
        'pembiayaan',
        'lokasi',
        'alamat',
        'waktu_mulai',
        'waktu_selesai',
        'image',
        'event_category_id',
        'kecamatan_id',
    ];

    protected $searchableFields = ['*'];

    protected $casts = [
        'tanggal' => 'date',
        'waktu_mulai' => 'datetime',
        'waktu_selesai' => 'datetime',
    ];

    public function eventCategory()
    {
        return $this->belongsTo(EventCategory::class);
    }

    public function eventMembers()
    {
        return $this->hasMany(EventMember::class);
    }

    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class);
    }
}
