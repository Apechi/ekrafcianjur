<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Ekraf extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'logo',
        'nama',
        'alamat',
        'coordinate',
        'nama_pemilik',
        'email',
        'no_telp',
        'website',
        'desc',
        'category_id',
        'sub_sector_id',
        'kecamatan_id',
        'user_id',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'ekraf';

    public function ekrafProducts()
    {
        return $this->hasMany(EkrafProduct::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function subSector()
    {
        return $this->belongsTo(SubSector::class);
    }

    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function countByCategory($categoryId)
    {
        return self::where('category_id', $categoryId)->count();
    }
}
