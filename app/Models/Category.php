<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['nama', 'image'];


    protected $searchableFields = ['*'];

    public function allEkraf()
    {
        return $this->hasMany(Ekraf::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
