<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Gedung extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'image', 'nama', 'desc', 'kegiatan', 'harga', 'alamat',
        'no_telp',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'gedung';

    public function bookingHeaders()
    {
        return $this->hasMany(BookingHeader::class);
    }

    public function gambar()
    {
        return $this->hasMany(GedungImage::class);
    }

    public function fasilitas()
    {
        return $this->hasMany(FasilitasGedung::class);
    }
}
