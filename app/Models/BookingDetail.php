<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BookingDetail extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'booking_header_id',
        'harga_unit',
        'sub_total',
        'durasi',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'booking_details';

    public function bookingHeader()
    {
        return $this->belongsTo(BookingHeader::class);
    }
}
