<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Socialmedia extends Model
{
    use HasFactory;
    use Searchable;

    protected $table = 'socialmedias';

    protected $fillable = ['icon', 'link'];

    protected $searchableFields = ['*'];
}
