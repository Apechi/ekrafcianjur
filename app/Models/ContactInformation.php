<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ContactInformation extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'image',
        'about',
        'email',
        'coordinate',
        'address',
        'no_telp',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'contact_informations';
}
