<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EventMember extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'user_id',
        'nama_lengkap',
        'email',
        'no_telp',
        'sub_sector_id',
        'kecamatan_id',
        'event_id',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'event_members';

    public function subSector()
    {
        return $this->belongsTo(SubSector::class);
    }

    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class);
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
