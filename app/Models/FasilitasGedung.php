<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FasilitasGedung extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['fasilitas', 'gedung_id'];

    protected $searchableFields = ['*'];

    protected $table = 'fasilitas_gedung';

    public function gedung()
    {
        return $this->belongsTo(Gedung::class);
    }
}
