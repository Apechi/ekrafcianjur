<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['image', 'title', 'body', 'writer', 'published_at'];

    protected $searchableFields = ['*'];

    protected $casts = [
        'published_at' => 'datetime',
    ];
}
