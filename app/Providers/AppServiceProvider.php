<?php

namespace App\Providers;

use App\Models\ContactInformation;
use App\Models\Socialmedia;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Paginator::useBootstrap();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontend.partials.footer', function ($view) {
            $footerData = ContactInformation::first();
            $footerSM = Socialmedia::all();

            // Ubah sesuai dengan query yang sesuai
            $view->with(['footerData' => $footerData, 'socialmedias' => $footerSM]);
        });
    }
}
