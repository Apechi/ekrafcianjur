<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ekraf', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('logo')->nullable();
            $table->string('nama');
            $table->text('alamat');
            $table->string('coordinate');
            $table->string('nama_pemilik');
            $table->string('email');
            $table->string('no_telp');
            $table->text('website');
            $table->text('desc');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('sub_sector_id');
            $table->unsignedBigInteger('kecamatan_id');
            $table->unsignedBigInteger('user_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ekraf');
    }
};
