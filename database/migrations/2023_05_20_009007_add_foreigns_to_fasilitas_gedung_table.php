<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fasilitas_gedung', function (Blueprint $table) {
            $table
                ->foreign('gedung_id')
                ->references('id')
                ->on('gedung')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fasilitas_gedung', function (Blueprint $table) {
            $table->dropForeign(['gedung_id']);
        });
    }
};
