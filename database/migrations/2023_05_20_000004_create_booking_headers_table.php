<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_headers', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->date('date');
            $table->bigInteger('total_harga');
            $table->enum('status', ['Pending', 'Selesai']);
            $table->enum('payment_type', ['Tunai', 'Debit']);
            $table->string('card_number')->nullable();
            $table->string('bank')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('gedung_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_headers');
    }
};
