<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->longText('body');
            $table->date('tanggal');
            $table->string('pembiayaan');
            $table->string('lokasi');
            $table->text('alamat');
            $table->dateTime('waktu_mulai');
            $table->dateTime('waktu_selesai');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('event_category_id');
            $table->unsignedBigInteger('kecamatan_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
};
