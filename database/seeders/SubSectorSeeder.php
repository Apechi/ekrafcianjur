<?php

namespace Database\Seeders;

use App\Models\SubSector;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubSectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect(
            [
                [
                    'nama' => 'Kuliner',
                ],
                [
                    'nama' => 'Fashion',
                ],
                [
                    'nama' => 'Kriya',
                ],
                [
                    'nama' => 'Televisi & Radio',
                ],
                [
                    'nama' => 'Penerbitan',
                ],
                [
                    'nama' => 'Arsitektur',
                ],
                [
                    'nama' => 'Periklanan',
                ],
                [
                    'nama' => 'Musik',
                ],
                [
                    'nama' => 'Fotografi',
                ],
                [
                    'nama' => 'Seni Pertunjukan',
                ],
                [
                    'nama' => 'Desain Produk',
                ],
                [
                    'nama' => 'Seni Rupa',
                ],
                [
                    'nama' => 'Desain Interior',
                ],
                [
                    'nama' => 'Film, Animasi, dan Video',
                ],
                [
                    'nama' => 'DKV',
                ],
                [
                    'nama' => 'Aplikasi',
                ],
                [
                    'nama' => 'Game',
                ],

            ]

        )->each(function ($sub) {
            DB::table('sub_sectors')->insert($sub);
        });
    }
}
