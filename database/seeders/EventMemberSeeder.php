<?php

namespace Database\Seeders;

use App\Models\EventMember;
use Illuminate\Database\Seeder;

class EventMemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EventMember::factory()
            ->count(5)
            ->create();
    }
}
