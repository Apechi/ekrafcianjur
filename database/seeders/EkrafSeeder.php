<?php

namespace Database\Seeders;

use App\Models\Ekraf;
use Illuminate\Database\Seeder;

class EkrafSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ekraf::factory()
            ->count(5)
            ->create();
    }
}
