<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FasilitasGedung;

class FasilitasGedungSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FasilitasGedung::factory()
            ->count(5)
            ->create();
    }
}
