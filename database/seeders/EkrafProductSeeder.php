<?php

namespace Database\Seeders;

use App\Models\EkrafProduct;
use Illuminate\Database\Seeder;

class EkrafProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EkrafProduct::factory()
            ->count(5)
            ->create();
    }
}
