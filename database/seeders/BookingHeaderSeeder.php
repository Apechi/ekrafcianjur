<?php

namespace Database\Seeders;

use App\Models\BookingHeader;
use Illuminate\Database\Seeder;

class BookingHeaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BookingHeader::factory()
            ->count(5)
            ->create();
    }
}
