<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();


        // Create default permissions
        Permission::create(['name' => 'list articles']);
        Permission::create(['name' => 'view articles']);
        Permission::create(['name' => 'create articles']);
        Permission::create(['name' => 'update articles']);
        Permission::create(['name' => 'delete articles']);

        Permission::create(['name' => 'list banners']);
        Permission::create(['name' => 'view banners']);
        Permission::create(['name' => 'create banners']);
        Permission::create(['name' => 'update banners']);
        Permission::create(['name' => 'delete banners']);

        Permission::create(['name' => 'list bookingdetails']);
        Permission::create(['name' => 'view bookingdetails']);
        Permission::create(['name' => 'create bookingdetails']);
        Permission::create(['name' => 'update bookingdetails']);
        Permission::create(['name' => 'delete bookingdetails']);

        Permission::create(['name' => 'list bookingheaders']);
        Permission::create(['name' => 'view bookingheaders']);
        Permission::create(['name' => 'create bookingheaders']);
        Permission::create(['name' => 'update bookingheaders']);
        Permission::create(['name' => 'delete bookingheaders']);

        Permission::create(['name' => 'list categories']);
        Permission::create(['name' => 'view categories']);
        Permission::create(['name' => 'create categories']);
        Permission::create(['name' => 'update categories']);
        Permission::create(['name' => 'delete categories']);

        Permission::create(['name' => 'list contactinformations']);
        Permission::create(['name' => 'view contactinformations']);
        Permission::create(['name' => 'create contactinformations']);
        Permission::create(['name' => 'update contactinformations']);
        Permission::create(['name' => 'delete contactinformations']);

        Permission::create(['name' => 'list contactsboxes']);
        Permission::create(['name' => 'view contactsboxes']);
        Permission::create(['name' => 'create contactsboxes']);
        Permission::create(['name' => 'update contactsboxes']);
        Permission::create(['name' => 'delete contactsboxes']);

        Permission::create(['name' => 'list allekraf']);
        Permission::create(['name' => 'view allekraf']);
        Permission::create(['name' => 'create allekraf']);
        Permission::create(['name' => 'update allekraf']);
        Permission::create(['name' => 'delete allekraf']);

        Permission::create(['name' => 'list ekrafproducts']);
        Permission::create(['name' => 'view ekrafproducts']);
        Permission::create(['name' => 'create ekrafproducts']);
        Permission::create(['name' => 'update ekrafproducts']);
        Permission::create(['name' => 'delete ekrafproducts']);

        Permission::create(['name' => 'list events']);
        Permission::create(['name' => 'view events']);
        Permission::create(['name' => 'create events']);
        Permission::create(['name' => 'update events']);
        Permission::create(['name' => 'delete events']);

        Permission::create(['name' => 'list eventcategories']);
        Permission::create(['name' => 'view eventcategories']);
        Permission::create(['name' => 'create eventcategories']);
        Permission::create(['name' => 'update eventcategories']);
        Permission::create(['name' => 'delete eventcategories']);

        Permission::create(['name' => 'list eventmembers']);
        Permission::create(['name' => 'view eventmembers']);
        Permission::create(['name' => 'create eventmembers']);
        Permission::create(['name' => 'update eventmembers']);
        Permission::create(['name' => 'delete eventmembers']);

        Permission::create(['name' => 'list allfasilitasgedung']);
        Permission::create(['name' => 'view allfasilitasgedung']);
        Permission::create(['name' => 'create allfasilitasgedung']);
        Permission::create(['name' => 'update allfasilitasgedung']);
        Permission::create(['name' => 'delete allfasilitasgedung']);

        Permission::create(['name' => 'list allgedung']);
        Permission::create(['name' => 'view allgedung']);
        Permission::create(['name' => 'create allgedung']);
        Permission::create(['name' => 'update allgedung']);
        Permission::create(['name' => 'delete allgedung']);

        Permission::create(['name' => 'list gedungimages']);
        Permission::create(['name' => 'view gedungimages']);
        Permission::create(['name' => 'create gedungimages']);
        Permission::create(['name' => 'update gedungimages']);
        Permission::create(['name' => 'delete gedungimages']);

        Permission::create(['name' => 'list allkecamatan']);
        Permission::create(['name' => 'view allkecamatan']);
        Permission::create(['name' => 'create allkecamatan']);
        Permission::create(['name' => 'update allkecamatan']);
        Permission::create(['name' => 'delete allkecamatan']);

        Permission::create(['name' => 'list partners']);
        Permission::create(['name' => 'view partners']);
        Permission::create(['name' => 'create partners']);
        Permission::create(['name' => 'update partners']);
        Permission::create(['name' => 'delete partners']);

        Permission::create(['name' => 'list socialmedias']);
        Permission::create(['name' => 'view socialmedias']);
        Permission::create(['name' => 'create socialmedias']);
        Permission::create(['name' => 'update socialmedias']);
        Permission::create(['name' => 'delete socialmedias']);

        Permission::create(['name' => 'list subsectors']);
        Permission::create(['name' => 'view subsectors']);
        Permission::create(['name' => 'create subsectors']);
        Permission::create(['name' => 'update subsectors']);
        Permission::create(['name' => 'delete subsectors']);

        // Create admin role and assign existing permissions
        $currentPermissions = Permission::all();
        $adminRole = Role::create(['name' => 'admin']);
        $adminRole->givePermissionTo($currentPermissions);

        // Create admin exclusive permissions
        Permission::create(['name' => 'list roles']);
        Permission::create(['name' => 'view roles']);
        Permission::create(['name' => 'create roles']);
        Permission::create(['name' => 'update roles']);
        Permission::create(['name' => 'delete roles']);

        Permission::create(['name' => 'list permissions']);
        Permission::create(['name' => 'view permissions']);
        Permission::create(['name' => 'create permissions']);
        Permission::create(['name' => 'update permissions']);
        Permission::create(['name' => 'delete permissions']);

        Permission::create(['name' => 'list users']);
        Permission::create(['name' => 'view users']);
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'update users']);
        Permission::create(['name' => 'delete users']);

        // Create admin role and assign all permissions
        $allPermissions = Permission::all();
        $superadminRole = Role::create(['name' => 'super-admin']);
        $superadminRole->givePermissionTo($allPermissions);

        $user = \App\Models\User::whereEmail('admin@admin.com')->first();

        if ($user) {
            $user->assignRole($superadminRole);
        }

        $userPermission = Permission::create(['name' => 'user permission']);
        $userrole = Role::create(['name' => 'user']);
        $userrole->givePermissionTo($userPermission);
    }
}
