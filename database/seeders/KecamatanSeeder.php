<?php

namespace Database\Seeders;

use App\Models\Kecamatan;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KecamatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect(
            [
                ['nama' => 'Kecamatan Agrabinta'],
                ['nama' => 'Kecamatan Bojongpicung'],
                ['nama' => 'Kecamatan Campaka'],
                ['nama' => 'Kecamatan Campaka Mulya'],
                ['nama' => 'Kecamatan Cibeber'],
                ['nama' => 'Kecamatan Cibinong'],
                ['nama' => 'Kecamatan Cidaun'],
                ['nama' => 'Kecamatan Cijati'],
                ['nama' => 'Kecamatan Cikadu'],
                ['nama' => 'Kecamatan Cikalongkulon'],
                ['nama' => 'Kecamatan Cilaku'],
                ['nama' => 'Kecamatan Cipanas'],
                ['nama' => 'Kecamatan Ciranjang'],
                ['nama' => 'Kecamatan Cugenang'],
                ['nama' => 'Kecamatan Gekbrong'],
                ['nama' => 'Kecamatan Haurwangi'],
                ['nama' => 'Kecamatan Kadupandak'],
                ['nama' => 'Kecamatan Karangtengah'],
                ['nama' => 'Kecamatan Leles'],
                ['nama' => 'Kecamatan Mande'],
                ['nama' => 'Kecamatan Naringgul'],
                ['nama' => 'Kecamatan Pacet'],
                ['nama' => 'Kecamatan Pagelaran'],
                ['nama' => 'Kecamatan Pasirkuda'],
                ['nama' => 'Kecamatan Sindangbarang'],
                ['nama' => 'Kecamatan Sukaluyu'],
                ['nama' => 'Kecamatan Sukanagara'],
                ['nama' => 'Kecamatan Sukaresmi'],
                ['nama' => 'Kecamatan Takokak'],
                ['nama' => 'Kecamatan Tanggeung'],
                ['nama' => 'Kecamatan Warungkondang']
            ]

        )->each(function ($kecamatan) {
            DB::table('kecamatan')->insert($kecamatan);
        });
    }
}
