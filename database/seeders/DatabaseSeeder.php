<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Adding an admin user
        $user = \App\Models\User::factory()
            ->count(1)
            ->create([
                'email' => 'admin@admin.com',
                'password' => Hash::make('admin'),
            ]);
        $this->call(PermissionsSeeder::class);
        // $this->call(ArticleSeeder::class);
        // $this->call(BannerSeeder::class);
        // $this->call(BookingDetailSeeder::class);
        // $this->call(BookingHeaderSeeder::class);
        $this->call(CategorySeeder::class);
        // $this->call(EkrafSeeder::class);
        // $this->call(EkrafProductSeeder::class);
        // $this->call(EventSeeder::class);
        // $this->call(EventCategorySeeder::class);
        // $this->call(EventMemberSeeder::class);
        // $this->call(FasilitasGedungSeeder::class);
        // $this->call(GedungSeeder::class);
        $this->call(KecamatanSeeder::class);
        $this->call(SubSectorSeeder::class);
        $this->call(UserSeeder::class);
    }
}
