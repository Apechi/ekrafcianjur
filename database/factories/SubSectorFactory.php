<?php

namespace Database\Factories;

use App\Models\SubSector;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubSectorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubSector::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nama' => $this->faker->text(255),
        ];
    }
}
