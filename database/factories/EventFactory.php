<?php

namespace Database\Factories;

use App\Models\Event;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(10),
            'body' => $this->faker->text,
            'tanggal' => $this->faker->date,
            'pembiayaan' => $this->faker->text,
            'lokasi' => $this->faker->address,
            'alamat' => $this->faker->text,
            'waktu_mulai' => $this->faker->dateTime,
            'waktu_selesai' => $this->faker->dateTime,
            'event_category_id' => \App\Models\EventCategory::factory(),
        ];
    }
}
