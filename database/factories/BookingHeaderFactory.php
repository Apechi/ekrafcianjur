<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\BookingHeader;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookingHeaderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BookingHeader::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date' => $this->faker->date,
            'total_harga' => $this->faker->randomNumber,
            'status' => 'Pending',
            'payment_type' => 'Tunai',
            'card_number' => $this->faker->text(255),
            'bank' => $this->faker->text(255),
            'user_id' => \App\Models\User::factory(),
            'gedung_id' => \App\Models\Gedung::factory(),
        ];
    }
}
