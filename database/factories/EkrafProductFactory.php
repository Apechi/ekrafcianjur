<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\EkrafProduct;
use Illuminate\Database\Eloquent\Factories\Factory;

class EkrafProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EkrafProduct::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nama' => $this->faker->text(255),
            'tipe' => $this->faker->text(255),
            'harga_jual' => $this->faker->randomNumber,
            'ekspor' => 'Ya',
            'desc' => $this->faker->text,
            'ekraf_id' => \App\Models\Ekraf::factory(),
        ];
    }
}
