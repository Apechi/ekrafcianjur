<?php

namespace Database\Factories;

use App\Models\EventMember;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventMemberFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EventMember::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nama_lengkap' => $this->faker->text(255),
            'email' => $this->faker->email,
            'no_telp' => $this->faker->text(255),
            'sub_sector_id' => \App\Models\SubSector::factory(),
            'kecamatan_id' => \App\Models\Kecamatan::factory(),
            'event_id' => \App\Models\Event::factory(),
            'user_id' => \App\Models\User::factory(),
        ];
    }
}
