<?php

namespace Database\Factories;

use App\Models\Gedung;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class GedungFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Gedung::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nama' => $this->faker->text(255),
            'desc' => $this->faker->text,
            'kegiatan' => $this->faker->text,
            'harga' => $this->faker->randomNumber,
        ];
    }
}
