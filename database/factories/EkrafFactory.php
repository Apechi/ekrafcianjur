<?php

namespace Database\Factories;

use App\Models\Ekraf;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class EkrafFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ekraf::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'logo' => $this->faker->text(255),
            'nama' => $this->faker->text(255),
            'alamat' => $this->faker->text,
            'coordinate' => $this->faker->text(255),
            'nama_pemilik' => $this->faker->text(255),
            'email' => $this->faker->email,
            'no_telp' => $this->faker->text(255),
            'website' => $this->faker->text,
            'desc' => $this->faker->text,
            'category_id' => \App\Models\Category::factory(),
            'sub_sector_id' => \App\Models\SubSector::factory(),
            'kecamatan_id' => \App\Models\Kecamatan::factory(),
            'user_id' => \App\Models\User::factory(),
        ];
    }
}
