<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\BookingDetail;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookingDetailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BookingDetail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'harga_unit' => $this->faker->randomNumber,
            'sub_total' => $this->faker->randomNumber,
            'durasi' => $this->faker->randomNumber(0),
            'booking_header_id' => \App\Models\BookingHeader::factory(),
        ];
    }
}
