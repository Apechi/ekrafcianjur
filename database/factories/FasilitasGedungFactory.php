<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\FasilitasGedung;
use Illuminate\Database\Eloquent\Factories\Factory;

class FasilitasGedungFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FasilitasGedung::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fasilitas' => $this->faker->text(255),
        ];
    }
}
