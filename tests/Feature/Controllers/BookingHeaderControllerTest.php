<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\BookingHeader;

use App\Models\Gedung;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookingHeaderControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_booking_headers()
    {
        $bookingHeaders = BookingHeader::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('booking-headers.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.booking_headers.index')
            ->assertViewHas('bookingHeaders');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_booking_header()
    {
        $response = $this->get(route('booking-headers.create'));

        $response->assertOk()->assertViewIs('app.booking_headers.create');
    }

    /**
     * @test
     */
    public function it_stores_the_booking_header()
    {
        $data = BookingHeader::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('booking-headers.store'), $data);

        $this->assertDatabaseHas('booking_headers', $data);

        $bookingHeader = BookingHeader::latest('id')->first();

        $response->assertRedirect(
            route('booking-headers.edit', $bookingHeader)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_booking_header()
    {
        $bookingHeader = BookingHeader::factory()->create();

        $response = $this->get(route('booking-headers.show', $bookingHeader));

        $response
            ->assertOk()
            ->assertViewIs('app.booking_headers.show')
            ->assertViewHas('bookingHeader');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_booking_header()
    {
        $bookingHeader = BookingHeader::factory()->create();

        $response = $this->get(route('booking-headers.edit', $bookingHeader));

        $response
            ->assertOk()
            ->assertViewIs('app.booking_headers.edit')
            ->assertViewHas('bookingHeader');
    }

    /**
     * @test
     */
    public function it_updates_the_booking_header()
    {
        $bookingHeader = BookingHeader::factory()->create();

        $user = User::factory()->create();
        $gedung = Gedung::factory()->create();

        $data = [
            'date' => $this->faker->date,
            'total_harga' => $this->faker->randomNumber,
            'status' => 'Pending',
            'payment_type' => 'Tunai',
            'card_number' => $this->faker->text(255),
            'bank' => $this->faker->text(255),
            'user_id' => $user->id,
            'gedung_id' => $gedung->id,
        ];

        $response = $this->put(
            route('booking-headers.update', $bookingHeader),
            $data
        );

        $data['id'] = $bookingHeader->id;

        $this->assertDatabaseHas('booking_headers', $data);

        $response->assertRedirect(
            route('booking-headers.edit', $bookingHeader)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_booking_header()
    {
        $bookingHeader = BookingHeader::factory()->create();

        $response = $this->delete(
            route('booking-headers.destroy', $bookingHeader)
        );

        $response->assertRedirect(route('booking-headers.index'));

        $this->assertModelMissing($bookingHeader);
    }
}
