<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EventMember;

use App\Models\Event;
use App\Models\SubSector;
use App\Models\Kecamatan;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EventMemberControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_event_members()
    {
        $eventMembers = EventMember::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('event-members.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.event_members.index')
            ->assertViewHas('eventMembers');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_event_member()
    {
        $response = $this->get(route('event-members.create'));

        $response->assertOk()->assertViewIs('app.event_members.create');
    }

    /**
     * @test
     */
    public function it_stores_the_event_member()
    {
        $data = EventMember::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('event-members.store'), $data);

        $this->assertDatabaseHas('event_members', $data);

        $eventMember = EventMember::latest('id')->first();

        $response->assertRedirect(route('event-members.edit', $eventMember));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_event_member()
    {
        $eventMember = EventMember::factory()->create();

        $response = $this->get(route('event-members.show', $eventMember));

        $response
            ->assertOk()
            ->assertViewIs('app.event_members.show')
            ->assertViewHas('eventMember');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_event_member()
    {
        $eventMember = EventMember::factory()->create();

        $response = $this->get(route('event-members.edit', $eventMember));

        $response
            ->assertOk()
            ->assertViewIs('app.event_members.edit')
            ->assertViewHas('eventMember');
    }

    /**
     * @test
     */
    public function it_updates_the_event_member()
    {
        $eventMember = EventMember::factory()->create();

        $subSector = SubSector::factory()->create();
        $kecamatan = Kecamatan::factory()->create();
        $event = Event::factory()->create();
        $user = User::factory()->create();

        $data = [
            'nama_lengkap' => $this->faker->text(255),
            'email' => $this->faker->email,
            'no_telp' => $this->faker->text(255),
            'sub_sector_id' => $subSector->id,
            'kecamatan_id' => $kecamatan->id,
            'event_id' => $event->id,
            'user_id' => $user->id,
        ];

        $response = $this->put(
            route('event-members.update', $eventMember),
            $data
        );

        $data['id'] = $eventMember->id;

        $this->assertDatabaseHas('event_members', $data);

        $response->assertRedirect(route('event-members.edit', $eventMember));
    }

    /**
     * @test
     */
    public function it_deletes_the_event_member()
    {
        $eventMember = EventMember::factory()->create();

        $response = $this->delete(route('event-members.destroy', $eventMember));

        $response->assertRedirect(route('event-members.index'));

        $this->assertModelMissing($eventMember);
    }
}
