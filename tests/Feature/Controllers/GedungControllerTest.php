<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\Gedung;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GedungControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_all_gedung()
    {
        $allGedung = Gedung::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('all-gedung.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.all_gedung.index')
            ->assertViewHas('allGedung');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_gedung()
    {
        $response = $this->get(route('all-gedung.create'));

        $response->assertOk()->assertViewIs('app.all_gedung.create');
    }

    /**
     * @test
     */
    public function it_stores_the_gedung()
    {
        $data = Gedung::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('all-gedung.store'), $data);

        $this->assertDatabaseHas('gedung', $data);

        $gedung = Gedung::latest('id')->first();

        $response->assertRedirect(route('all-gedung.edit', $gedung));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_gedung()
    {
        $gedung = Gedung::factory()->create();

        $response = $this->get(route('all-gedung.show', $gedung));

        $response
            ->assertOk()
            ->assertViewIs('app.all_gedung.show')
            ->assertViewHas('gedung');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_gedung()
    {
        $gedung = Gedung::factory()->create();

        $response = $this->get(route('all-gedung.edit', $gedung));

        $response
            ->assertOk()
            ->assertViewIs('app.all_gedung.edit')
            ->assertViewHas('gedung');
    }

    /**
     * @test
     */
    public function it_updates_the_gedung()
    {
        $gedung = Gedung::factory()->create();

        $data = [
            'nama' => $this->faker->text(255),
            'desc' => $this->faker->text,
            'kegiatan' => $this->faker->text,
            'harga' => $this->faker->randomNumber,
        ];

        $response = $this->put(route('all-gedung.update', $gedung), $data);

        $data['id'] = $gedung->id;

        $this->assertDatabaseHas('gedung', $data);

        $response->assertRedirect(route('all-gedung.edit', $gedung));
    }

    /**
     * @test
     */
    public function it_deletes_the_gedung()
    {
        $gedung = Gedung::factory()->create();

        $response = $this->delete(route('all-gedung.destroy', $gedung));

        $response->assertRedirect(route('all-gedung.index'));

        $this->assertModelMissing($gedung);
    }
}
