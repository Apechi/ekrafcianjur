<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EventCategory;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EventCategoryControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_event_categories()
    {
        $eventCategories = EventCategory::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('event-categories.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.event_categories.index')
            ->assertViewHas('eventCategories');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_event_category()
    {
        $response = $this->get(route('event-categories.create'));

        $response->assertOk()->assertViewIs('app.event_categories.create');
    }

    /**
     * @test
     */
    public function it_stores_the_event_category()
    {
        $data = EventCategory::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('event-categories.store'), $data);

        $this->assertDatabaseHas('event_categories', $data);

        $eventCategory = EventCategory::latest('id')->first();

        $response->assertRedirect(
            route('event-categories.edit', $eventCategory)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_event_category()
    {
        $eventCategory = EventCategory::factory()->create();

        $response = $this->get(route('event-categories.show', $eventCategory));

        $response
            ->assertOk()
            ->assertViewIs('app.event_categories.show')
            ->assertViewHas('eventCategory');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_event_category()
    {
        $eventCategory = EventCategory::factory()->create();

        $response = $this->get(route('event-categories.edit', $eventCategory));

        $response
            ->assertOk()
            ->assertViewIs('app.event_categories.edit')
            ->assertViewHas('eventCategory');
    }

    /**
     * @test
     */
    public function it_updates_the_event_category()
    {
        $eventCategory = EventCategory::factory()->create();

        $data = [
            'nama' => $this->faker->text(255),
        ];

        $response = $this->put(
            route('event-categories.update', $eventCategory),
            $data
        );

        $data['id'] = $eventCategory->id;

        $this->assertDatabaseHas('event_categories', $data);

        $response->assertRedirect(
            route('event-categories.edit', $eventCategory)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_event_category()
    {
        $eventCategory = EventCategory::factory()->create();

        $response = $this->delete(
            route('event-categories.destroy', $eventCategory)
        );

        $response->assertRedirect(route('event-categories.index'));

        $this->assertModelMissing($eventCategory);
    }
}
