<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\BookingDetail;

use App\Models\BookingHeader;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookingDetailControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_booking_details()
    {
        $bookingDetails = BookingDetail::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('booking-details.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.booking_details.index')
            ->assertViewHas('bookingDetails');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_booking_detail()
    {
        $response = $this->get(route('booking-details.create'));

        $response->assertOk()->assertViewIs('app.booking_details.create');
    }

    /**
     * @test
     */
    public function it_stores_the_booking_detail()
    {
        $data = BookingDetail::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('booking-details.store'), $data);

        $this->assertDatabaseHas('booking_details', $data);

        $bookingDetail = BookingDetail::latest('id')->first();

        $response->assertRedirect(
            route('booking-details.edit', $bookingDetail)
        );
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_booking_detail()
    {
        $bookingDetail = BookingDetail::factory()->create();

        $response = $this->get(route('booking-details.show', $bookingDetail));

        $response
            ->assertOk()
            ->assertViewIs('app.booking_details.show')
            ->assertViewHas('bookingDetail');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_booking_detail()
    {
        $bookingDetail = BookingDetail::factory()->create();

        $response = $this->get(route('booking-details.edit', $bookingDetail));

        $response
            ->assertOk()
            ->assertViewIs('app.booking_details.edit')
            ->assertViewHas('bookingDetail');
    }

    /**
     * @test
     */
    public function it_updates_the_booking_detail()
    {
        $bookingDetail = BookingDetail::factory()->create();

        $bookingHeader = BookingHeader::factory()->create();

        $data = [
            'harga_unit' => $this->faker->randomNumber,
            'sub_total' => $this->faker->randomNumber,
            'durasi' => $this->faker->randomNumber(0),
            'booking_header_id' => $bookingHeader->id,
        ];

        $response = $this->put(
            route('booking-details.update', $bookingDetail),
            $data
        );

        $data['id'] = $bookingDetail->id;

        $this->assertDatabaseHas('booking_details', $data);

        $response->assertRedirect(
            route('booking-details.edit', $bookingDetail)
        );
    }

    /**
     * @test
     */
    public function it_deletes_the_booking_detail()
    {
        $bookingDetail = BookingDetail::factory()->create();

        $response = $this->delete(
            route('booking-details.destroy', $bookingDetail)
        );

        $response->assertRedirect(route('booking-details.index'));

        $this->assertModelMissing($bookingDetail);
    }
}
