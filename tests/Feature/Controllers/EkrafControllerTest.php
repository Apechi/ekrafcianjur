<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\Ekraf;

use App\Models\Category;
use App\Models\SubSector;
use App\Models\Kecamatan;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EkrafControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_all_ekraf()
    {
        $allEkraf = Ekraf::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('all-ekraf.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.all_ekraf.index')
            ->assertViewHas('allEkraf');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ekraf()
    {
        $response = $this->get(route('all-ekraf.create'));

        $response->assertOk()->assertViewIs('app.all_ekraf.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ekraf()
    {
        $data = Ekraf::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('all-ekraf.store'), $data);

        $this->assertDatabaseHas('ekraf', $data);

        $ekraf = Ekraf::latest('id')->first();

        $response->assertRedirect(route('all-ekraf.edit', $ekraf));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ekraf()
    {
        $ekraf = Ekraf::factory()->create();

        $response = $this->get(route('all-ekraf.show', $ekraf));

        $response
            ->assertOk()
            ->assertViewIs('app.all_ekraf.show')
            ->assertViewHas('ekraf');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ekraf()
    {
        $ekraf = Ekraf::factory()->create();

        $response = $this->get(route('all-ekraf.edit', $ekraf));

        $response
            ->assertOk()
            ->assertViewIs('app.all_ekraf.edit')
            ->assertViewHas('ekraf');
    }

    /**
     * @test
     */
    public function it_updates_the_ekraf()
    {
        $ekraf = Ekraf::factory()->create();

        $category = Category::factory()->create();
        $subSector = SubSector::factory()->create();
        $kecamatan = Kecamatan::factory()->create();
        $user = User::factory()->create();

        $data = [
            'logo' => $this->faker->text(255),
            'nama' => $this->faker->text(255),
            'alamat' => $this->faker->text,
            'coordinate' => $this->faker->text(255),
            'nama_pemilik' => $this->faker->text(255),
            'email' => $this->faker->email,
            'no_telp' => $this->faker->text(255),
            'website' => $this->faker->text,
            'desc' => $this->faker->text,
            'category_id' => $category->id,
            'sub_sector_id' => $subSector->id,
            'kecamatan_id' => $kecamatan->id,
            'user_id' => $user->id,
        ];

        $response = $this->put(route('all-ekraf.update', $ekraf), $data);

        $data['id'] = $ekraf->id;

        $this->assertDatabaseHas('ekraf', $data);

        $response->assertRedirect(route('all-ekraf.edit', $ekraf));
    }

    /**
     * @test
     */
    public function it_deletes_the_ekraf()
    {
        $ekraf = Ekraf::factory()->create();

        $response = $this->delete(route('all-ekraf.destroy', $ekraf));

        $response->assertRedirect(route('all-ekraf.index'));

        $this->assertModelMissing($ekraf);
    }
}
