<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\Kecamatan;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class KecamatanControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_all_kecamatan()
    {
        $allKecamatan = Kecamatan::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('all-kecamatan.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.all_kecamatan.index')
            ->assertViewHas('allKecamatan');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_kecamatan()
    {
        $response = $this->get(route('all-kecamatan.create'));

        $response->assertOk()->assertViewIs('app.all_kecamatan.create');
    }

    /**
     * @test
     */
    public function it_stores_the_kecamatan()
    {
        $data = Kecamatan::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('all-kecamatan.store'), $data);

        $this->assertDatabaseHas('kecamatan', $data);

        $kecamatan = Kecamatan::latest('id')->first();

        $response->assertRedirect(route('all-kecamatan.edit', $kecamatan));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_kecamatan()
    {
        $kecamatan = Kecamatan::factory()->create();

        $response = $this->get(route('all-kecamatan.show', $kecamatan));

        $response
            ->assertOk()
            ->assertViewIs('app.all_kecamatan.show')
            ->assertViewHas('kecamatan');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_kecamatan()
    {
        $kecamatan = Kecamatan::factory()->create();

        $response = $this->get(route('all-kecamatan.edit', $kecamatan));

        $response
            ->assertOk()
            ->assertViewIs('app.all_kecamatan.edit')
            ->assertViewHas('kecamatan');
    }

    /**
     * @test
     */
    public function it_updates_the_kecamatan()
    {
        $kecamatan = Kecamatan::factory()->create();

        $data = [
            'nama' => $this->faker->text(255),
        ];

        $response = $this->put(
            route('all-kecamatan.update', $kecamatan),
            $data
        );

        $data['id'] = $kecamatan->id;

        $this->assertDatabaseHas('kecamatan', $data);

        $response->assertRedirect(route('all-kecamatan.edit', $kecamatan));
    }

    /**
     * @test
     */
    public function it_deletes_the_kecamatan()
    {
        $kecamatan = Kecamatan::factory()->create();

        $response = $this->delete(route('all-kecamatan.destroy', $kecamatan));

        $response->assertRedirect(route('all-kecamatan.index'));

        $this->assertModelMissing($kecamatan);
    }
}
