<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\EkrafProduct;

use App\Models\Ekraf;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EkrafProductControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_ekraf_products()
    {
        $ekrafProducts = EkrafProduct::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('ekraf-products.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.ekraf_products.index')
            ->assertViewHas('ekrafProducts');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_ekraf_product()
    {
        $response = $this->get(route('ekraf-products.create'));

        $response->assertOk()->assertViewIs('app.ekraf_products.create');
    }

    /**
     * @test
     */
    public function it_stores_the_ekraf_product()
    {
        $data = EkrafProduct::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('ekraf-products.store'), $data);

        $this->assertDatabaseHas('ekraf_products', $data);

        $ekrafProduct = EkrafProduct::latest('id')->first();

        $response->assertRedirect(route('ekraf-products.edit', $ekrafProduct));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_ekraf_product()
    {
        $ekrafProduct = EkrafProduct::factory()->create();

        $response = $this->get(route('ekraf-products.show', $ekrafProduct));

        $response
            ->assertOk()
            ->assertViewIs('app.ekraf_products.show')
            ->assertViewHas('ekrafProduct');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_ekraf_product()
    {
        $ekrafProduct = EkrafProduct::factory()->create();

        $response = $this->get(route('ekraf-products.edit', $ekrafProduct));

        $response
            ->assertOk()
            ->assertViewIs('app.ekraf_products.edit')
            ->assertViewHas('ekrafProduct');
    }

    /**
     * @test
     */
    public function it_updates_the_ekraf_product()
    {
        $ekrafProduct = EkrafProduct::factory()->create();

        $ekraf = Ekraf::factory()->create();

        $data = [
            'nama' => $this->faker->text(255),
            'tipe' => $this->faker->text(255),
            'harga_jual' => $this->faker->randomNumber,
            'ekspor' => 'Ya',
            'desc' => $this->faker->text,
            'ekraf_id' => $ekraf->id,
        ];

        $response = $this->put(
            route('ekraf-products.update', $ekrafProduct),
            $data
        );

        $data['id'] = $ekrafProduct->id;

        $this->assertDatabaseHas('ekraf_products', $data);

        $response->assertRedirect(route('ekraf-products.edit', $ekrafProduct));
    }

    /**
     * @test
     */
    public function it_deletes_the_ekraf_product()
    {
        $ekrafProduct = EkrafProduct::factory()->create();

        $response = $this->delete(
            route('ekraf-products.destroy', $ekrafProduct)
        );

        $response->assertRedirect(route('ekraf-products.index'));

        $this->assertModelMissing($ekrafProduct);
    }
}
