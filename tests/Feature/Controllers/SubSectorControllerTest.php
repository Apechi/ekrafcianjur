<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\SubSector;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SubSectorControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_sub_sectors()
    {
        $subSectors = SubSector::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('sub-sectors.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.sub_sectors.index')
            ->assertViewHas('subSectors');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_sub_sector()
    {
        $response = $this->get(route('sub-sectors.create'));

        $response->assertOk()->assertViewIs('app.sub_sectors.create');
    }

    /**
     * @test
     */
    public function it_stores_the_sub_sector()
    {
        $data = SubSector::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('sub-sectors.store'), $data);

        $this->assertDatabaseHas('sub_sectors', $data);

        $subSector = SubSector::latest('id')->first();

        $response->assertRedirect(route('sub-sectors.edit', $subSector));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_sub_sector()
    {
        $subSector = SubSector::factory()->create();

        $response = $this->get(route('sub-sectors.show', $subSector));

        $response
            ->assertOk()
            ->assertViewIs('app.sub_sectors.show')
            ->assertViewHas('subSector');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_sub_sector()
    {
        $subSector = SubSector::factory()->create();

        $response = $this->get(route('sub-sectors.edit', $subSector));

        $response
            ->assertOk()
            ->assertViewIs('app.sub_sectors.edit')
            ->assertViewHas('subSector');
    }

    /**
     * @test
     */
    public function it_updates_the_sub_sector()
    {
        $subSector = SubSector::factory()->create();

        $data = [
            'nama' => $this->faker->text(255),
        ];

        $response = $this->put(route('sub-sectors.update', $subSector), $data);

        $data['id'] = $subSector->id;

        $this->assertDatabaseHas('sub_sectors', $data);

        $response->assertRedirect(route('sub-sectors.edit', $subSector));
    }

    /**
     * @test
     */
    public function it_deletes_the_sub_sector()
    {
        $subSector = SubSector::factory()->create();

        $response = $this->delete(route('sub-sectors.destroy', $subSector));

        $response->assertRedirect(route('sub-sectors.index'));

        $this->assertModelMissing($subSector);
    }
}
